import React from 'react';
import {DeckGL, ScatterplotLayer,GeoJsonLayer,ColumnLayer} from 'deck.gl';
import ReactMapGL, {Popup,_MapContext as MapContext,NavigationControl} from 'react-map-gl';
import {defaultMapStyle, darkMapStyle, lightMapStyle, satelliteMapStyle} from './style/map-style.js';
import ControlBanjir from './ControlBanjir.js'
import mapboxgl from 'mapbox-gl'
import axios from 'axios';
import { Button,Icon } from 'semantic-ui-react';
import "../analyzer/App.css";


const MAPBOX_TOKEN = 'pk.eyJ1IjoibW9ocmlkd2FuaGRwIiwiYSI6ImNrMDRvaGwzdDA0dDkzaG9tNnA3dmViODQifQ.qa1W5PaDaHzLnqIXbAM7zw';
const pintuAirLink = 'https://hnjp62bwxh.execute-api.us-west-2.amazonaws.com/GeoDev/getpintuairbanjir'
const Rainbow = require('rainbowvis.js');
const convert = require('color-convert');
var groupColor = new Rainbow();
const refMap =  React.createRef();
const arrPolygonDate = ['01 Januari 2020','02 Januari 2020','03 Januari 2020','04 Januari 2020','05 Januari 2020','24 Januari 2020','25 Februari 2020','26 Februari 2020'];
const arrPengungsiDate = ['04 Januari 2020','05 Januari 2020','06 Januari 2020','07 Januari 2020','08 Januari 2020','09 Januari 2020','10 Januari 2020'];

const arrLinkPolygon = [
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/1_Januari_2020_3.geojson',
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/2_Januari_2020_3.geojson',
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/3_Januari_2020_3.geojson',
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/5_Januari_2020_3.geojson',
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/5_Januari_2020_3.geojson',
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/24+Januari+2020.geojson',
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_f602d150-5788-11ea-844c-dba067cdcf5f_Area Banjir 25 Februari_2.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_7babcb40-5847-11ea-a5e4-c1c53950f32f_26_Februari_2020_4.geojson"
];

const arrLinkPoint = [
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/laporan_banjir_1_januari.geojson',
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/laporan_banjir_2_januari.geojson',
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/laporan_banjir_3_januari.geojson',
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/laporan_banjir_4_januari.geojson',
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/laporan_banjir_5_januari.geojson',  
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/laporan_banjir_5_januari.geojson',  
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_6bfb6060-57a8-11ea-b77f-69086c04fc3a_Titik_Banjir_25_Februari_3.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_d4125e30-5846-11ea-a48e-93c99462b8bf_Titik_Banjir_26_Februari_2020_4.geojson"
];
const arrLinkDAS = [
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/DAS+CILICIS.geojson', // DAS
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/10Jan_Sungai_Utama_Ciliwung_Cisadane.geojson', // Sungai Utama
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/Danau.geojson', // Danau  
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/10Jan_Sungai_Jakarta.geojson' // Anak Sungai
];

const arrColorDAS = [
  'purple', // DAS cilicis
  'darkblue',//sungai utama
  'yellow ', // Danau
  'cyan' //Anak sungai
]

const arrTypeDAS = [
  'fill', // DAS cilicis
  'line',//sungai utama
  'fill', // Danau
  'line' //Anak sungai
]

const arrLinkPengungsi = [
  'https://geomapid-layer.s3-us-west-2.amazonaws.com/banjir/pengungsi_','_Januari_2020_2.geojson'
]
export default class MapBanjir extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    mapStyle: defaultMapStyle,
    viewState : {
        width : "100vw",
        height : "100vh",
        latitude: -6.175110,
        longitude: 106.865036,
        zoom: 12
      },
      viewStateDeck : {
        width : "100%",
        height : 1000,
        latitude: -6.175110,
        longitude: 106.865036,
        zoom: 12
      },
      popupInfo : null,
      layers3D : [],
      hoveredObject : null,
      pointerX : 0,
      pointerY : 0,
      visualParams : [],
      dataLayers : null,
      view3D : false,
      columnPosition : null,
      year : 1,
      layerInfo : null,
      layerInfoVisibility : false,
      activeLayer : null,
      scaleCount : 4,
      legendColor : null,
      isPoint : false,
      radius : 200,
      viewBuffer : false,
      layerSettings : {
        smallestName : 'Fewer',
        largestName : 'More',
        smallestColor : '#FF0000',
        largestColor : '#00FF00'
      },
      showPopup : false,
      dataColumn : null,
      selectedMode : 'height',
      inspectMode : false,
      arrLayerInfo : [],
      max : 1,
      min : 0,
      time : 1,
      pointGeojson : null,
      polygonGeojson : null,
      indexPolygon : 0,
      width : window.innerWidth,
      height : window.innerHeight,
      viewPolygon : false,
      viewDAS : true,
      maxPengungsi : 1,
      minPengungsi : 0,
      indexPengungsi : 0,
      viewPengungsi : false,
      viewPintu  : false,
      viewLaporan : true,
      infoTooltip : null,
      popupX : 0,
      popupY : 0,
      popupProps : null,
      popupType : ''

  }
    this._onViewStateChange = this._onViewStateChange.bind(this);
    this._onViewStateChange = this._onViewStateChange.bind(this);
    this._refreshMap = this._refreshMap.bind(this);
    this.layerBoxRef = React.createRef();
    this._renderTooltip = this._renderTooltip.bind(this);
    this._resize = this._resize.bind(this);
    this.loadDAS = this.loadDAS.bind(this);
    this.mapOnLoad = this.mapOnLoad.bind(this);
    groupColor.setSpectrum('darkred','red','orange','yellow','green','blue','darkblue','violet','purple');

  }

 
  async componentDidMount(){
    if(window.innerWidth > 500){
      document
      .getElementById("deckgl-wrapper")
      .addEventListener("contextmenu", evt => evt.preventDefault());  
    }
    var pGeojson = [];
    var plGeojson = [];
    var pengungsiGeojson = [];
    var paGeojson = [];
    const {radius,view3D,viewPolygon,viewLaporan,viewPengungsi,viewPintu} = this.state;  
    

    await axios.get(arrLinkPoint[0]).then(
      function (response) {
        pGeojson = response.data;
      }
    ).catch(function (error) {
      console.log(error);
    });

    await axios.get(arrLinkPolygon[0]).then(
      function (response) {
        plGeojson = response.data;

      }
    ).catch(function (error) {
      console.log(error);
    });
    
    await axios.get(pintuAirLink).then(
      function (response) {
        paGeojson = response.data;
      }
    ).catch(function (error) {
      console.log(error);
    });
    

       

    await axios.get(arrLinkPengungsi[0] + '4' + arrLinkPengungsi[1]).then(
      function (response) {
        pengungsiGeojson =  response.data;
      }
    ).catch(function (error) {
      console.log(error);
    });
    
    
    this.setState({pointGeojson : pGeojson});
    this.setState({polygonGeojson : plGeojson});
    this.setState({pengungsiGeojson : pengungsiGeojson});
    this.setState({pintuGeojson : paGeojson});

    this._render3DLayers(pGeojson,viewLaporan, plGeojson,radius,view3D,viewPolygon,pengungsiGeojson,viewPengungsi,paGeojson,viewPintu);
    //Resize window
    window.addEventListener('resize', this._resize);
    // this._resize();    
  }
  
  mapOnLoad(){
    
    const map = this.reactMap.getMap();

    map.on('load', function() {
      map.addControl(new mapboxgl.AttributionControl({
        customAttribution:
          '<a href="https://mapid.io/">Geomapid v2.0 | © MAPID </a>| <a href="https://www.maptiler.com/copyright/">© MapTiler </a> | <a href="https://www.openstreetmap.org/copyright">© OpenStreetMap contributors </a> | <a href="https://petabencana.id/map/jakarta">© Data Source: petabencana.id</a>'
      }))
    });
    this.loadDAS();

    
  }

  async loadDAS(){
    var dsGeojson = []
      await axios.get(arrLinkDAS[0]).then(
        function (response) {
          dsGeojson.push(response.data);
        }
      ).catch(function (error) {
        console.log(error);
      });
    // }
    await axios.get(arrLinkDAS[1]).then(
      function (response) {
        dsGeojson.push(response.data);
      }
    ).catch(function (error) {
      console.log(error);
    });
    await axios.get(arrLinkDAS[2]).then(
      function (response) {
        dsGeojson.push(response.data);
      }
    ).catch(function (error) {
      console.log(error);
    });
    await axios.get(arrLinkDAS[3]).then(
      function (response) {
        dsGeojson.push(response.data);
      }
    ).catch(function (error) {
      console.log(error);
    });

    const map = this.reactMap.getMap();
    
    var i =0;
    for(i = 0 ; i < 4 ; i ++){
      if (!map.getLayer('das' + (i +1))){
        if(arrTypeDAS[i] === 'fill'){
          map.addLayer(
            {
              'id': 'das' + (i +1),
              'type': 'fill',
              'source': {
                'type': 'geojson',
                'data': dsGeojson[i]
              },
              'paint': {
                  'fill-color': arrColorDAS[i],
                  'fill-opacity':  i === 1 ? 0.8 : 0.4
                }
              }
          )
        }
        else{
          map.addLayer(
            {
              'id': 'das' + (i +1),
              'type': 'line',
              'source': {
                'type': 'geojson',
                'data': dsGeojson[i]
              },
              'layout': {
                  'line-join': 'round',
                  'line-cap': 'round'
              },
              'paint': {
                  'line-color': arrColorDAS[i],
                  'line-width': 3,
                  'line-opacity' : i === 3 ? 0.7 : 0.4
                }
              }
          )
        }
        
      }
    }
   
   this.setState({dasGeojson : dsGeojson});    

  }
  _resize() {
    
    this._onViewStateChange({
      width: window.innerWidth,
      height: window.innerHeight
      // width : "100%",
      // height : "100%"
    }); 
    this.setState({
      width : window.innerWidth,
      height : window.innerHeight 
    })
  }
  
    _render3DLayers(pointGeojson,viewLaporan,polygonGeojson,radius,view3D,viewPolygon,pengungsiGeojson,viewPengungsi,pintuGeojson,viewPintu){

    var max = 1;
    var min = 0;
    var geojson;
    var isDifferent = false;
    try{
      geojson = pointGeojson.features ? pointGeojson.features.filter((o) => (o.properties.report_data !== null)).filter((o) => (Object.prototype.hasOwnProperty.call(o.properties.report_data,'flood_depth'))) : null;
    
    }
    catch(err){
      geojson = pointGeojson.features;
      isDifferent = true;
    }
    
    var plGeojson = polygonGeojson;
    var pgGeojson = pengungsiGeojson.features.filter((o) => (o.geometry !== null));
    
    if(geojson){
      
      max = isDifferent ? Math.max.apply(Math, geojson.map(function(o) { return o.properties['TINGGI_CM'];})) : Math.max.apply(Math, geojson.map(function(o) { return o.properties.report_data === null ? 0 : o.properties.report_data['flood_depth'];}));
      min = isDifferent ? Math.min.apply(Math, geojson.map(function(o) { return o.properties['TINGGI_CM'];})) : Math.min.apply(Math, geojson.map(function(o) { return o.properties.report_data === null?  0 : o.properties.report_data['flood_depth'];}));
    
    }
    var paGeojson = pintuGeojson.features;

    var maxPengungsi = 1;
    var minPengungsi = 0;

    maxPengungsi = Math.max.apply(Math, pgGeojson.map(function(o) { return o.properties.JIWA === null ? 0 : o.properties['JIWA'];}));
    minPengungsi = Math.min.apply(Math, pgGeojson.map(function(o) { return o.properties.JIWA === null?  0 : o.properties['JIWA'];}));
    
    
    //Color settubgs
    var myRainbow = new Rainbow();
    myRainbow.setSpectrum('#006400','yellow','#8B0000');      
    

    myRainbow.setNumberRange(0,4 - 1);
    var legendItem = []
    var j;
    for(j = 0 ; j < 4 ; j++){
      legendItem.push(convert.hex.rgb(myRainbow.colourAt(j)));       
    }
    var arr = [];    
    var i = 0;
    if(max === min){
      i = max < 10 ? 0 : max <= 70 ? 1 : max <= 150 ? 2 : 3;
    }
    this.setState({legendColor : myRainbow});
    var infoTooltip = null;
    
    if(viewLaporan && geojson){
      infoTooltip = {
        header : ['Ketinggian banjir : ',''],
        name : isDifferent ? ['TINGGI_CM','KETERANGAN'] : ['flood_depth','text'],
        content : ['CM','']
      }
      
      if(view3D){
        arr.push(
          new ColumnLayer({
            id: 1,
            data : geojson,
            diskResolution : 2000,
            opacity: 0.7,
            stroked: false,
            filled: true,
            radius : radius * 1,
            extruded: true,
            wireframe: true,
            getElevationScale : 5000,    
            elevationScale : max <= 500 ? 1 : 500/max,
            getPosition : d => d.geometry.coordinates,
            getElevation: d => d.properties.report_data?  d.properties.report_data['flood_depth'] * 10 : d.properties['TINGGI_CM'] * 10,
            getFillColor: d =>  max === min ? legendItem[i] : legendItem[(Math.round(((isDifferent ? d.properties['TINGGI_CM'] : d.properties.report_data['flood_depth'])/max)*3))] ,
            getLineColor: d => max === min ? legendItem[i] :legendItem[(Math.round(((isDifferent ? d.properties['TINGGI_CM'] : d.properties.report_data['flood_depth'])/max)*3))],        
            pickable: true,
            onHover: (info) => { this.setState({
              hoveredObject: info.object,
              pointerX: info.x,
              pointerY: info.y,
              layerType : 'laporan',
              infoTooltip : infoTooltip
            });},
            onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'laporan'})}
        })
        );
      }
      else{
        arr.push(
          new ScatterplotLayer({
            id: 1,
            data : geojson,
            pickable: true,
            opacity: 0.7,
            stroked: true,
            filled: true,
            radiusMinPixels: 1,
            radiusMaxPixels: 5000,
            lineWidthMinPixels: 1,
            getPosition: d => d.geometry.coordinates,
            getElevation : 1,
            getRadius: radius * 1,
            getFillColor: d => d.properties.report_data ? legendItem[(Math.round((d.properties.report_data['flood_depth']/max)*3))] : legendItem[(Math.round((d.properties['TINGGI_CM']/max)*3))],
            getLineColor: d => d.properties.report_data? legendItem[(Math.round((d.properties.report_data['flood_depth']/max)*3))] : legendItem[(Math.round((d.properties['TINGGI_CM']/max)*3))],               
            onHover: (info) => { this.setState({
              hoveredObject: info.object,
              pointerX: info.x,
              pointerY: info.y,
            layerType : 'laporan',
            infoTooltip : infoTooltip
            }); },
            onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'laporan'})}
          })
        )
      }

    }
  
  if(viewPolygon){
    
    infoTooltip = {
      header : ['',''],
      name : ['RW','KELURAHAN'],
      content : ['','']
    }

    if(view3D){
      arr.push(new GeoJsonLayer({
        id: 2,
        data : plGeojson,
        opacity: 1,
        stroked : true,
        filled: true ,
        fp64 : true,
        wireframe: true,
        extruded : true,
        elevationScale : (max <= 500 ) ? 1 * 10 : (500/max)  ,
        getElevation: d => Object.prototype.hasOwnProperty.call(d.properties,'state')  ? (d.properties['state'] === 1 ? 5 : d.properties['state']  === 2 ? 50 : d.properties['state'] === 3 ? 100 : 175) : (d.properties['RENDAH_CM'] - d.properties['TINGGI_CM']) /2 , 
        getFillColor: d => Object.prototype.hasOwnProperty.call(d.properties,'state')  ? legendItem[d.properties['state'] - 1] : (d.properties['RENDAH_CM'] === 151 ? legendItem[3] : d.properties['RENDAH_CM'] === 71 ? legendItem[2] : d.properties['RENDAH_CM'] === 10 ? legendItem[1] : legendItem[0]),
        getLineColor: d => Object.prototype.hasOwnProperty.call(d.properties,'state')  ? legendItem[d.properties['state'] - 1] : (d.properties['RENDAH_CM'] === 151 ? legendItem[3] : d.properties['RENDAH_CM'] === 71 ? legendItem[2] : d.properties['RENDAH_CM'] === 10 ? legendItem[1] : legendItem[0]) ,
        getLineWidth : d => 1,
        pickable: true,
        onHover: (info) => { this.setState({
          hoveredObject: info.object,
          pointerX: info.x,
          pointerY: info.y,
          layerType : 'polygon',
          infoTooltip : infoTooltip 
        }); },
        onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'data_bnpb'})}
      }));
    }else{
      arr.push(
        new GeoJsonLayer({
          id: 2,
          data : plGeojson,
          opacity: 1,
          stroked : true,
          filled: true ,
          fp64 : true,
          wireframe: true,
          extruded : true,
          elevationScale : 1  ,
          getElevation: 0.1 , 
          getFillColor: d => Object.prototype.hasOwnProperty.call(d.properties,'state')  ? legendItem[d.properties['state'] - 1] : (d.properties['RENDAH_CM'] === 151 ? legendItem[3] : d.properties['RENDAH_CM'] === 71 ? legendItem[2] : d.properties['RENDAH_CM'] === 10 ? legendItem[1] : legendItem[0]),
          getLineColor: d => Object.prototype.hasOwnProperty.call(d.properties,'state')  ? legendItem[d.properties['state'] - 1] : (d.properties['RENDAH_CM'] === 151 ? legendItem[3] : d.properties['RENDAH_CM'] === 71 ? legendItem[2] : d.properties['RENDAH_CM'] === 10 ? legendItem[1] : legendItem[0]) ,
          getLineWidth : d => 1,
          pickable: true,
          onHover: (info) => { this.setState({
            hoveredObject: info.object,
            pointerX: info.x,
            pointerY: info.y,
            layerType : 'polygon',
            infoTooltip : infoTooltip
          }); },
          onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'data_bnpb'})}


        })
      );
    }
  }

  if(viewPengungsi){

    infoTooltip = {
      header : ['Kota / Kab : ','Kecamatan : ', 'Kelurahan : ', 'Jumlah KK : ', 'Jumlah Jiwa'],
      name : ['KAB_KOT',"KECAMATAN","KELURAHAN","KK","JIWA"],
      content : ['','','',' KK', ' Jiwa']
    }
    if(view3D){
      arr.push(new ColumnLayer({
        id: 'pengungsi',
        data : pgGeojson,
        diskResolution : 2000,
        opacity: 0.7,
        stroked: false,
        filled: true,
        radius : radius * 1,
        extruded: true,
        wireframe: true,
        getElevationScale : 5000,    
        elevationScale : maxPengungsi <= 500 ? 1 : 500/maxPengungsi,
        getPosition : d => d.geometry.coordinates,
        getElevation: d => d.properties['JIWA'] * 10,
        getFillColor: d =>  legendItem[(Math.round((d.properties['JIWA']/maxPengungsi)*3))] ,
        getLineColor: d => legendItem[(Math.round((d.properties['JIwa']/maxPengungsi)*3))],        
        pickable: true,
        onHover: (info) => { this.setState({
          hoveredObject: info.object,
          pointerX: info.x,
          pointerY: info.y,
          layerType : 'pointPengungsi'
        });},
        onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'pengungsi'})}
    }))
    }
    else{
      
      arr.push(
      new ScatterplotLayer({
        id: 'pengungsi',
        data : pgGeojson,
        pickable: true,
        opacity: 0.7,
        stroked: true,
        filled: true,
        radiusMinPixels: 1,
        radiusMaxPixels: 5000,
        lineWidthMinPixels: 1,
        getPosition: d => d.geometry.coordinates,
        getElevation : 1,
        getRadius: radius * 1,
        getFillColor: d => legendItem[(Math.round((d.properties['JIWA']/maxPengungsi)*3))],
        getLineColor: d => legendItem[(Math.round((d.properties['JIWA']/maxPengungsi)*3))],               
        onHover: (info) => { this.setState({
          hoveredObject: info.object,
          pointerX: info.x,
          pointerY: info.y,
        layerType : 'pointPengungsi'
  
        }); },
        onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'pengungsi'})}

      })
    )
    }
  }

  if(viewPintu){  
    infoTooltip = {
      header : ['Nama PA : ','Ketinggian : ', 'Level Siaga : '],
      name : ['gaugenameid',"water_level","level_status"],
      content : ['',' CM','']
    }
    if(view3D){
      arr.push(new ColumnLayer({
        id: 'pintu-air',
        data : paGeojson,
        diskResolution : 2000,
        opacity: 0.7,
        stroked: false,
        filled: true,
        radius : radius * 1,
        extruded: true,
        wireframe: true,
        getElevationScale : 5000,    
        elevationScale : maxPengungsi <= 500 ? 1 : 500/maxPengungsi,
        getPosition : d => d.geometry.coordinates,
        getElevation: d => d.properties['water_level'] * 10,
        getFillColor: d => legendItem[d.properties.level_siaga === 1 ? 4 : d.properties.level_siaga === 2 ? 3 : d.properties.level_siaga === 3 ? 2 : 1] ,
        getLineColor: d => legendItem[d.properties.level_siaga === 1 ? 4 : d.properties.level_siaga === 2 ? 3 : d.properties.level_siaga === 3 ? 2 : 1],        
        pickable: true,
        onHover: (info) => { this.setState({
          hoveredObject: info.object,
          pointerX: info.x,
          pointerY: info.y,
          layerType : 'pointPengungsi',
          infoTooltip  : infoTooltip
        });},
        onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'pintu'})}


    }))
    }
    else{
      
      arr.push(
      new ScatterplotLayer({
        id: 'pintu-air',
        data : paGeojson,
        pickable: true,
        opacity: 0.7,
        stroked: true,
        filled: true,
        radiusMinPixels: 1,
        radiusMaxPixels: 5000,
        lineWidthMinPixels: 1,
        getPosition: d => d.geometry.coordinates,
        getElevation : 1,
        getRadius: radius * 1,
        getFillColor: d => legendItem[d.properties.level_siaga === 1 ? 4 : d.properties.level_siaga === 2 ? 3 : d.properties.level_siaga === 3 ? 2 : 1],
        getLineColor: d => legendItem[d.properties.level_siaga === 1 ? 4 : d.properties.level_siaga === 2 ? 3 : d.properties.level_siaga === 3 ? 2 : 1],             
        onHover: (info) => { this.setState({
          hoveredObject: info.object,
          pointerX: info.x,
          pointerY: info.y,
        layerType : 'pointPengungsi',
        infoTooltip : infoTooltip
        }); },
        onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'pintu'})}


      })
    )
    }
  }
        this.setState({layers3D : arr});
        this.setState({isDifferent : isDifferent});
        this.setState({layerSettings : {smallestName : min, largestName : max}});
        this.setState({minPengungsi : minPengungsi,maxPengungsi : maxPengungsi });

  }

  _renderTooltip() {
    const {pointerX, pointerY, hoveredObject,layerType,infoTooltip,width,isDifferent} = this.state;
    const isMobile = width <= 500;

    return (
      (hoveredObject && !isMobile ) && (
        <div  style={{top: pointerY, left: pointerX, zIndex : 13,position: 'absolute',
          margin: 8,
          padding: 4,
          background: 'rgba(0, 0, 0, 0.8)',
          color: '#fff',
          maxWidth: 300,
          fontSize: 10,
          pointerEvents: 'none',
          maxWidth : '200px'}}>
            {
              hoveredObject['properties']['image'] ? 
              
             <img src={hoveredObject['properties']['image']} width="100" height="125"/> : ''
            }        
            
            {

            infoTooltip.header.map(function(key,index) {       
              return ( 
                <div key={index}>                    
                    <div>{key}{(layerType === 'laporan' && index === 0 && !isDifferent) ? hoveredObject['properties']['report_data'][infoTooltip.name[index]] : hoveredObject['properties'][infoTooltip.name[index]]} {infoTooltip.content[index]}<br /></div>            
                </div>
                )
            })
          }
          
        </div>
      )
    );
  }

  
  _onViewStateChange({viewState}) {
    this.setState({
      viewState: {...this.state.viewState, ...viewState}
    });
  }
  changeMapStyle = (e) => {

    if(e === 'dark'){
      this.setState({mapStyle : darkMapStyle});
    }
    else if(e === 'light'){
      this.setState({mapStyle : lightMapStyle});
    }else if(e === 'basic'){
      this.setState({mapStyle : defaultMapStyle});
    }
    else{
      this.setState({mapStyle : satelliteMapStyle});      
    }
  }
  

  async changeTimePolygon(value){
    const {radius,viewLaporan,view3D,viewPolygon,pengungsiGeojson,viewPengungsi,pintuGeojson,viewPintu} = this.state;
    var  plgeojson = [];
    var pointGeojson = [];

    await axios.get(arrLinkPolygon[value]).then(
      function (response) {
        plgeojson = response.data;
      }
    ).catch(function (error) {
      console.log(error);
    });
    
    await axios.get(arrLinkPoint[value]).then(
      function (response) {
        pointGeojson = response.data;
      }
    ).catch(function (error) {
      console.log(error);
    });

    // plgeojson = plgeojson.features.filter((o) => (o.geometry.type === 'Polygon'));
    // plgeojson = {"type":"FeatureCollection", "features" : plgeojson}
    
    this.setState({ polygonGeojson: plgeojson});            
    this.setState({pointGeojson : pointGeojson});                     

    this._render3DLayers(pointGeojson,viewLaporan,plgeojson,radius,view3D,viewPolygon,pengungsiGeojson,viewPengungsi,pintuGeojson,viewPintu);
  }


  async changeTimePengungsi(value){
    const {radius,viewLaporan,view3D,viewPolygon,pointGeojson,polygonGeojson,viewPengungsi,pintuGeojson} = this.state;
    var geojson = [];
    var tgl = parseInt(value) + 4;
    await axios.get(arrLinkPengungsi[0] + tgl + arrLinkPengungsi[1]).then(
      function (response) {
        geojson = response.data;
      }
    ).catch(function (error) {
      console.log(error);
    });
    

    this.setState({pengungsiGeojson: geojson});                   

    this._render3DLayers(pointGeojson,viewLaporan,polygonGeojson,radius,view3D,viewPolygon,geojson,viewPengungsi,pintuGeojson);
  }

  _updateSettings = (name, value) => {

    const {pointGeojson,polygonGeojson,view3D,viewPolygon,pengungsiGeojson,viewPengungsi,pintuGeojson,viewPintu,viewLaporan} = this.state;
    const map = this.reactMap.getMap();
    
    if (name === 'year') {
      this.setState({year: value});
      this.changeTimePoint(value);
    }
    else if(name === 'radius'){
        this.setState({radius: value});
        this._render3DLayers(pointGeojson,viewLaporan,polygonGeojson,value,view3D,viewPolygon,pengungsiGeojson,viewPengungsi,pintuGeojson);

    } 
    else if(name === 'indexPolygon'){
      this.setState({indexPolygon: value});
      this.changeTimePolygon(value);
    }
    else if(name === 'inspect'){
        this.setState({inspectMode : value});
    }
    else if(name === 'buffer'){

      this.setState({view3D : value});    
      this._refreshMap(value,viewLaporan,viewPolygon,viewPengungsi,viewPintu);
    }
    else if(name === 'layerPolygon'){
      this.setState({viewPolygon : value});    
      
      this._refreshMap(view3D,viewLaporan,value,viewPengungsi,viewPintu);
    }
    else if(name === 'viewDAS'){
      

      if (value) {
        map.setLayoutProperty('das1', 'visibility', 'visible');
        map.setLayoutProperty('das2', 'visibility', 'visible');
        map.setLayoutProperty('das3', 'visibility', 'visible');
        map.setLayoutProperty('das4', 'visibility', 'visible');



        } else {
          map.setLayoutProperty('das1', 'visibility', 'none');
          map.setLayoutProperty('das2', 'visibility', 'none');
          map.setLayoutProperty('das3', 'visibility', 'none');
          map.setLayoutProperty('das4', 'visibility', 'none');
      }

      this.setState({viewDAS : value});    
    }
    else if(name === 'indexPengungsi'){
      this.setState({indexPengungsi: value});
      this.changeTimePengungsi(value);
    }
    else if(name === 'viewPengungsi'){

      this.setState({viewPengungsi : value});    
      this._refreshMap(view3D,viewLaporan,viewPolygon,value,viewPintu);
    }
    else if(name === 'viewPintu'){
      // if (value) {
      //   map.setLayoutProperty('pintu-air', 'visibility', 'visible');


      //   } else {
      //     map.setLayoutProperty('pintu-air', 'visibility', 'none');
      // }

      this.setState({viewPintu : value});    
      this._refreshMap(view3D,viewLaporan,viewPolygon,viewPengungsi,value);
    }
    else if(name === 'viewLaporan'){
      this.setState({viewLaporan : value});    
      this._refreshMap(view3D,value,viewPolygon,viewPengungsi,viewPintu);
    }

  };

  _refreshMap(view3D,viewLaporan,viewPolygon,viewPengungsi,viewPintu){
    this.setState({layers3D: []});
    const {pointGeojson,polygonGeojson,radius,dasGeojson,pengungsiGeojson,pintuGeojson} = this.state;
  
    setTimeout(() => {
      this._render3DLayers(pointGeojson,viewLaporan,polygonGeojson,radius,view3D,viewPolygon,pengungsiGeojson,viewPengungsi,pintuGeojson,viewPintu);
    },5
     );
     this.loadDAS(dasGeojson);
     this._resize();
  }


 
  _onViewStateChangeDeck({viewState}) {
    this.setState({
      viewStateDeck: {...this.state.viewStateDeck, ...viewState}
    });


  }

  render() {
    
    const {viewState,layers3D,legendColor,viewPolygon,view3D,viewDAS,inspectMode,mapStyle,viewPengungsi,viewPintu,popupX,popupY} = this.state;    
    const {scaleCount,isPoint,viewBuffer,visualParams,dataColumn,selectedMode,year,layerSettings,radius,indexPolygon,width,minPengungsi,maxPengungsi,indexPengungsi,viewLaporan,showPopup} = this.state;
    const {popupInfo,popupProps,popupType,isDifferent} = this.state;
    var layerName = '';
    const isMobile = width <= 500;
    
    return (
      <div style={{height: "100%", width:"100%"}}
      >
        <div style={{position: 'absolute', top: 15, left: 10,zIndex : 14}}>
          <a href="https://geo.mapid.io">
          <img src="https://geomapid2019.s3-us-west-2.amazonaws.com/logo.svg" width="35" height="35" alt="Go to Dashboard"/>
          </a>
        </div>
                  <div className="btn-map-style">
        <Button.Group>
        <Button icon onClick={() => this._refreshMap(view3D,viewLaporan,viewPolygon,viewPengungsi,viewPintu)}>
          <Icon name='refresh' />
        </Button>
            <Button value='basic' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Street</Button>
       {(!isMobile) &&
        <div>
        <Button value='light' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Light</Button>
        <Button value='dark' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Dark</Button>      
        </div>
      }

            <Button value='satellite' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Satellite</Button>   
      </Button.Group> 
        </div>
      

        {
          legendColor &&

          <ControlBanjir
          // containerComponent={this.props.containerComponent}
          settings={{
            scaleCount,
            isPoint,
            viewBuffer,
            visualParams,
            dataColumn,
            selectedMode,
            inspectMode,
            year,
            legendColor,
            layerSettings,
            radius,
            layerName,
            indexPolygon,
            arrPolygonDate,
            isMobile,
            viewPolygon,
            viewDAS,
            arrColorDAS,
            minPengungsi,
            maxPengungsi,
            indexPengungsi,
            arrPengungsiDate,
            viewPengungsi,
            viewPintu,
            viewLaporan
          }}
          onChange={this._updateSettings}
          style={{overflow: 'auto'}}
          ref={refMap}
          /> 
        }

      <div style={{height: "100vh", width:"100vw"}} id="map-render">
        {/* <ReactMapGL
          mapStyle={mapStyle}
          mapboxApiAccessToken={MAPBOX_TOKEN}
          {...viewState}
          // refMap={refMap}
          ref={(reactMap) => this.reactMap = reactMap}
          preserveDrawingBuffer={true}
          attributionControl={false}
          onLoad={() => {this.mapOnLoad()}}
          customAttribution={'<a href="https://mapid.io/">Geomapid v2.0 | © MAPID </a>| <a href="https://www.maptiler.com/copyright/">© MapTiler </a> | <a href="https://www.openstreetmap.org/copyright">© OpenStreetMap contributors </a> | <a href="https://petabencana.id/map/jakarta">© Data Source: petabencana.id</a>'}
      >

            <DeckGL initialViewState={viewState}
            controller={true}
            layers={layers3D}
            {...viewState}
          // onViewStateChange={this._onViewStateChange.bind(this)}
          onViewStateChange={this._onViewStateChange.bind(this)}
          glOptions={{preserveDrawingBuffer: true}}
            >
              
          { inspectMode && this._renderTooltip() }
        </DeckGL>
          
      
    
      </ReactMapGL> */}

      <DeckGL 
      initialViewState={viewState}
      controller={true}
      layers={layers3D}
      viewState = {viewState}
      onViewStateChange={(viewState) => this._onViewStateChange(viewState)}
      ContextProvider={MapContext.Provider}

      glOptions={{preserveDrawingBuffer: true}}
      >
          
 
  <ReactMapGL
    mapStyle={mapStyle}
    mapboxApiAccessToken={MAPBOX_TOKEN}
    preserveDrawingBuffer={true}
    ref={(reactMap) => this.reactMap = reactMap}

    // refMap={refMap}
    onLoad={() => {this.mapOnLoad()}}
  >


 <div style={{position : 'absolute',right : 10,top : 20, zIndex : 15}}>
        <NavigationControl 
      onViewStateChange={(viewState) => this._onViewStateChange(viewState)}
      showCompass={true}
      captureDrag={true}
      captureScroll={true}
  />
        </div>
{
  ( showPopup && inspectMode) && 
   <Popup
 latitude={popupX}
 longitude={popupY}
 closeButton={true}
 closeOnClick={false}
 onClose={() => this.setState({showPopup: false})}
 anchor="top" 
 style={
 {
 position: 'absolute',
 margin: 8,
 padding: 4,
 background: 'rgba(0, 0, 0, 0.8)',
 color: '#fff',
 maxWidth: 300,
 fontSize: 10
   }
 }
 >
    <div> 
    {
              popupInfo['properties']['image'] ? 
              
             <img src={popupInfo['properties']['image']} width="100" height="125"/> : ''
            }          
   {
   popupProps.header.map(function(key,index) {       
    console.log(); 
    return ( 
       <div key={index}>
           <div>{key}{(popupType === 'laporan' && index === 0 && !isDifferent ) ? popupInfo['properties']['report_data'][popupProps.name[index]] : popupInfo['properties'][popupProps.name[index]]} {popupProps.content[index]}<br /></div>            
       </div>
       )
   })
 }
   </div>
 </Popup>

 }

</ReactMapGL>
        
    { inspectMode && this._renderTooltip() }
   
  </DeckGL>
    </div>
</div>
    )}  
}
