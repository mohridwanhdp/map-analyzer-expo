import React, {Component} from 'react';
import ReactMapGL, { NavigationControl, FlyToInterpolator} from 'react-map-gl';
import { Button,Icon } from 'semantic-ui-react';
import {json as requestJson} from 'd3-request';
import {defaultMapStyle, darkMapStyle, lightMapStyle, satelliteMapStyle} from './style/map-style.js';
import LayerInfoPolice from './LayerInfoPolice.js';
import DeckGL from 'deck.gl';
import {scaleThreshold} from 'd3-scale';
import {LightingEffect, AmbientLight, _SunLight as SunLight} from '@deck.gl/core';
import DATA from './data/police.geojson';
import ControlPanel from './ControlPanel.js'
import {ColumnLayer} from '@deck.gl/layers';
const TOKEN = 'pk.eyJ1IjoibW9ocmlkd2FuaGRwIiwiYSI6ImNrMDRvaGwzdDA0dDkzaG9tNnA3dmViODQifQ.qa1W5PaDaHzLnqIXbAM7zw';
// const LayerURL = 'https://hnjp62bwxh.execute-api.us-west-2.amazonaws.com/GeoDev/getprojectbyid?_id=5d4b878691552812234f7c29'
const navStyle = {
  position: 'absolute',
  top: 36,
  right: 0,
  padding: '10px',
  zIndex : 9
};

export const COLOR_SCALE = scaleThreshold()
  .domain([0, 0.2, 0.4, 0.6, 0.8,1.0])
  .range([
    [0, 112, 0],
    [35, 136, 35],
    [96,136,0],
    [255, 191, 0],
    [255,129,0],    
    [210,34,45]
  ]);

const ambientLight = new AmbientLight({
  color: [255, 255, 255],
  intensity: 1.0
});

const dirLight = new SunLight({
  timestamp: Date.UTC(2019, 7, 1, 11),
  color: [255, 255, 255],
  intensity: 1.0,
  _shadow: true
});


class MapPolice extends Component {
  constructor(props) {
		super(props);
		this.state = {
      mapStyle: defaultMapStyle,
      viewport: {
      width : "100%",
      height : 800,
      latitude: -6.175110,
      longitude: 106.865036,
      zoom: 12
    },
    viewState : {
      latitude: -6.175110,
      longitude: 106.865036,
      zoom: 12
    },
      dataLayers : null,
      geoJSONLayers : [],
      view3D : true,
      year : "2016",
      yearData : null,
      layerInfo : null,
      layerInfoVisibility : true,
      layer3D : null,
      hoveredObject : null, pointerX : null, pointerY : null,
      visualParams : []
    };
    
    this._loadData = this._loadData.bind(this); 
    this.changeLayerVisibility = this.changeLayerVisibility.bind(this);
    this.changeLayerInfoVisibility = this.changeLayerInfoVisibility.bind(this);
    this.changeDimensionalView = this.changeDimensionalView.bind(this);
    this._onViewStateChange = this._onViewStateChange.bind(this);
    this._refreshMap = this._refreshMap.bind(this);
    const lightingEffect = new LightingEffect({ambientLight, dirLight});
    lightingEffect.shadowColor = [0, 0, 0, 0.5];
    this._effects = [lightingEffect];
  }
  
  loadJson(){
    //For 2D Layers
    requestJson(DATA, (error, response) => {
      if (!error) {
        this._loadData(response);    
        this.setState({yearData : response});
        this._updateSettings('year',this.state.year);
        this._render3DLayers(this.state.year);
      }
    });
  }
   componentDidMount() {
    const map = this.reactMap.getMap();
    map.on('load',f =>(
      this.loadJson()
    ));
    
    map.doubleClickZoom.disable();
    //Resize window
    window.addEventListener('resize', this._resize.bind(this))
    this._resize()
  }
 
  _resize() {
    this._onViewportChange({
      width: window.innerWidth,
      height: window.innerHeight
    }); 
  }

  _onViewportChange = (viewport) => {
    this.setState({
      viewport: { ...this.state.viewport, ...viewport },
      viewState: {...this.state.viewState, ...viewport}
    })
  }
  _onViewStateChange({viewState}) {
    this.setState({viewState});
  }
  changeLayerVisibility(layer){
    const map = this.reactMap.getMap();
    var visibility  = map.getLayoutProperty(layer, 'visibility');; 

    if (visibility === 'visible') {
      map.setLayoutProperty(layer, 'visibility', 'none');
      } else {
      map.setLayoutProperty(layer, 'visibility', 'visible');
      }
  }

  //Method for layer info
  changeLayerInfoVisibility(layer){
    const vis = this.state.layerInfoVisibility;
    this.setState({layerInfoVisibility : !vis});
    
  }

  //3D Map Handler
  changeDimensionalView(){
    const stat = this.state.view3D;
 
    if(stat){
      // viewport.pitch = 0;
      this.setState({view3D : false});
      this.setState({layer3D : null});

      
    } else{
      // viewport.pitch = 60;
      this._render3DLayers(this.state.year);
      this.setState({view3D : true}); 
    }
  }
  

  _render3DLayers(year) {
    var data = this.state.dataLayers;
    var arr = [];   

    console.log(JSON.stringify(data));
    arr = data.map( (f) => (
      new ColumnLayer({
            id: f.id,
            data : f.geojson.features,
            diskResolution : 1000,
            opacity: 1,
            stroked: false,
            filled: true,
            radius : 50,
            extruded: true,
            wireframe: true,
            getElevationScale : 5000,
            getPosition : d => d.geometry.coordinates,
            getElevation: d => d.properties.yearCount[year] * 10,
            getFillColor: d => COLOR_SCALE(d.properties.yearCount[year]/600),
            getLineColor: d => COLOR_SCALE(d.properties.yearCount[year]/600),
            pickable: false,
            updateTriggers: {
              getElevation: [this.state.layer3D],
              getFillColor: [this.state.layer3D],
              getLineColor: [this.state.layer3D],

            }
          })
    ));

    this.setState({layer3D : arr});
  }


  //Event when button map style pressed
  changeMapStyle = (e) => {
    console.log('You press change map style ' + e);
    if(e === 'dark'){
      this.setState({mapStyle : darkMapStyle});
    }
    else if(e === 'light'){
      this.setState({mapStyle : lightMapStyle});
    }else if(e === 'basic'){
      this.setState({mapStyle : defaultMapStyle});
    }
    else{
      this.setState({mapStyle : satelliteMapStyle});      
    }
  }


  _loadData = dataRes => {   
    const dataLayers = dataRes.layers;
    const map = this.reactMap.getMap();
    var i;

        for(i in dataLayers){
          if (!map.getLayer(dataLayers[i]._id)){
            map.addLayer(              
              {
                'id': dataLayers[i]._id,
                'type': 'symbol',
                'source': {
                  'type': 'geojson',
                  'data': dataLayers[i].geojson
                }
                ,'layout': {
                  'visibility': 'visible',
                  'icon-image': 'police'
                }
              }
            );
                
          }    
        }

    /**
     * If you want to add draw component to map, just uncomment two lines below
     */
    // var Draw = new MapboxDraw();
    // map.addControl(Draw, 'top-right');

    this.setState({dataLayers : dataLayers});      
  }

  
  _onMouseMove(evt) {
    if (evt.nativeEvent) {
      this.setState({mousePosition: [evt.nativeEvent.offsetX, evt.nativeEvent.offsetY]});
    }
  }

  _onMouseEnter() {
    this.setState({mouseEntered: true});
  }

  _onMouseLeave() {
    this.setState({mouseEntered: false});
  }
  


  //Year Control Panel
  _updateSettings = (name, value) => {
    if (name === 'year') {
      this.setState({year: value});
      this._render3DLayers(value);
    }
  };

  _refreshMap(){
    console.log('refresh clicked');
    this.setState({view3D : false});
    setTimeout(() => {
      this._render3DLayers(this.state.year);
      this.setState({view3D : true});
    },100
     );
  }


  _goTo = (l) => {

    var longitude = l[0];
    var latitude = l[1];
   
    this.setState({view3D : false});
    
    this._onViewportChange({
      longitude,
      latitude,
      zoom: 14,
      transitionInterpolator: new FlyToInterpolator(),
      transitionDuration: 1500
    });

    setTimeout(() => {
      this._render3DLayers(this.state.year);
      this.setState({view3D : true});
    },1500
     );
    
  }

  _renderTooltip() {
    const {hoveredObject, pointerX, pointerY} = this.state || {};
    return hoveredObject && (
      <div style={{position: 'absolute', zIndex: 9, pointerEvents: 'none', left: pointerX, top: pointerY}}>
        { hoveredObject.message }
      </div>
    );
  }


  addVisualParam(val){
    var arr = this.state.visualParams;
    arr.push(val);
    arr.sort();

    this.setState({visualParams : arr});
  }

  deleteVisualParam(val){
    var arr = this.state.visualParams;
    
    arr = arr.filter(function(ele){
      return ele != val;
    });
    this.setState({visualParams : arr});


  }

  render() {
    const { 
        viewport,mapStyle,layerInfoVisibility, view3D,layerInfo,layer3D,viewState} = this.state

      return (
        
        <div  style={{height: "100%", width:"100%"}}>
       
        <div className="btn-map-style">
        <Button.Group>
        <Button icon onClick={() => this._refreshMap()}>
          <Icon name='refresh' />
        </Button>
            <Button value='basic' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Street</Button>
        <Button value='light' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Light</Button>
            <Button value='dark' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Dark</Button>
            <Button value='satellite' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Satellite</Button>   
      </Button.Group> 
        </div>
       
        <ControlPanel
        containerComponent={this.props.containerComponent}
        settings={this.state}
        onChange={this._updateSettings}
        /> 
        <LayerInfoPolice 
        visibility={layerInfoVisibility}
        layerInfo={layerInfo}
        goTo = {this._goTo}        
        add = {this.addVisualParam}
        delete = {this.deleteVisualParam}
        />
        <ReactMapGL
        ref={(reactMap) => this.reactMap = reactMap}
        mapboxApiAccessToken={TOKEN}
          {...viewport}
          onViewportChange={this._onViewportChange.bind(this)}
          // onViewStateChange={this._onViewStateChange.bind(this)}
          // doubleClickZoom={false}
          mapStyle={mapStyle}
          mapRef={this.mapRef}
          onHover = {this._onHover}
          > 
         
         {
            view3D && 
            <DeckGL
          layers={layer3D}
          effects={this._effects}
          initialViewState={viewState}
          {...viewState}
          onViewStateChange={this._onViewStateChange.bind(this)}
          controller={true}
          >
            {this._renderTooltip()}
            </DeckGL>
      }
          <div className="nav" style={navStyle}>
      <NavigationControl 
      />
    </div>
    
        </ReactMapGL>
        
        {
          // dataLayers &&
          
          //   <div>
          // <LayerBox data={{dataLayers}}
          // changeVisibility={this.changeLayerVisibility}
          // changeInfo={this.changeLayerInfoVisibility}
          // changeDimension = {this.changeDimensionalView}
          // view3D= {view3D}
          // zoom = {this.zoomBound}
          // />
          
          // </div>
        }  
        
         </div>
    );
  }
}
export default MapPolice;