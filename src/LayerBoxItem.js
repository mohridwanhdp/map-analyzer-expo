import React, { PureComponent } from 'react';
import {Card,Button, Icon} from 'semantic-ui-react';
import axios from 'axios';
import { geometry } from '@turf/helpers';

export default class LayerBoxItem extends PureComponent{

    constructor(props){
        super(props);
        this.state = {
            btnState : false,
            layer : props.layer
        };
    }
    onClickView(key){
        this.props.changeVisibility(key);
        (this.state.btnState) ? this.setState({btnState : false}) : this.setState({btnState : true});
    }

    async onClickButtonName(geojson){
        
        var gj = '';
        await axios.get(geojson).then(
            function (response) {
              gj = response.data;
            }
          ).catch(function (error) {
            console.log(error);
        });

        // var arr = gj.features.map((f) => (
        //     f.geometry.coordinates
        // ));

        console.log(gj);
        this.props.fitBounds(gj);

    }

    async getGeojson(geojson){
       

        //   return gj;
    }
    render(){
        const {layer,btnState} = this.state;
        return (
            <Card key={layer._id}>
                <Card.Content>
                    <Card.Header>
                        <Button onClick={() => this.onClickButtonName(layer.geojson)}>{layer.name}</Button>
                    </Card.Header>                    
                    <div>
                        Layer type : {layer.layer_type}               
                    </div>
                    <div>
                    {layer.description}
                    </div>                    
                </Card.Content>
                <Card.Content extra>
                    <div>
                        <Button icon basic color='green' onClick={() => this.onClickView(layer._id)}>
                            <Icon name={btnState ? 'eye' : 'eye slash'} />
                        </Button>
                        <Button icon basic color='teal' onClick={() => this.props.changeInfo(layer._id)}>
                            <Icon name='table' />
                        </Button>
                    </div>
                </Card.Content>
            </Card>
        );
    }
        
}

