import React, {Component} from 'react';
import ReactMapGL, { NavigationControl} from 'react-map-gl';
import { Button,ButtonToolbar } from 'react-bootstrap';
import {json as requestJson} from 'd3-request';
import {defaultMapStyle, darkMapStyle, lightMapStyle, satelliteMapStyle,dataLayer} from './style/map-style.js';
import LayerBox from './LayerBox.js';
import LayerInfo from './LayerInfo.js';
import {fromJS} from 'immutable';
import DeckGL, { GeoJsonLayer,PolygonLayer} from 'deck.gl';
import MapGLDraw, { EditorModes } from 'react-map-gl-draw';
import MapboxDraw from '@mapbox/mapbox-gl-draw';
import {scaleThreshold} from 'd3-scale';
import {LightingEffect, AmbientLight, _SunLight as SunLight} from '@deck.gl/core';
import DATA from './data/example.geojson';
import YEARLY_DATA from './data/yearexample.geojson';
import ControlPanel from './ControlPanel.js'
// import LayerURL from './data/yearproject.geojson'

const TOKEN = 'pk.eyJ1IjoibW9ocmlkd2FuaGRwIiwiYSI6ImNrMDRvaGwzdDA0dDkzaG9tNnA3dmViODQifQ.qa1W5PaDaHzLnqIXbAM7zw';
const LayerURL = 'https://hnjp62bwxh.execute-api.us-west-2.amazonaws.com/GeoDev/getprojectbyid?_id=5d4b878691552812234f7c29'
const navStyle = {
  position: 'absolute',
  top: 36,
  right: 0,
  padding: '10px',
  zIndex : 9
};

export const COLOR_SCALE = scaleThreshold()
  .domain([-0.6, -0.45, -0.3, -0.15, 0, 0.15, 0.3, 0.45, 0.6, 0.75, 0.9, 1.05, 1.2])
  .range([
    [65, 182, 196],
    [127, 205, 187],
    [199, 233, 180],
    [237, 248, 177],
    // zero
    [255, 255, 204],
    [255, 237, 160],
    [254, 217, 118],
    [254, 178, 76],
    [253, 141, 60],
    [252, 78, 42],
    [227, 26, 28],
    [189, 0, 38],
    [128, 0, 38]
  ]);


const ambientLight = new AmbientLight({
  color: [255, 255, 255],
  intensity: 1.0
});

const dirLight = new SunLight({
  timestamp: Date.UTC(2019, 7, 1, 22),
  color: [255, 255, 255],
  intensity: 1.0,
  _shadow: true
});


class MapPolice extends Component {
  constructor(props) {
		super(props);
		this.state = {
      mapStyle: defaultMapStyle,
      viewport: {
      width : "100%",
      height : 800,
      latitude: -6.964228,
      longitude: 107.563213,
      zoom: 5
    },
    viewState : {
      latitude: -6.964228,
      longitude: 107.563213,
      zoom: 5
    },
      dataLayers : null,
      geoJSONLayers : [],
      selectedMode: EditorModes.READ_ONLY,
      features: [],
      selectedFeatureId: null,
      layerInfoVisibility : true,
      view3D : false,
      year : "2016",
      yearData : null,
      layerInfo : null,
      layer3D : null
    };
    
    this._loadData = this._loadData.bind(this); 
    this.changeLayerVisibility = this.changeLayerVisibility.bind(this);
    this.changeLayerInfoVisibility = this.changeLayerInfoVisibility.bind(this);
    this.changeDimensionalView = this.changeDimensionalView.bind(this);
    this._onViewStateChange = this._onViewStateChange.bind(this);
    this.zoomBound = this.zoomBound.bind(this);
    const lightingEffect = new LightingEffect({ambientLight, dirLight});
    lightingEffect.shadowColor = [0, 0, 0, 0.5];
    this._effects = [lightingEffect];
  }
  
  loadJson(){
    //For 2D Layers
    requestJson(LayerURL, (error, response) => {
      if (!error) {
        console.log(JSON.stringify(response));
        this._loadData(response);
      }
    });

    //For 3D layers and info
    requestJson(YEARLY_DATA, (error, response) => {
      if (!error) {
        this.setState({yearData : response});
        this._updateSettings('year',this.state.year);
        this._render3DLayers(this.state.year);
      }
    });
   
  }
   componentDidMount() {
   this.loadJson();

    
    //Resize window
    window.addEventListener('resize', this._resize.bind(this))
    this._resize()
  }
 
  _resize() {
    this._onViewportChange({
      width: window.innerWidth,
      height: window.innerHeight
    })
  }

  _onViewportChange = (viewport) => {
    this.setState({
      viewport: { ...this.state.viewport, ...viewport },
      viewState: {...this.state.viewState, ...viewport}
    })
  }
  _onViewStateChange({viewState}) {
    this.setState({viewState});
  }
  changeLayerVisibility(layer){
    const map = this.reactMap.getMap();
    var visibility  = map.getLayoutProperty(layer, 'visibility');; 

    if (visibility === 'visible') {
      map.setLayoutProperty(layer, 'visibility', 'none');
      } else {
      map.setLayoutProperty(layer, 'visibility', 'visible');
      }
  }

  //Method for layer info
  changeLayerInfoVisibility(layer){
    const vis = this.state.layerInfoVisibility;
    this.setState({layerInfoVisibility : !vis});
    
  }

  //3D Map Handler
  changeDimensionalView(){
    const stat = this.state.view3D;
 
    if(stat){
      // viewport.pitch = 0;
      this.setState({view3D : false});
      
    } else{
      // viewport.pitch = 60;
      this._render3DLayers(this.state.year);
      this.setState({view3D : true}); 
    }
  }

  _render3DLayers(year) {
    
    
    var data = this.state.dataLayers;
    if(data){
        var arr = [];    

        arr = data.map( (f) => (
          new GeoJsonLayer({
                id: 'example',
                data : f.geojson,
                opacity: 0.8,
                stroked: false,
                filled: true,
                extruded: true,
                wireframe: true,
                getElevation: f => Math.sqrt(f.properties.value[year].valuePerSqm) * 10,
                getFillColor: f => COLOR_SCALE(f.properties.value[year].growth),
                getLineColor: [255, 255, 255],
                pickable: false,
                updateTriggers: {
                  getElevation: [this.state.layer3D],
                  getFillColor: [this.state.layer3D]
                }
              })
        ));
    
    }
 
    // arr =  [
    //   // new PolygonLayer({
    //   //   id: 'ground',
    //   //   data: JKT,
    //   //   stroked: false,
    //   //   getPolygon: f => f,
    //   //   getFillColor: [0, 0, 0, 0]
    //   // }),
    //   new GeoJsonLayer({
    //     id: 'example',
    //     data : YEARLY_DATA,
    //     opacity: 0.8,
    //     stroked: false,
    //     filled: true,
    //     extruded: true,
    //     wireframe: true,
    //     getElevation: f => Math.sqrt(f.properties.value[year].valuePerSqm) * 10,
    //     getFillColor: f => COLOR_SCALE(f.properties.value[year].growth),
    //     getLineColor: [255, 255, 255],
    //     pickable: false,
    //     updateTriggers: {
    //       getElevation: [this.state.layer3D],
    //       getFillColor: [this.state.layer3D]
    //     }
    //   })
    // ];
   
    this.setState({layer3D : arr});
  }


  //Event when button map style pressed
  changeMapStyle = (e) => {
    console.log('You press change map style ' + e);
    if(e == 'dark'){
      this.setState({mapStyle : darkMapStyle});
    }
    else if(e == 'light'){
      this.setState({mapStyle : lightMapStyle});
    }else if(e == 'streets'){
      this.setState({mapStyle : defaultMapStyle});
    }
    else{
      this.setState({mapStyle : satelliteMapStyle});      
    }
  }

  getPaintProperties(layer){
    var type = layer.layer_type;
    console.log(layer.name + " : " + type);
    if(type == 'symbol'){
        return { 
            "icon-image": "information"
        };
    }
    else if(type == 'fill'){
        return { 
            "fill-color": "information",
            "fill-opacity" : 0.8
        };
    }
    else{

    }
  }
  _loadData = dataRes => {   
    const dataLayers = dataRes.layers;
    const map = this.reactMap.getMap();

    var i;
    if(map){
        for(i in dataLayers){
            if (!map.getLayer(dataLayers[i]._id)){
              map.addLayer(              
                {
                  'id': dataLayers[i]._id,
                  'type': dataLayers[i].layer_type,
                  'source': {
                    'type': 'geojson',
                    'data': dataLayers[i].geojson
                  }
                  ,'layout': {
                    'visibility': 'visible'
                  },
                  'paint' : this.getPaintProperties(dataLayers[i])
                  
                }
              );
                  
            }
            console.log(JSON.stringify(this.getPaintProperties(dataLayers[i])));    
          }
    }

    /**
     * If you want to add draw component to map, just uncomment two lines below
     */
    // var Draw = new MapboxDraw();
    // map.addControl(Draw, 'top-right');

    this.setState({dataLayers : dataLayers});      
  }

  
  _onMouseMove(evt) {
    if (evt.nativeEvent) {
      this.setState({mousePosition: [evt.nativeEvent.offsetX, evt.nativeEvent.offsetY]});
    }
  }

  _onMouseEnter() {
    this.setState({mouseEntered: true});
  }

  _onMouseLeave() {
    this.setState({mouseEntered: false});
  }
  

  //DRAW METHOD
  _switchMode = evt => {
    const selectedMode = evt.target.id;
    this.setState({
     selectedMode: selectedMode === this.state.selectedMode ? null : selectedMode
    });
  };
 

  // _renderToolbar = () => {
  //   return (
  //     <div style={{position: 'absolute', top: 0, right: 0, maxWidth: '320px',zIndex : 9}}>
  //       <select onChange={this._switchMode}>
  //         <option value="">--Please choose a mode--</option>
  //         {MODES.map(mode => <option value={mode.id}>{mode.text}</option>)}
  //       </select>
  //     </div>
  //   );
  // };

  //Year Control Panel
  _updateSettings = (name, value) => {
    if (name === 'year') {
      this.setState({year: value});
      this._render3DLayers(value);
    }
  };


  zoomBound(id){
    console.log(" Layer with " + id + " Clicked");
    // const feature = event.features[0];
    // if (feature) {
    //   // calculate the bounding box of the feature
    //   const [minLng, minLat, maxLng, maxLat] = bbox(feature);
    //   // construct a viewport instance from the current state
    //   const viewport = new WebMercatorViewport(this.state.viewport);
    //   const {longitude, latitude, zoom} = viewport.fitBounds([[minLng, minLat], [maxLng, maxLat]], {
    //     padding: 40
    //   });

    //   this.setState({
    //     viewport: {
    //       ...this.state.viewport,
    //       longitude,
    //       latitude,
    //       zoom,
    //       transitionInterpolator: new LinearInterpolator({
    //         around: [event.offsetCenter.x, event.offsetCenter.y]
    //       }),
    //       transitionDuration: 1000
    //     }
    //   });
    // }
  }
  render() {
    const { 
        viewport,mapStyle,dataLayers,layerInfoVisibility, view3D,layerInfo,layer3D,viewState} = this.state

      return (
        
        <div  style={{height: "100%", width:"100%"}}>
       
        <div className="btn-map-style">
        <ButtonToolbar>
          <Button value='light' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Light</Button>
            <Button value='dark' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Dark</Button>
            <Button value='basic' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Street</Button>
            <Button value='satellite' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Satellite</Button>         
        </ButtonToolbar>
        </div>
       
        <ControlPanel
        containerComponent={this.props.containerComponent}
        settings={this.state}
        onChange={this._updateSettings}
        /> 
        <LayerInfo 
        visibility={layerInfoVisibility}
        layerInfo={layerInfo}
        />
        <ReactMapGL
        ref={(reactMap) => this.reactMap = reactMap}
        mapboxApiAccessToken={TOKEN}
          {...viewport}
          onViewportChange={this._onViewportChange.bind(this)}
          mapStyle={mapStyle}
          mapRef={this.mapRef}
          onHover = {this._onHover}
          > 
          {
            view3D && 
            <DeckGL
          layers={layer3D}
          effects={this._effects}
          initialViewState={viewState}
          {...viewport}
          controller={true}
          // onViewportChange={this._onViewportChange.bind(this)}
          onViewStateChange={this._onViewStateChange}
          />
      }
       
          <div className="nav" style={navStyle}>
      <NavigationControl />
    </div>
    
    
      
   
        </ReactMapGL>
        
        {
          dataLayers &&
          
            <div>
          <LayerBox data={{dataLayers}}
          changeVisibility={this.changeLayerVisibility}
          changeInfo={this.changeLayerInfoVisibility}
          changeDimension = {this.changeDimensionalView}
          view3D= {view3D}
          zoom = {this.zoomBound}
          />
          
          </div>
        }  
        
         </div>
    );
  }
}
export default MapPolice;