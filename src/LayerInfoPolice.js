import React, { PureComponent } from 'react';
import {Table} from 'semantic-ui-react';
import DATA from './data/police.geojson';
import {json as requestJson} from 'd3-request';
import {Button,Checkbox,Radio} from 'semantic-ui-react';


const LoadingSpinner = () => (
  <div style={{height: "100%", width:"100%",backgroundColor : '#000',zIndex :9}}>
    <i className="fa fa-spinner fa-spin" />
  </div>
);
class LayerInfoPolice extends PureComponent {
    
    constructor(props){
        super(props);
        
        this.state = {
          features : null,
          isLoading : true
        };

        this.loadJson = this.loadJson.bind(this);
    }

    componentDidMount(){
     this.loadJson();
  }

    loadJson(){
      requestJson(DATA, (error, response) => {
        if (!error) {
          this.setState({features : response.layers[0].geojson.features});
        }
        else{
            JSON.stringify(error);
        }
      });
    }

   goTo(f){
      console.log(JSON.stringify(f));
      this.props.goTo(f.geometry.coordinates);
    }
    
    add(val){
      console.log(val);
    }

    render() {
      const {features} = this.state;  
      return (


            <div className={this.props.visibility ? 'layer-info' : 'layer-info-hidden'}>
           
            <Table celled structured>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell rowSpan='2'> <center>Name</center></Table.HeaderCell>
                    <Table.HeaderCell colSpan='4'><center> Total reports /year </center></Table.HeaderCell>                    
                  </Table.Row>
                  <Table.Row>
                  <Table.HeaderCell><center><Checkbox
                        label='2015'
                        name='radioGroup'
                        value='2015'
                        onClick={(e) => this.add(e.target.value)}
                        /></center></Table.HeaderCell>
                   <Table.HeaderCell><center><Checkbox
                        label='2016'
                        name='radioGroup'
                        onClick={(e) => this.add(e.target.checked)}
                        value='2016'/></center></Table.HeaderCell>
                    <Table.HeaderCell><center><Checkbox
                        label='2017'
                        name='radioGroup'
                        onClick={(e) => this.add(e.target.checked)}
                        value='2017'/></center></Table.HeaderCell>
                    <Table.HeaderCell><center><Checkbox
                        label='2018'
                        onClick={(e) => this.add(e.target.checked)}
                        name='radioGroup'
                        value='2018'/></center></Table.HeaderCell>                
                
                </Table.Row>
                </Table.Header>
              { features && 
                
                <Table.Body>
                 {
                   features.map((f) =>(
                   <Table.Row>
                      
                    <Table.Cell>
                       <Button content={f.properties.NAMA} basic onClick={() => this.goTo(f)} />
                    </Table.Cell>
                    <Table.Cell>
                    {f.properties.yearCount["2015"]}                    
                    </Table.Cell>
                    <Table.Cell>
                    {f.properties.yearCount["2016"]}
                    
                    </Table.Cell>
                    <Table.Cell>
                    {f.properties.yearCount["2017"]}
                    
                    </Table.Cell>
                    <Table.Cell>
                    {f.properties.yearCount["2018"]}
                    
                    </Table.Cell>
                    </Table.Row>
                   ))  
                
                 }
                </Table.Body>
              }
                
              </Table>
              </div>
            );
        }
    }

export default LayerInfoPolice;