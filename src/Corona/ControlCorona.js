import React, {PureComponent} from 'react';
import { Button,Icon,Checkbox, Divider} from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css'

const mobileStyle = {
  left : 0,
  bottom :0,
  height: "27.5vh",
  width : "100vw",
  position: 'absolute',
  overflow: 'auto',
  background: '#fff',
  padding: 10,
  fontSize: 13,
  color: '#6b6b76',
  outline: 'none',
  textTransform: 'uppercase',
  zIndex: 4
};

const settingsButtonStyle = {
  position: 'absolute',
  top : 0,
  right : '-10px',
  margin : [10,0,0,0],
  background : 'white'
}

const buttonViewStyle = {
  position : 'absolute',right : 0,top : 0,backgroundColor : 'rgba(0,0,0,0)'
}
const defaultContainer = ({children}) => <div className="control-panel" id="control-panel" style={{overflow : 'auto',maxWidth : 600}}>{children}</div>;
const mobileContainer = ({children}) => <div id="control-panel" style={mobileStyle}>{children}</div>;

const Rainbow = require('rainbowvis.js');
const groupColor = new Rainbow();
const arrColorDAS = [
  'cyan', //Anak sungai
  'darkblue',//sungai utama
  'yellow', // Danau
  'purple' // DAS cilicis

];
function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

function PengungsiLegend(props){
  const settings = props.settings;
  const legendItem = props.legendItem;
  
  var i = 0;
  var ele = [];
  var min = settings.minPengungsi;
  var max = settings.maxPengungsi;
  var temp = min;
  var increment = round((max - min) / settings.scaleCount, 0);

  if(min === max){
    i = max < 10 ? 0 : max <= 70 ? 1 : max <= 150 ? 2 : 3;
    ele.push(
    <Divider style={{wrapContent : true,minHeight : 10}}>
      <div style={{backgroundColor : '#' + legendItem[i],width : 30, height : 20,float : 'left',marginRight : 5}}/>
      <div style={{borderColor : 'white',padding : 2, textDecoration : 'none'}}> 
        {min}
      </div>
    </Divider>
    );
  }else{
    for(i = 0 ; i < settings.scaleCount;i ++){  
      ele.push(<Divider key={'pengungsi'+i} style={{wrapContent : true,minHeight : 10}}>
        <div style={{backgroundColor : '#' + legendItem[i],width : 27, height : 18,float : 'left',marginRight : 5}}/>
        <div style={{borderColor : 'white',padding : 2, textDecoration : 'none'}}> 
          {i === 0 ? min : round(temp + 1,0)} - { (temp+increment <= max - 1) ? round(temp+increment,0) : max}   
          
        </div>
        </Divider>);
    temp = temp + increment ;
    }
  }
  return <div id='legend-layout'> {ele} </div>;
}

function NonDecimalLegend(props){
    const settings = props.settings;
    const legendItem = props.legendItem;
    
    var i = 0;
    var ele = [];
    var min = settings.layerSettings.smallestName;
    var max = settings.layerSettings.largestName;
    var temp = min;
    var increment = 10;//round((max - min) / settings.scaleCount, 0);
  
    if(min === max){
      i = max < 10 ? 0 : max <= 70 ? 1 : max <= 150 ? 2 : 3;
      ele.push(
      <Divider style={{wrapContent : true,minHeight : 10}}>
        <div style={{backgroundColor : '#' + legendItem[i],width : 30, height : 20,float : 'left',marginRight : 5}}/>
        <div style={{borderColor : 'white',padding : 2, textDecoration : 'none'}}> 
          {min}
        </div>
      </Divider>
      );
    }else{
      for(i = 0 ; i < settings.scaleCount;i ++){  
        ele.push(
        <Divider key={'height'+i} style={{wrapContent : true,minHeight : 6,fontSize : 11}}>
          <div style={{backgroundColor : '#' + legendItem[i],width : 24, height : 16,float : 'left',marginRight : 5}}/>
          <div style={{borderColor : 'white',padding : 2, textDecoration : 'none'}}> 
            
            
            {i === settings.scaleCount - 1 ? '> 100' : i === 0 ? min : round(temp ,0)} {i === settings.scaleCount - 1 ? '' : '-'} {i === settings.scaleCount - 1 ? '' : (temp+increment <= max - 1) ? round(temp+increment -1 ,0) : max}   
            
          </div>
          </Divider>);
      temp = temp + increment;
  
      }
    }
    return <div id='legend-layout'> {ele} </div>;
}

function HeightLegend(props){
  const settings = props.settings;
  const legendItem = props.legendItem;
  
  var i = 0;
  var ele = [];
  var min = settings.layerSettings.smallestName;
  var max = settings.layerSettings.largestName;
  var temp = min;
  var increment = round((max - min) / settings.scaleCount, 2);

  if(min === max){
    i = max < 10 ? 0 : max <= 70 ? 1 : max <= 150 ? 2 : 3;
    ele.push(
    <Divider style={{wrapContent : true,minHeight : 10}}>
      <div style={{backgroundColor : '#' + legendItem[i],width : 30, height : 20,float : 'left',marginRight : 5}}/>
      <div style={{borderColor : 'white',padding : 2, textDecoration : 'none'}}> 
        {min}
      </div>
    </Divider>
    );
  }else{
    for(i = 0 ; i < settings.scaleCount;i ++){  
      ele.push(
      <Divider key={'height'+i} style={{wrapContent : true,minHeight : 10}}>
        <div style={{backgroundColor : '#' + legendItem[i],width : 30, height : 20,float : 'left',marginRight : 5}}/>
        <div style={{borderColor : 'white',padding : 2, textDecoration : 'none'}}> 
          {i === 0 ? min : round(temp + 0.01,2)} - { (temp+increment <= max - 0.01) ? round(temp+increment,2) : max}   
          
        </div>
        </Divider>);
    temp = temp + increment ;

    }
  }
  return <div id='legend-layout'> {ele} </div>;
}

export default class ControlCorona extends PureComponent {
  constructor(props){
    super(props);
    this.state = {
      smallestName : 'Fewer',
      largestName : 'More',
      smallestColor : '#FF0000',
      largestColor : '#00FF00',
      colorPicked : false,
      openModal : false,
      selectedColor : "darkred,yellow,darkgreen",
      classNumber : 4,
      errorMessage : "The value of class number must between 2 and 15",
      errorStat : false,
      minimized : false
    };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.saveLayerSetting = this.saveLayerSetting.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeComplete = this.handleChangeComplete.bind(this);
    this.validationCheck = this.validationCheck.bind(this);
    this.minimizePanel = this.minimizePanel.bind(this);
    groupColor.setSpectrum('darkred','red','orange','yellow','green','blue','darkblue','violet','purple');
  }

  closeModal(){
    this.setState({openModal : false});
  }
  openModal(){
    this.setState({openModal : true});
  }

  saveLayerSetting = () => {
    const {selectedColor,classNumber} = this.state;
    var arrColor = selectedColor.split(',');
    var smallestColor = arrColor[0];
    var midColor = arrColor[1];
    var largestColor = arrColor[2];
    this.props.onChange('layerSettings',{smallestColor,midColor,largestColor,classNumber});

    this.closeModal();
  }

  validationCheck(){
    const {classNumber} = this.state;
    if(classNumber < 2 || classNumber > 15){
      this.setState({errorStat : true})
    }
    else{
      this.setState({errorStat : false})

      this.saveLayerSetting();

    }
  }
  handleChangeComplete(value){
      this.setState({ selectedColor: value });

  }
  handleChangeMode(val){
    // this.setState({selectedMode : val})
    this.props.onChange('mode',val);
  }
  handleChange(val){
    this.setState({[val.target.name] : val.target.value});
    // const {name,value} = val;
  }

  handleChangeClassNumber(val){
    this.setState({classNumber : val});
  }

  UNSAFE_componentWillReceiveProps(nextProps){
    if(nextProps.scaleCount !== this.state.classNumber){
      this.setState({classNumber : nextProps.scaleCount});
    }

  }
  
  minimizePanel(){
    const {minimized} = this.state;
    this.setState({minimized : !minimized})
  }
  changeViewState(name,val){
    if(name === 'polygon'){
      this.props.onChange('layerPolygon',!val);
    }
    else if(name === 'pengungsi'){
      this.props.onChange('viewPengungsi',!val);
    
    }
    else if(name === 'pintu'){
      this.props.onChange('viewPintu',!val);
    }
    else if(name === 'laporan'){
      this.props.onChange('viewLaporan',!val);    
    }
    else{
      this.props.onChange('viewProvinsi',!val);
    }
  }
  render() {
    const {settings} = this.props;
    const Container = settings.isMobile ? (this.props.containerComponent || mobileContainer) : (this.props.containerComponent || defaultContainer);
    const {minimized} = this.state;
    var viewBuffer = settings.viewBuffer;

    var i;
    var legendItem = [];
    
    for(i = 0 ; i < settings.scaleCount ; i++){
       legendItem.push(settings.legendColor.colourAt(i));        
    }

    var ele = [];
    for(i = 0;i<5;i++){
      ele.push(
        <Divider key={'ele'+i} style={{wrapContent : true,minHeight : 10}}>
          <div style={{backgroundColor : '#' + legendItem[i],width : 30, height : 20,float : 'left',marginRight : 5}}/>
          <div style={{borderColor : 'white',padding : 2, textDecoration : 'none', height : 'wrap-content'}}> 
            {i === 0 ? '< 10' : i === 1 ? '10 - 70' : i === 2 ? '71 - 150' : '> 150' }
          
          </div>
        </Divider>
      );      
    }  


    return (
      minimized &&  
      <Button style={{
        position: 'absolute',
        right: "3%",
        margin: 15
        
        ,
        zIndex: 4
      }} 
      color = 'blue'
      onClick={this.minimizePanel} icon >
      <Icon name='angle double left'/>
    </Button>
    
    || 
      <Container style={{h4 : '1em',h5 : '0.9em'}}>
      
        
        {/* Header Legend */}
        <div style={{position : 'absolute',backgroundColor : 'white',top : 0,zIndex : 13}}>
          
            <div  style={{position : 'fixed',width: settings.isMobile ? "99%" : 242,backgroundColor : 'white',padding:10,borderBottom : 'solid black 1px'}} >
                {
                  settings.isMobile ?<center>        <h2>Legend</h2></center> :         <h2>Legend</h2>
                }
                  <Button onClick={this.minimizePanel}  icon style={settings.isMobile ? settingsButtonStyle : {position : 'absolute',right : 0,top : 5}}>
                    <Icon name='angle double right'/>
                  </Button>    
            </div>
        </div>

        {/* Untuk inspect mode, mengeluarkan info saat layer di-hover */}
      <div style={{position : 'relative',top : 60}}>
          
          {/* Button untuk checkbox  */}
          <div width="100%" style={{verticalAlign : 'top',position:'relative',zIndex : 12}}>
                <Checkbox
                  label='Inspect Mode'
                  name='inspectMode'
                  value={settings.inspectMode}
                  checked={settings.inspectMode}
                  onClick={(e, {checked}) => this.props.onChange('inspect',checked)}
                />
                <Checkbox
                  style={{position : 'absolute',top : 0,right : 0}}

                label='View 3D'
                name='controlPanel'
                value={viewBuffer}
                onClick={(e, {checked}) => this.props.onChange('buffer', checked)}
                />
              <hr/>
            </div>
       
        </div>
        {
              settings.infoRekap &&
          <div id ="rekap-kejadian" style={{position : 'relative'}}>
              <br/>
              <br/>

                  <div className={'header-four'}>Rekap Terkini</div>
                  <div>Total kejadian : <b>{settings.infoRekap.totalKejadian}</b></div>
                  <div>Total kematian : <b>{settings.infoRekap.totalKematian}</b></div>
                  <div>Total pemulihan : <b>{settings.infoRekap.totalPemulihan}</b> </div>
                  <div>Persentase kematian : <b>{round(settings.infoRekap.persenKematian,2)} % </b></div>
                  <div>Persentase pemulihan : <b>{round(settings.infoRekap.persenPemulihan,2)} %</b></div>
          <hr/>
          </div> 
            
            }
  <div id ='kota-terdampak-realtime' style={{position : 'relative'}}>
        <div div className={'header-four'}>Kota Terdampak Virus Corona (COVID-19) (Real time)</div> 
        <Button icon color='white' onClick={() => this.changeViewState('laporan',settings.viewLaporan)}  style={buttonViewStyle}>
            <Icon name={settings.viewLaporan ? 'eye' : 'eye slash'} />
        </Button>
        
        
        {
          settings.layerSettings.smallestName === Infinity && settings.layerSettings.largestName === -Infinity ?
          <b>0 reports in result</b> : <center><h4>Jumlah terinfeksi</h4></center> 

        }
        
         { settings.layerSettings.smallestName !== Infinity && settings.layerSettings.largestName !== -Infinity &&
             <NonDecimalLegend settings={settings} legendItem={legendItem}  onChange={this.props.onChange}/> 
        }
    <hr />
    </div>   
        


        
        Tanggal : {settings.arrPolygonDate[settings.indexPolygon]}  
        <input
          type="range"
          value={settings.indexPolygon}
          min={0}
          max={settings.arrPolygonDate.length - 1}
          step={1}
          onChange={evt => this.props.onChange('indexPolygon', evt.target.value)}
      /> 
      <div className="input">
        
        <div> Radius scale : {settings.radius} X </div>
      
        <input
            type="range"
            value={settings.radius}
            min={1}
            max={10}
            step={0.5}
            onChange={evt => this.props.onChange('radius', evt.target.value)}
        />
      </div>

      <div>
      Jumlah terinfeksi : <span style={{fontSize : 18, fontWeight : 'bold'}}> {settings.sumTerinfeksi}</span>
      </div>

      <div id="negara-terdampak" style={{position :'relative'}}>
        <div className={'header-four'}>Negara Terdampak Virus Corona (COVID-19)</div>
          <Button icon color='white' onClick={() => this.changeViewState('polygon',settings.viewPolygon)}  style={buttonViewStyle}>
              <Icon name={settings.viewPolygon ? 'eye' : 'eye slash'} />
          </Button>
          {/* <center><h3>Genangan Banjir</h3></center> */}
          
        
          {
            settings.minNegara === Infinity && settings.maxNegara === -Infinity ?
            <b>0 reports in result</b> : <center><h4>Jumlah terinfeksi</h4></center>  
          }
          
          { 
            settings.minNegara !== Infinity && settings.maxNegara !== -Infinity &&
            <NonDecimalLegend settings={{
              layerSettings : {
                smallestName : settings.minNegara,
                largestName : settings.maxNegara
              },
              scaleCount : settings.scaleCount
            }} legendItem={legendItem}  onChange={this.props.onChange}/> 
          }
      </div>
      {/* <hr/> */}
      {/* <div id='provinsi-terdampak' style={{position :'relative'}}>
          <div className={'header-four'}>Provinsi Terdampak Virus Corona (COVID-19)</div>
              <Button icon color='white' onClick={() => this.changeViewState('pintu',settings.viewPintu)}  style={buttonViewStyle}>
                  <Icon name={settings.viewPintu ? 'eye' : 'eye slash'} />
              </Button>
              
              {
                settings.minNegara === Infinity && settings.maxNegara === -Infinity ?
                <b>0 reports in result</b> : <center><h4>Jumlah terinfeksi</h4></center>  
              }
              
              { 
                settings.minNegara !== Infinity && settings.maxNegara !== -Infinity &&
                <NonDecimalLegend settings={{
                  layerSettings : {
                    smallestName : settings.minNegara,
                    largestName : settings.maxNegara
                  },
                  scaleCount : settings.scaleCount
                }} legendItem={legendItem}  onChange={this.props.onChange}/> 
              }
      </div> */}

      {/* <div id="prov-terdampak" style={{position : 'relative'}}>
        <h4>Provinsi Terdampak-China Virus Corona</h4>
          <Button icon color='white' onClick={() => this.changeViewState('pintu',settings.viewPintu)}  style={buttonViewStyle}>
              <Icon name={settings.viewPintu ? 'eye' : 'eye slash'} />
          </Button>
          
          {
            settings.minNegara === Infinity && settings.maxNegara === -Infinity ?
            <b>0 reports in result</b> : <center><h4>Jumlah terinfeksi</h4></center>  
          }
          
          { 
            settings.minNegara !== Infinity && settings.maxNegara !== -Infinity &&
            <NonDecimalLegend settings={{
              layerSettings : {
                smallestName : settings.minNegara,
                largestName : settings.maxNegara
              },
              scaleCount : settings.scaleCount
            }} legendItem={legendItem}  onChange={this.props.onChange}/> 
          }
      </div> */}
      <hr/>
            <div id="kota-terdampak" style={{position :'relative'}}>
        <div className={'header-four'}>Kota Terdampak Virus Corona (COVID-19) </div>
          <Button icon color='white' onClick={() => this.changeViewState('pengungsi',settings.viewPengungsi)}  style={buttonViewStyle}>
              <Icon name={settings.viewPengungsi ? 'eye' : 'eye slash'} />
          </Button>
          {/* <center><h3>Genangan Banjir</h3></center> */}
          
        
          {
            settings.minNegara === Infinity && settings.maxNegara === -Infinity ?
            <b>0 reports in result</b> : <center><h4>Jumlah terinfeksi</h4></center>  
          }
          
          { 
            settings.minNegara !== Infinity && settings.maxNegara !== -Infinity &&
            <NonDecimalLegend settings={{
              layerSettings : {
                smallestName : settings.minNegara,
                largestName : settings.maxNegara
              },
              scaleCount : settings.scaleCount
            }} legendItem={legendItem}  onChange={this.props.onChange}/> 
          }     
      
      <hr/>
      </div>  
          

      </Container>
    );
  }
} 
