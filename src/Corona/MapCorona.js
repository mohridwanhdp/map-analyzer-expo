import React from 'react';
import {DeckGL, ScatterplotLayer,GeoJsonLayer,ColumnLayer} from 'deck.gl';
import ReactMapGL, {Popup} from 'react-map-gl';
import {defaultMapStyle, darkMapStyle, lightMapStyle, satelliteMapStyle} from '../style/map-style.js';
import 'semantic-ui-css/semantic.min.css'
import ControlCorona from './ControlCorona.js'
import mapboxgl from 'mapbox-gl'
import axios from 'axios';
import { Button,Icon } from 'semantic-ui-react';
import {scaleThreshold} from 'd3-scale';
import ReactGA from 'react-ga';
import LayerInfo from './LayerInfoCorona.js'
import '../App.css';

const MAPBOX_TOKEN = 'pk.eyJ1IjoibW9ocmlkd2FuaGRwIiwiYSI6ImNrMDRvaGwzdDA0dDkzaG9tNnA3dmViODQifQ.qa1W5PaDaHzLnqIXbAM7zw';
const layerURL = 'https://hnjp62bwxh.execute-api.us-west-2.amazonaws.com/GeoDev/getdatacoronavirus'
const Rainbow = require('rainbowvis.js');
const convert = require('color-convert');
var groupColor = new Rainbow();
const refMap =  React.createRef();
ReactGA.initialize('UA-157598276-1', {
  debug: false,
  titleCase: false
});

ReactGA.pageview('/coronavirus');

const arrPolygonDate = ['22 Januari 2020','23 Januari 2020','24 Januari 2020','25 Januari 2020','26 Januari 2020','27 Januari 2020','28 Januari 2020',
'29 Januari 2020','30 Januari 2020','31 Januari 2020','01 Februari 2020','02 Februari 2020','03 Februari 2020','04 Februari 2020','05 Februari 2020',
'06 Februari 2020','07 Februari 2020','08 Februari 2020','09 Februari 2020','10 Februari 2020','11 Februari 2020','12 Februari 2020','13 Februari 2020','14 Februari 2020','15 Februari 2020','16 Februari 2020',
'17 Februari 2020','18 Februari 2020','19 Februari 2020','20 Februari 2020','21 Februari 2020','22 Februari 2020','23 Februari 2020','24 Februari 2020','25 Februari 2020','26 Februari 2020','27 Februari 2020','28 Februari 2020','29 Februari 2020',
'1 Maret 2020'
];

const arrLinkPolygon = "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_cfe14340-5c5a-11ea-a8f3-d9e9046a841b_Negara_Terdampak_18.geojson";
const arrLinkPoint = "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_fb58bc10-5c5a-11ea-a8f3-d9e9046a841b_Kota_Terdampak_12.geojson";


const arrLinkProvinsi = [
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_9b0b99d0-427f-11ea-9a92-3334784b8dee_Provinsi Terdampak 22 Januari 2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_a95fd050-427f-11ea-9a92-3334784b8dee_Provinsi Terdampak 23 Januari 2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_50f03cb0-4280-11ea-9a92-3334784b8dee_Provinsi Terdampak 24 Januari 2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_7b58dd90-4280-11ea-9a92-3334784b8dee_Provinsi Terdampak 25 Januari 2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_2ccd06f0-4281-11ea-9a92-3334784b8dee_Provinsi Terdampak 26 Januari 2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_442d3810-4281-11ea-9a92-3334784b8dee_Provinsi Terdampak 27 Januari 2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_5e72fcf0-4281-11ea-9a92-3334784b8dee_Provinsi Terdampak 28 Januari 2020.geojson"
];

const linkProvinsiJerman = "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_65ad10c0-4310-11ea-980d-b770a84c3601_Provinsi_Terdampak_27-28_Januari_2020.geojson"

const arrLinkProvinsiUS = [
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_77ed3100-431c-11ea-980d-b770a84c3601_Provinsi_Terdampak_22_Januari_2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_904074b0-431c-11ea-980d-b770a84c3601_Provinsi_Terdampak_23_Januari_2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_a3297590-431c-11ea-980d-b770a84c3601_Provinsi_Terdampak_24_Januari_2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_b4d8e500-431c-11ea-980d-b770a84c3601_Provinsi_Terdampak_25_Januari_2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_ca2d1980-431c-11ea-980d-b770a84c3601_Provinsi_Terdampak_26_Januari_2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_df296a00-431c-11ea-980d-b770a84c3601_Provinsi_Terdampak_27_Januari_2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_f0889690-431c-11ea-980d-b770a84c3601_Provinsi_Terdampak_28_Januari_2020.geojson"

];

const arrLinkProvinsiCanada = [
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_00263e30-4337-11ea-b7e0-5d79d198e807_26 Januari 2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_143dacf0-4337-11ea-b7e0-5d79d198e807_27 Januari 2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_7a5b5690-4337-11ea-b7e0-5d79d198e807_28 Januari 2020.geojson"
];

const arrLinkProvinsiMalaysia =[
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_88632ab0-434b-11ea-b7e0-5d79d198e807_Provinsi_Terdampak_25_JANUARI_2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_a0e01670-434b-11ea-b7e0-5d79d198e807_Provinsi_Terdampak_26_JANUARI_2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_ab6c4870-434b-11ea-b7e0-5d79d198e807_Provinsi_Terdampak_27_JANUARI_2020.geojson",
  "https://geomapid-layer.s3-us-west-2.amazonaws.com/public/mapidseeit/mapidseeit_bc3e5030-434b-11ea-b7e0-5d79d198e807_Provinsi_Terdampak_28_JANUARI_2020.geojson"
];


const arrPengungsiDate = ['04 Januari 2020','05 Januari 2020','06 Januari 2020','07 Januari 2020','08 Januari 2020','09 Januari 2020','10 Januari 2020'];
const arrColorDAS = [
    'purple', // DAS cilicis
    'darkblue',//sungai utama
    'yellow ', // Danau
    'cyan' //Anak sungai
  ]


export default class MapCorona extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    mapStyle: defaultMapStyle,
    viewState : {
        width : "100vw",
        height : "100vh",
        latitude: -6.175110,
        longitude: 106.865036,
        zoom: 1.25
      },
      viewStateDeck : {
        width : "100%",
        height : 1000,
        latitude: -6.175110,
        longitude: 106.865036,
        zoom: 12,
      
      },
      popupInfo : null,
      layers3D : [],
      hoveredObject : null,
      pointerX : 0,
      pointerY : 0,
      visualParams : [],
      dataLayers : null,
      view3D : false,
      columnPosition : null,
      year : 0,
      layerInfo : null,
      layerInfoVisibility : false,
      activeLayer : null,
      scaleCount : 11,
      legendColor : null,
      isPoint : false,
      radius : 1,
      viewBuffer : false,
      layerSettings : {
        smallestName : 'Fewer',
        largestName : 'More',
        smallestColor : '#FF0000',
        largestColor : '#00FF00'
      },
      showPopup : false,
      dataColumn : null,
      selectedMode : 'height',
      inspectMode : false,
      arrLayerInfo : [],
      max : 1,
      min : 0,
      time : 1,
      pointGeojson : null,
      polygonGeojson : null,
      indexPolygon : 0,
      width : window.innerWidth,
      height : window.innerHeight,
      viewPolygon : true,
      viewProvinsi : true,
      maxNegara : 1,
      minNegara : 0,
      indexPengungsi : 0,
      viewPengungsi : false,
      viewPintu  : false,
      viewLaporan : false,
      infoTooltip : null,
      popupX : 0,
      popupY : 0,
      popupProps : null,
      popupType : '',
      sumTerinfeksi : 0,
      infoRekap : null,
      pengungsiGeojson : null

  }
  
    this._onViewStateChange = this._onViewStateChange.bind(this);
    this._onViewStateChange = this._onViewStateChange.bind(this);
    this._refreshMap = this._refreshMap.bind(this);
    this.layerBoxRef = React.createRef();
    this._renderTooltip = this._renderTooltip.bind(this);
    this._resize = this._resize.bind(this);
    this._onMapLoaded = this._onMapLoaded.bind(this);

    groupColor.setSpectrum('darkred','red','orange','yellow','green','blue','darkblue','violet','purple');

  }

 
  async componentDidMount(){
      //Laporan corona
    var pGeojson = [];
    var plGeojson = [];
    var pengungsiGeojson = [];
    var paGeojson = [];
    const {radius,view3D,viewPolygon,viewLaporan,viewPengungsi,viewPintu,indexPolygon} = this.state;  
    

    await axios.get(layerURL).then(
      function (response) {
        pGeojson = response.data;
      }
    ).catch(function (error) {
      console.log(error);
    });

    await axios.get(arrLinkPolygon).then(
      function (response) {
        plGeojson = response.data;
      }
    ).catch(function (error) {
      console.log(error);
    });
   

    await axios.get(arrLinkPoint).then(
      function (response) {
        pengungsiGeojson = response.data;
      }
    ).catch(function (error) {
      console.log(error);
    });

    await axios.get(arrLinkProvinsi[0]).then(
      function (response) {
        paGeojson.china = response.data;
      }
    ).catch(function (error) {
      console.log(error);
    });

    await axios.get(arrLinkProvinsiUS[0]).then(
      function (response) {
        paGeojson.us = response.data;
      }
    ).catch(function (error) {
      console.log(error);
    });

    await axios.get(linkProvinsiJerman).then(
      function (response) {
        paGeojson.jerman = response.data;
      }
    ).catch(function (error) {
      console.log(error);
    });

    
    this.setState({pointGeojson : pGeojson});
    this.setState({polygonGeojson : plGeojson});
    this.setState({pengungsiGeojson : pengungsiGeojson});
    this.setState({pintuGeojson : paGeojson});

    const baseDate =  this.getBaseDate(indexPolygon);
    this.setState({layerInfo : [{geojson : plGeojson,baseDate : baseDate}]});
    this._render3DLayers(pGeojson,viewLaporan,radius,view3D,viewPolygon,viewPengungsi,paGeojson,viewPintu,indexPolygon);
    //Resize window
    window.addEventListener('resize', this._resize);
    // this._resize();    
  }

  _onMapLoaded(){
    const map = this.reactMap.getMap();

    if(map){
      map.addControl(new mapboxgl.AttributionControl({
        customAttribution:
          '<a href="https://mapid.io/">Geomapid v2.0 | © MAPID </a>| <a href="https://www.maptiler.com/copyright/">© MapBox </a> | <a href="https://www.openstreetmap.org/copyright">© OpenStreetMap contributors </a> | <a href="https://www.who.int/westernpacific/emergencies/novel-coronavirus">© Data Source: WHO</a>'
      }))
    }
  }
  getBaseDate(indexPolygon){
    return indexPolygon < 10 ?  (parseInt(indexPolygon)+22) + '_JAN' : indexPolygon <  39 ? (parseInt(indexPolygon-9)) + '_FEB' : (parseInt(indexPolygon - 38)) + '_MAR';
  }

  _resize() {
    
    this._onViewStateChange({
      width: window.innerWidth,
      height: window.innerHeight
      // width : "100%",
      // height : "100%"
    }); 
    this.setState({
      width : window.innerWidth,
      height : window.innerHeight 
    })
  }
  
    _render3DLayers(pointGeojson,viewLaporan,radius,view3D,viewPolygon,viewPengungsi,pintuGeojson,viewPintu,indexPolygon){

    var max = 1;
    var min = 0;
    var maxNegara = 1;
    var minNegara = 0;


    const {scaleCount,polygonGeojson,pengungsiGeojson} = this.state;
    var baseDate = this.getBaseDate(indexPolygon);
    var colName = 'INF_' + baseDate;
    
    //Color settubgs
    var myRainbow = new Rainbow();
    // myRainbow.setSpectrum('#0B2B83','green','yellow','#8B0000');      
    myRainbow.setSpectrum('#006400','yellow','#8B0000');      
    

    myRainbow.setNumberRange(0,scaleCount - 1);
    var legendItem = []
    var j;
    
    for(j = 0 ; j < scaleCount ; j++){
      legendItem.push(convert.hex.rgb(myRainbow.colourAt(j)));       
    }
    const COLOR_SCALE = scaleThreshold()
          .domain([1.0, 2.0, 3.0, 4.0, 5.0,6.0,7.0,8.0,9.0,1.0,10000.0])
          .range(legendItem);

    var arr = [];    
    var i = 0;
    var divider = function(num){
      var div = num < 50 ? 1 : 250;
      return num < 50 ? num / div : 50 + num/div;
    };

    
    this.setState({legendColor : myRainbow});
    var infoTooltip = null;

    if(polygonGeojson.features){
        /**
       * ========NEGARA TERDAMPAK KORONA=========
       * Type = polygon
       * ========================================
       * geojson = variabel menampung features dari pointGeojson (Kota terdampak korana)
       */
      var plGeojson = polygonGeojson;
      // plGeojson.features = polygonGeojson.features.filter(function(o) { if(o.properties[colName] > 0){return o}})
      // console.log(polygonGeojson);      
      // console.log(plGeojson);
        maxNegara = Math.max.apply(Math, plGeojson.features.map(function(o) { return o.properties[colName]}));
        minNegara = Math.min.apply(Math, plGeojson.features.map(function(o) { return o.properties[colName]}));
        var arrValueInfected= plGeojson.features.map(function(o) {return o.properties[colName]});
        
        var arrSum = function(arr){
          return arr.reduce(function(a,b){
            return a + b
          }, 0);
        }

        // console.log(arrSum(arrValueConfirmed));
        var sum = arrSum(arrValueInfected);
        this.setState({sumTerinfeksi : sum});

        
      if(viewPolygon){
      
        infoTooltip = {
          header : ['Terinfeksi : ','Meninggal : ','Pulih : '],
          name : ['INF_' + baseDate,'DTH_'+baseDate, 'REC_' + baseDate],
          content : ['','','']
        };
        
        if(view3D){
          arr.push(new GeoJsonLayer({
            id: 'negara',
            data : plGeojson,
            opacity: 0.7,
            stroked : true,
            filled: true ,
            fp64 : true,
            wireframe: true,
            extruded : true,
            elevationScale : (max <= 500 ) ? 1 * 250 : (500/max) * 250  ,
            // getElevation: d =>  d.properties[colName] === 0 ? 0 : d.properties[colName] > 50 ? d.properties[colName] : d.properties[colName] * (maxNegara / 50) , 
            getElevation: d =>  d.properties[colName] === 0 ? 0 : d.properties[colName] > 500 ? 25000 + (d.properties[colName]/10) : d.properties[colName] > 100 ? 22500 + d.properties[colName] : d.properties[colName] > 90 ? 20000 + d.properties[colName] : d.properties[colName] > 80 ? 17500 + d.properties[colName] : d.properties[colName] > 70 ? 15000 + d.properties[colName] : d.properties[colName] > 60 ? 12500 + d.properties[colName] : d.properties[colName] > 50 ? 10000 + d.properties[colName] : d.properties[colName] > 40 ? 9000 + d.properties[colName] : d.properties[colName] > 30 ? 8000 + d.properties[colName] : d.properties[colName] > 20 ? 7000 + d.properties[colName] : d.properties[colName] > 10 ? 6000 + d.properties[colName] : 5000 + d.properties[colName] , 
            getFillColor: d => d.properties[colName] === 0 ? [0,0,0,0] : COLOR_SCALE(d.properties[colName]/10),
            getLineColor: d => d.properties[colName] === 0 ? [0,0,0,0] : COLOR_SCALE(d.properties[colName]/10) ,
            getLineWidth : d => 1,
            pickable: true,
            onHover: (info) => { 
              
            this.setState({
              hoveredObject: info.object,
              pointerX: info.x,
              pointerY: info.y,
              layerType : 'polygon',
              infoTooltip : infoTooltip 
            }); },
            updateTriggers : {
              getElevation : this.state.indexPolygon,
              getFillColor : this.state.indexPolygon,
              getLineColor : this.state.indexPolygon
            },
            transitions : {
              getElevation : 1000
            }
            // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'data_bnpb'})}
          }));
        }else{
          arr.push(
            new GeoJsonLayer({
              id: 'negara',
              data : plGeojson,
              opacity: 0.7,
              stroked : true,
              filled: true ,
              fp64 : true,
              wireframe: true,
              extruded : true,
              elevationScale : 1  ,
              getElevation: d=> d.properties[colName] === 0 ? 0 : 0.1 , 
              getFillColor: d => d.properties[colName] === 0 ? [0,0,0,0] : COLOR_SCALE(d.properties[colName]/10),
             getLineColor: d => d.properties[colName] === 0 ? [0,0,0,0] : COLOR_SCALE(d.properties[colName]/10) ,
              getLineWidth : d => 1,
              pickable: true,
              onHover: (info) => { this.setState({
                hoveredObject: info.object,
                pointerX: info.x,
                pointerY: info.y,
                layerType : 'polygon',
                infoTooltip : infoTooltip
              }); },
              updateTriggers : {
                getElevation : this.state.indexPolygon,
                getFillColor : this.state.indexPolygon,
                getLineColor : this.state.indexPolygon
              }
              // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'data_bnpb'})}
    
    
            })
          );
        }
      }

    }
    


    /**
     * PROVINSI TERDAMPAK
     */
    
    if(viewPintu){
      var maxProv = 1;
      var minProv = 0;

      infoTooltip = {
        header : ['Terinfeksi : ','Meninggal : ','Pulih : '],
        name : ['INF_' + baseDate,'DTH_'+baseDate, 'REC_' + baseDate],
        content : ['','','']
      };

      if(pintuGeojson.china){
          //Provinsi di China
          var chinaGeojson = pintuGeojson.china;
          maxProv = Math.max.apply(Math, chinaGeojson.features.map(function(o) { return o.properties[colName]}));
          minProv = Math.min.apply(Math, chinaGeojson.features.map(function(o) { return o.properties[colName]}));

          if(view3D){
            arr.push(new GeoJsonLayer({
              id: 'provinsi',
              data : chinaGeojson,
              opacity: 0.7,
              stroked : true,
              filled: true ,
              fp64 : true,
              wireframe: true,
              extruded : true,
              elevationScale : (max <= 500 ) ? 1 * 250 : (500/max) * 250  ,
              getElevation: d =>  d.properties[colName] > 50 ? (d.properties[colName] === maxProv ? d.properties[colName] :  maxProv -  d.properties[colName])  : d.properties[colName] * (maxProv / 50) ,
              getFillColor: d => COLOR_SCALE(d.properties[colName]/10),
              getLineColor: d => COLOR_SCALE(d.properties[colName]/10) ,
              getLineWidth : d => 1,
              pickable: true,
              onHover: (info) => { 
                
                this.setState({
                hoveredObject: info.object,
                pointerX: info.x,
                pointerY: info.y,
                layerType : 'provinsi',
                infoTooltip : infoTooltip 
              }); },
              transitions : {
                getElevation : 1000
              }
              // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'data_bnpb'})}
            }));
          }else{
            arr.push(
              new GeoJsonLayer({
                id: 'provinsi',
                data : chinaGeojson,
                opacity: 0.7,
                stroked : true,
                filled: true ,
                fp64 : true,
                wireframe: true,
                extruded : true,
                elevationScale : 1  ,
                getElevation: 0.1 , 
                getFillColor: d => COLOR_SCALE(d.properties[colName]/10),
                getLineColor: d => COLOR_SCALE(d.properties[colName]/10) ,
                getLineWidth : d => 1,
                pickable: true,
                onHover: (info) => { this.setState({
                  hoveredObject: info.object,
                  pointerX: info.x,
                  pointerY: info.y,
                  layerType : 'provinsi',
                  infoTooltip : infoTooltip
                }); },
                // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'data_bnpb'})}
      
      
              })
            );
          }

      }
      
      //Provinsi di Amerika 
      if(pintuGeojson.us){
        var usGeojson = pintuGeojson.us;
        if(view3D){
          arr.push(new GeoJsonLayer({
            id: 'provinsi-us',
            data : usGeojson,
            opacity: 0.7,
            stroked : true,
            filled: true ,
            fp64 : true,
            wireframe: true,
            extruded : true,
            elevationScale : (max <= 500 ) ? 1 * 250 : (500/max) * 250  ,
            getElevation: d =>  d.properties[colName] > 50 ? (d.properties[colName] === maxProv ? d.properties[colName] :  maxProv -  d.properties[colName])  : d.properties[colName] * (maxProv / 50) ,
            getFillColor: d => COLOR_SCALE(d.properties[colName]/10),
            getLineColor: d => COLOR_SCALE(d.properties[colName]/10) ,
            getLineWidth : d => 1,
            pickable: true,
            onHover: (info) => { 
              
              this.setState({
              hoveredObject: info.object,
              pointerX: info.x,
              pointerY: info.y,
              layerType : 'provinsi',
              infoTooltip : infoTooltip 
            }); },
            transitions : {
              getElevation : 1000
            }
            // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'data_bnpb'})}
          }));
        }else{
          arr.push(
            new GeoJsonLayer({
              id: 'provinsi-us',
              data : usGeojson,
              opacity: 0.7,
              stroked : true,
              filled: true ,
              fp64 : true,
              wireframe: true,
              extruded : true,
              elevationScale : 1  ,
              getElevation: 0.1 , 
              getFillColor: d => COLOR_SCALE(d.properties[colName]/10),
              getLineColor: d => COLOR_SCALE(d.properties[colName]/10) ,
              getLineWidth : d => 1,
              pickable: true,
              onHover: (info) => { this.setState({
                hoveredObject: info.object,
                pointerX: info.x,
                pointerY: info.y,
                layerType : 'provinsi',
                infoTooltip : infoTooltip
              }); },
              // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'data_bnpb'})}
            })
          );
        }

      }

      if(pintuGeojson.jerman && indexPolygon > 4){
        var jermanGeojson = pintuGeojson.jerman;
        if(view3D){
          arr.push(new GeoJsonLayer({
            id: 'provinsi-jerman',
            data : jermanGeojson,
            opacity: 0.7,
            stroked : true,
            filled: true ,
            fp64 : true,
            wireframe: true,
            extruded : true,
            elevationScale : (max <= 500 ) ? 1 * 250 : (500/max) * 250  ,
            getElevation: d =>  d.properties[colName] > 50 ? (d.properties[colName] === maxProv ? d.properties[colName] :  maxProv -  d.properties[colName])  : d.properties[colName] * (maxProv / 50) ,
            getFillColor: d => COLOR_SCALE(d.properties[colName]/10),
            getLineColor: d => COLOR_SCALE(d.properties[colName]/10) ,
            getLineWidth : d => 1,
            pickable: true,
            onHover: (info) => { 
              
              this.setState({
              hoveredObject: info.object,
              pointerX: info.x,
              pointerY: info.y,
              layerType : 'provinsi',
              infoTooltip : infoTooltip 
            }); },
            transitions : {
              getElevation : 1000
            }
            // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'data_bnpb'})}
          }));
        }else{
          arr.push(
            new GeoJsonLayer({
              id: 'provinsi-jerman',
              data : jermanGeojson,
              opacity: 0.7,
              stroked : true,
              filled: true ,
              fp64 : true,
              wireframe: true,
              extruded : true,
              elevationScale : 1  ,
              getElevation: 0.1 , 
              getFillColor: d => COLOR_SCALE(d.properties[colName]/10),
              getLineColor: d => COLOR_SCALE(d.properties[colName]/10) ,
              getLineWidth : d => 1,
              pickable: true,
              onHover: (info) => { this.setState({
                hoveredObject: info.object,
                pointerX: info.x,
                pointerY: info.y,
                layerType : 'provinsi',
                infoTooltip : infoTooltip
              }); },
              // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'data_bnpb'})}
            })
          );
        }
      }

      if(pintuGeojson.canada && indexPolygon > 3){
        var canadaGeojson = pintuGeojson.canada;
        if(view3D){
          arr.push(new GeoJsonLayer({
            id: 'provinsi-canada',
            data : canadaGeojson,
            opacity: 0.7,
            stroked : true,
            filled: true ,
            fp64 : true,
            wireframe: true,
            extruded : true,
            elevationScale : (max <= 500 ) ? 1 * 250 : (500/max) * 250  ,
            getElevation: d =>  d.properties[colName] > 50 ? (d.properties[colName] === maxProv ? d.properties[colName] :  maxProv -  d.properties[colName])  : d.properties[colName] * (maxProv / 50) ,
            getFillColor: d => COLOR_SCALE(d.properties[colName]/10),
            getLineColor: d => COLOR_SCALE(d.properties[colName]/10) ,
            getLineWidth : d => 1,
            pickable: true,
            onHover: (info) => { 
              
              this.setState({
              hoveredObject: info.object,
              pointerX: info.x,
              pointerY: info.y,
              layerType : 'provinsi',
              infoTooltip : infoTooltip 
            }); },
            transitions : {
              getElevation : 1000
            }
            // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'data_bnpb'})}
          }));
        }else{
          arr.push(
            new GeoJsonLayer({
              id: 'provinsi-canada',
              data : canadaGeojson,
              opacity: 0.7,
              stroked : true,
              filled: true ,
              fp64 : true,
              wireframe: true,
              extruded : true,
              elevationScale : 1  ,
              getElevation: 0.1 , 
              getFillColor: d => COLOR_SCALE(d.properties[colName]/10),
              getLineColor: d => COLOR_SCALE(d.properties[colName]/10) ,
              getLineWidth : d => 1,
              pickable: true,
              onHover: (info) => { this.setState({
                hoveredObject: info.object,
                pointerX: info.x,
                pointerY: info.y,
                layerType : 'provinsi',
                infoTooltip : infoTooltip
              }); },
              // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'data_bnpb'})}
            })
          );
        }
      }

      if(pintuGeojson.malaysia && indexPolygon > 2){
        var malaysiaGeojson = pintuGeojson.malaysia;
        if(view3D){
          arr.push(new GeoJsonLayer({
            id: 'provinsi-malaysia',
            data : malaysiaGeojson,
            opacity: 0.7,
            stroked : true,
            filled: true ,
            fp64 : true,
            wireframe: true,
            extruded : true,
            elevationScale : (max <= 500 ) ? 1 * 250 : (500/max) * 250  ,
            getElevation: d =>  d.properties[colName] > 50 ? (d.properties[colName] === maxProv ? d.properties[colName] :  maxProv -  d.properties[colName])  : d.properties[colName] * (maxProv / 50) ,
            getFillColor: d => COLOR_SCALE(d.properties[colName]/10),
            getLineColor: d => COLOR_SCALE(d.properties[colName]/10) ,
            getLineWidth : d => 1,
            pickable: true,
            onHover: (info) => { 
              
              this.setState({
              hoveredObject: info.object,
              pointerX: info.x,
              pointerY: info.y,
              layerType : 'provinsi',
              infoTooltip : infoTooltip 
            }); },
            transitions : {
              getElevation : 1000
            }
            // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'data_bnpb'})}
          }));
        }else{
          arr.push(
            new GeoJsonLayer({
              id: 'provinsi-malaysia',
              data : malaysiaGeojson,
              opacity: 0.7,
              stroked : true,
              filled: true ,
              fp64 : true,
              wireframe: true,
              extruded : true,
              elevationScale : 1  ,
              getElevation: 0.1 , 
              getFillColor: d => COLOR_SCALE(d.properties[colName]/10),
              getLineColor: d => COLOR_SCALE(d.properties[colName]/10) ,
              getLineWidth : d => 1,
              pickable: true,
              onHover: (info) => { this.setState({
                hoveredObject: info.object,
                pointerX: info.x,
                pointerY: info.y,
                layerType : 'provinsi',
                infoTooltip : infoTooltip
              }); },
              // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'data_bnpb'})}
            })
          );
        }
      }
    }


    if(pengungsiGeojson){
        /**
         * ==KOTA TERDAMPAK KORONA(NON REAL TIME)==
         * Type = point
         * ========================================
         * geojson = variabel menampung features dari pointGeojson (Kota terdampak korana)
         */
        var maxKota = 1;
        var minKota = 0;
        var ktGeojson = pengungsiGeojson.features; 

        // maxKota = Math.max.apply(Math, plGeojson.features.map(function(o) { return o.properties[colName]}));
        // minKota = Math.min.apply(Math, plGeojson.features.map(function(o) { return o.properties[colName]}));
      if(viewPengungsi){
        infoTooltip = {
          header : ['Terinfeksi : ','Meninggal : ','Pulih : '],
          name : ['INF_' + baseDate,'DTH_'+baseDate, 'REC_' + baseDate],
          content : ['','','']
        };
        
        // Handling per 5 Februari
        // if(indexPolygon > 11){
        //   colName = 'INF_2_FEB';
        // }
        
        if(view3D){
          
          var colLayer = ktGeojson.map((f,index) =>{
            return new ColumnLayer({
              id: 'kota-non'+index,
              data : [f],
              diskResolution : 2000,
              opacity: 0.7,
              stroked: false,
              filled: true,
              radius : f.properties[colName] === 0 ? 0 : divider(f.properties[colName]) * radius * 1 *1000,
              extruded: true,
              wireframe: true,
              // getElevationScale : 1,    
              // elevationScale : max <= 500 ? 1 : 500/max,
              getPosition : d => d.geometry.coordinates,
              getElevation: d => d.properties[colName] * 1000,
              getFillColor: d => COLOR_SCALE(d.properties[colName]/10),
              getLineColor: d => COLOR_SCALE(d.properties[colName]/10) ,     
              pickable: true,
              onHover: (info) => { this.setState({
                hoveredObject: info.object,
                pointerX: info.x,
                pointerY: info.y,
                layerType : 'kota-non',
                infoTooltip : infoTooltip
              });},
              transitions : {
                getRadius : 1000,
                getElevation : 1000
                
              },
              updateTriggers : {
                getElevation : this.state.indexPolygon,
                getFillColor : this.state.indexPolygon,
                getLineColor : this.state.indexPolygon
              }
              // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'laporan'})}
          })
          });
          arr.push(...colLayer);
          
        }
        else{
          arr.push(
            new ScatterplotLayer({
              id: 'kota-non',
              data : ktGeojson,
              pickable: true,
              opacity: 0.7,
              stroked: true,
              filled: true,
              radiusMinPixels: 1,
              radiusMaxPixels: 5000,
              lineWidthMinPixels: 1,
              getPosition: d => d.geometry.coordinates,
              getElevation :  1,
              getRadius: d => d.properties[colName] === 0 ? 0 : divider(d.properties[colName]) * radius * 1 *1000,
              // getFillColor: d => legendItem[(Math.floor((d.properties['Confirmed']/max)*(scaleCount - 1)))] ,
              // getLineColor: d => legendItem[(Math.floor((d.properties['Confirmed']/max)*(scaleCount - 1)))],
              getFillColor: d => d.properties[colName] === 0 ? [0,0,0,0] : COLOR_SCALE(d.properties[colName]/10),
              getLineColor: d => d.properties[colName] === 0 ? [0,0,0,0] : COLOR_SCALE(d.properties[colName]/10) ,              
              onHover: (info) => { this.setState({
                hoveredObject: info.object,
                pointerX: info.x,
                pointerY: info.y,
              layerType : 'kota-non',
              infoTooltip : infoTooltip
              }); },updateTriggers : {
                getElevation : this.state.indexPolygon,
                getFillColor : this.state.indexPolygon,
                getLineColor : this.state.indexPolygon,
                getRadius : [this.state.radius,this.state.indexPolygon]
              }
              // transitions : {
              //   getRadius : 1000
              // }
              // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'laporan'})}
            })
          )
        }

      }
    }
    
    if(pointGeojson){
        /**
       * ========KOTA TERDAMPAK KORONA=========
       * Type = point
       * ========================================
       * geojson = variabel menampung features dari pointGeojson (Kota terdampak korana)
       */
      // scaleCount = 5;
      // myRainbow.setNumberRange(0,scaleCount - 1);
      // legendItem = [];
      
      // for(j = 0 ; j < scaleCount ; j++){
      //   legendItem.push(convert.hex.rgb(myRainbow.colourAt(j)));       
      // }

      var geojson = pointGeojson.features//= pointGeojson.features.filter((o) => (o.properties.report_data !== null)).filter((o) => (Object.prototype.hasOwnProperty.call(o.properties.report_data,'flood_depth')));
      
      max = Math.max.apply(Math, geojson.map(function(o) { return o.properties.Confirmed;}));
      min = Math.min.apply(Math, geojson.map(function(o) { return o.properties.Confirmed;}));
      
    var arrValueConfirmed = geojson.map(function(o) {return o.properties['Confirmed'] } );
    var arrValueDeath = geojson.map(function(o) {return o.properties['Deaths'] } );
    var arrValueRecover = geojson.map(function(o) {return o.properties['Recovered']});
    
    var sumConf = arrSum(arrValueConfirmed);
    var sumDth = arrSum(arrValueDeath);
    var sumRec = arrSum(arrValueRecover);
    this.setState({
      infoRekap : {
        totalKejadian : sumConf,
        totalKematian : sumDth,
        totalPemulihan : sumRec,
        persenKematian : (sumDth/sumConf) * 100,
        persenPemulihan : (sumRec/sumConf) * 100
      }
    });

      if(viewLaporan){
        infoTooltip = {
          header : ['Province State : ','Country Region : ','Confirmed :','Deaths : ','Recovered : '],
          name : ['Province_State','Country_Region','Confirmed','Deaths','Recovered'],
          content : ['','','','','']
        }

        if(view3D){
          
          
          var colLayer = geojson.map((f,index) =>{
            return new ColumnLayer({
              id: 'kota'+index,
              data : [f],
              diskResolution : 2000,
              opacity: 0.7,
              stroked: false,
              filled: true,
              radius : divider(f.properties['Confirmed']) * radius * 1 *1000,
              extruded: true,
              wireframe: true,
              // getElevationScale : 1,    
              // elevationScale : max <= 500 ? 1 : 500/max,
              getPosition : d => d.geometry.coordinates,
              getElevation: d => d.properties['Confirmed'] * 1000,
              getFillColor: d => COLOR_SCALE(d.properties['Confirmed']/10),
              getLineColor: d => COLOR_SCALE(d.properties['Confirmed']/10) ,     
              pickable: true,
              onHover: (info) => { this.setState({
                hoveredObject: info.object,
                pointerX: info.x,
                pointerY: info.y,
                layerType : 'negara',
                infoTooltip : infoTooltip
              });},
              transitions : {
                getRadius : 1000
              }
              // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'laporan'})}
          })
          });
          arr.push(...colLayer);
          // arr.push(
          //   new GeoJsonLayer({
          //     id: 'kota',
          //     data : pointGeojson,
          //     // diskResolution : 2000,
          //     opacity: 0.7,
          //     stroked: false,
          //     filled: true,
          //     fp64 : true,
          //     // radius : d => divider(d.properties['Confirmed']) * radius * 1 *1000,
          //     extruded: true,
          //     wireframe: true,
          //     // getElevationScale : 5000,   

          //     elevationScale : 1,
          //     // getPosition : d => d.geometry.coordinates,
          //     getElevation:1000,// d => d.properties['Confirmed'] * 1000,
          //     // getFillColor: d => legendItem[(Math.floor((d.properties['Confirmed']/max)*(scaleCount - 1)))] ,
          //     // getLineColor: d => legendItem[(Math.floor((d.properties['Confirmed']/max)*(scaleCount - 1)))],   
          //     getFillColor: d => COLOR_SCALE(d.properties['Confirmed']/10),
          //     getLineColor: d => COLOR_SCALE(d.properties['Confirmed']/10) ,     
          //     getRadius : d => divider(d.properties['Confirmed']) * radius * 1 *1000,
          //     pickable: true,
          //     onHover: (info) => { this.setState({
          //       hoveredObject: info.object,
          //       pointerX: info.x,
          //       pointerY: info.y,
          //       layerType : 'korban',
          //       infoTooltip : infoTooltip
          //     });}
          //     //,
          //     // transitions : {
          //     //   radius : 1000
          //     // }
          //     // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'laporan'})}
          // })
          // );
        }
        else{
          arr.push(
            new ScatterplotLayer({
              id: 'kota',
              data : geojson,
              pickable: true,
              opacity: 0.7,
              stroked: true,
              filled: true,
              radiusMinPixels: 1,
              radiusMaxPixels: 5000,
              lineWidthMinPixels: 1,
              getPosition: d => d.geometry.coordinates,
              getElevation :  1,
              getRadius: d => divider(d.properties['Confirmed']) * radius * 1 *1000,
              // getFillColor: d => legendItem[(Math.floor((d.properties['Confirmed']/max)*(scaleCount - 1)))] ,
              // getLineColor: d => legendItem[(Math.floor((d.properties['Confirmed']/max)*(scaleCount - 1)))],
              getFillColor: d => COLOR_SCALE(d.properties['Confirmed']/10),
              getLineColor: d => COLOR_SCALE(d.properties['Confirmed']/10) ,              
              onHover: (info) => { this.setState({
                hoveredObject: info.object,
                pointerX: info.x,
                pointerY: info.y,
              layerType : 'kota',
              infoTooltip : infoTooltip
              }); },
              updateTriggers : {
                getRadius : this.state.radius
              }
              // transitions : {
              //   getRadius : 1000
              // }
              // onClick : (info) => {this.setState({popupInfo : info.object,showPopup : true,popupX : info.coordinate[1],popupY : info.coordinate[0],popupProps : infoTooltip,popupType : 'laporan'})}
            })
          )
        }

      }
      

    }
    
    // console.log(arr);
    this.setState({layers3D : arr});
    this.setState({layerSettings : {smallestName : min, largestName : max}});
    this.setState({minNegara : minNegara,maxNegara : maxNegara });

  }

  _renderTooltip() {
    const {pointerX, pointerY, hoveredObject,layerType,infoTooltip,width} = this.state;
    const isMobile = width <= 500;
    return (
      (hoveredObject) && (
        <div  style={{top: pointerY, left: pointerX, zIndex : 13,position: 'absolute',
          margin: 8,
          padding: 4,
          background: 'rgba(0, 0, 0, 0.8)',
          color: '#fff',
          maxWidth: 300,
          fontSize: 10,
          pointerEvents: 'none'}}>
            <div>              
            {
            infoTooltip.header.map(function(key,index) {       
              return ( 
                <div key={index}>
                    <div>{key}{hoveredObject['properties'][infoTooltip.name[index]]} {infoTooltip.content[index]}<br /></div>            
                </div>
                )
            })
          }
            </div>
        </div>
      )
    );
  }

  _onViewStateChange({viewState}) {
    this.setState({
      viewState: {...this.state.viewState, ...viewState}
    });
  }
  changeMapStyle = (e) => {

    if(e === 'dark'){
      this.setState({mapStyle : darkMapStyle});
    }
    else if(e === 'light'){
      this.setState({mapStyle : lightMapStyle});
    }else if(e === 'basic'){
      this.setState({mapStyle : defaultMapStyle});
    }
    else{
      this.setState({mapStyle : satelliteMapStyle});      
    }
  }
  

  async changeTimePolygon(value){
    const {radius,pointGeojson,polygonGeojson,viewLaporan,view3D,viewPolygon,viewPengungsi,viewPintu} = this.state;
    var  plgeojson = [];
    var  pengungsiGeojson = [];
    var  pintuGeojson = [];
      // await axios.get(arrLinkPolygon[value]).then(
      //   function (response) {
      //     plgeojson = response.data;
      //   }
      // ).catch(function (error) {
      //   console.log(error);
      // });
    
    if(viewPolygon){
      var baseDate = this.getBaseDate(value);
      console.log(baseDate);
      this.setState({layerInfo : [{geojson : polygonGeojson,baseDate : baseDate}]});
    }
    
    
      if(viewPintu){
        await axios.get(arrLinkProvinsi[value]).then(
          function (response) {
            pintuGeojson.china = response.data;
          }
        ).catch(function (error) {
          console.log(error);
        });

        await axios.get(arrLinkProvinsiUS[value]).then(
          function (response) {
            pintuGeojson.us = response.data;
          }
        ).catch(function (error) {
          console.log(error);
        });
        
        if(value > 3){
          await axios.get(arrLinkProvinsiCanada[value - 4]).then(
            function (response) {
              pintuGeojson.canada = response.data;
            }
          ).catch(function (error) {
            console.log(error);
          });  
        }

        if(value > 2){
          await axios.get(arrLinkProvinsiMalaysia[value - 3]).then(
            function (response) {
              pintuGeojson.malaysia = response.data;
            }
          ).catch(function (error) {
            console.log(error);
          });  
        }

        pintuGeojson.jerman = this.state.pintuGeojson.jerman;        
      }

      
      

    // plgeojson = plgeojson.features.filter((o) => (o.geometry.type === 'Polygon'));
    // plgeojson = {"type":"FeatureCollection", "features" : plgeojson}
    
    // this.setState({ polygonGeojson: plgeojson});  
    // this.setState({ pengungsiGeojson: pengungsiGeojson});            
    this.setState({ pintuGeojson: pintuGeojson});            

    // this.setState({ layerInfo : [{geojson : polygonGeojson}]});            
        

    this._render3DLayers(pointGeojson,viewLaporan,radius,view3D,viewPolygon,viewPengungsi,pintuGeojson,viewPintu,value);
  }



  _updateSettings = (name, value) => {

    const {pointGeojson,view3D,viewPolygon,viewPengungsi,pintuGeojson,viewPintu,viewLaporan,indexPolygon} = this.state;
    const map = this.reactMap.getMap();
    
    if (name === 'year') {
      this.setState({year: value});
      this.changeTimePoint(value);
    }
    else if(name === 'radius'){
        this.setState({radius: value});
        this._render3DLayers(pointGeojson,viewLaporan,value,view3D,viewPolygon,viewPengungsi,pintuGeojson,viewPintu,indexPolygon);

    } 
    else if(name === 'indexPolygon'){
      this.setState({indexPolygon: value});
      this.changeTimePolygon(value);
    }
    else if(name === 'inspect'){
        this.setState({inspectMode : value});
    }
    else if(name === 'buffer'){

      this.setState({view3D : value});    
      this._refreshMap(value,viewLaporan,viewPolygon,viewPengungsi,viewPintu);
    }
    else if(name === 'layerPolygon'){
      this.setState({viewPolygon : value});    
      
      this._refreshMap(view3D,viewLaporan,value,viewPengungsi,viewPintu);
    }
    else if(name === 'indexPengungsi'){
      this.setState({indexPengungsi: value});
      this.changeTimePengungsi(value);
    }
    else if(name === 'viewPengungsi'){

      this.setState({viewPengungsi : value});    
      this._refreshMap(view3D,viewLaporan,viewPolygon,value,viewPintu);
    }
    else if(name === 'viewPintu'){
      // if (value) {
      //   map.setLayoutProperty('pintu-air', 'visibility', 'visible');


      //   } else {
      //     map.setLayoutProperty('pintu-air', 'visibility', 'none');
      // }

      this.setState({viewPintu : value});    
      this._refreshMap(view3D,viewLaporan,viewPolygon,viewPengungsi,value);
    }
    else if(name === 'viewLaporan'){
      this.setState({viewLaporan : value});    
      this._refreshMap(view3D,value,viewPolygon,viewPengungsi,viewPintu);
    }

  };

  _refreshMap(view3D,viewLaporan,viewPolygon,viewPengungsi,viewPintu){
    this.setState({layers3D: []});
    const {pointGeojson,radius,pintuGeojson,indexPolygon} = this.state;
  
    setTimeout(() => {
      this._render3DLayers(pointGeojson,viewLaporan,radius,view3D,viewPolygon,viewPengungsi,pintuGeojson,viewPintu,indexPolygon);
    },5
     );
 
     this._resize();
  }


 
  _onViewStateChangeDeck({viewState}) {
    this.setState({
      viewStateDeck: {...this.state.viewStateDeck, ...viewState}
    });


  }

  render() {
    
    const {viewState,layers3D,legendColor,viewPolygon,view3D,viewProvinsi,inspectMode,mapStyle,viewPengungsi,viewPintu,popupX,popupY} = this.state;    
    const {scaleCount,isPoint,viewBuffer,visualParams,dataColumn,selectedMode,year,layerSettings,radius,indexPolygon,width,minNegara,maxNegara,indexPengungsi,viewLaporan,showPopup,sumTerinfeksi} = this.state;
    const {popupInfo,popupProps,popupType,layerInfo,infoRekap} = this.state;
    var layerName = '';
    const isMobile = width <= 500;
    
    return (
      <div style={{height: "100%", width:"100%"}}
      >
        <div style={{position: 'absolute', top: 15, left: 10,zIndex : 14}}>
          <a href="https://geo.mapid.io">
          <img src="https://geomapid2019.s3-us-west-2.amazonaws.com/logo.svg" width="35" height="35" alt="Go to Dashboard"/>
          </a>
        </div>
                  <div className="btn-map-style">
        <Button.Group>
        <Button icon onClick={() => this._refreshMap(view3D,viewLaporan,viewPolygon,viewPengungsi,viewPintu)}>
          <Icon name='refresh' />
        </Button>
            <Button value='basic' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Street</Button>
       {(!isMobile) &&
        <div>
        <Button value='light' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Light</Button>
        <Button value='dark' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Dark</Button>      
        </div>
      }

            <Button value='satellite' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Satellite</Button>   
      </Button.Group> 
        </div>
      

        {
          legendColor &&

          <ControlCorona
          // containerComponent={this.props.containerComponent}
          settings={{
            scaleCount,
            isPoint,
            viewBuffer,
            visualParams,
            dataColumn,
            selectedMode,
            inspectMode,
            year,
            legendColor,
            layerSettings,
            radius,
            layerName,
            indexPolygon,
            arrPolygonDate,
            isMobile,
            viewPolygon,
            viewProvinsi,
            arrColorDAS,
            minNegara,
            maxNegara,
            indexPengungsi,
            arrPengungsiDate,
            viewPengungsi,
            viewPintu,
            viewLaporan,
            sumTerinfeksi,
            infoRekap
          }}
          onChange={this._updateSettings}
          style={{overflow: 'auto'}}
          ref={refMap}
          /> 
        }
 {
 !isMobile && 
  <LayerInfo
  visibility={viewPolygon}
  layerInfo={layerInfo}
  />
 }
  
      <div style={{height: "100vh", width:"100vw"}} id="map-render">
        <ReactMapGL
          mapStyle={mapStyle}
          mapboxApiAccessToken={MAPBOX_TOKEN}
          {...viewState}
          // refMap={refMap}
          ref={(reactMap) => this.reactMap = reactMap}
          preserveDrawingBuffer={true}
          attributionControl={false}
          onLoad={() => {this._onMapLoaded()}}
          customAttribution={'<a href="https://mapid.io/">Geomapid v2.0 | © MAPID </a>| <a href="https://www.maptiler.com/copyright/">© MapTiler </a> | <a href="https://www.openstreetmap.org/copyright">© OpenStreetMap contributors </a> | <a href="https://petabencana.id/map/jakarta">© Data Source: petabencana.id</a>'}
      >
 {
           ( showPopup && inspectMode) && 
            <Popup
          latitude={popupX}
          longitude={popupY}
          closeButton={true}
          closeOnClick={false}
          onClose={() => this.setState({showPopup: false})}
          anchor="top" 
          style={
          {
          position: 'absolute',
          margin: 8,
          padding: 4,
          background: 'rgba(0, 0, 0, 0.8)',
          color: '#fff',
          maxWidth: 300,
          fontSize: 10
            }
          }
          >
             <div>              
            {
            popupProps.header.map(function(key,index) {       
              return ( 
                <div key={index}>
                    <div>{key}{(popupType === 'laporan' && index === 0 ) ? popupInfo['properties']['report_data'][popupProps.name[index]] : popupInfo['properties'][popupProps.name[index]]} {popupProps.content[index]}<br /></div>            
                </div>
                )
            })
          }
            </div>
          </Popup>

          }

            <DeckGL initialViewState={viewState}
            controller={true}
            layers={layers3D}
            {...viewState}
          // onViewStateChange={this._onViewStateChange.bind(this)}
          onViewStateChange={this._onViewStateChange.bind(this)}
          glOptions={{preserveDrawingBuffer: true}}
            >
              
          { inspectMode && this._renderTooltip() }
        </DeckGL>
          
      
    
      </ReactMapGL>
    </div>
</div>
    )}  
}
