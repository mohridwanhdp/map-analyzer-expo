import React, { PureComponent } from 'react';
import {Table} from 'semantic-ui-react';
import {json as requestJson} from 'd3-request';
import {Checkbox,Loader,Dimmer,Segment,Modal,Message} from 'semantic-ui-react';
import axios from 'axios';
const LoadingSpinner = () => (
 
  <div style={{height: 200, width:"100%",backgroundColor : '#000',zIndex :9,color : 'white',position : 'absolute',bottom : 0}}>
   <Segment style={{height: "100%", width:"100%",backgroundColor : '#000',zIndex :9,color : 'white'}}>
     <Dimmer active>
       <Loader>Load data</Loader>
     </Dimmer>
 </Segment>  </div>
);
const mobileStyle = {
  maxWidth : 400,
  height : 300,
  maxHeight : 250,
  overflow : 'auto',
  size : 10
}

class LayerInfo extends PureComponent {
    
    constructor(props){
        super(props);
        
        this.state = {
            arrChecked : [],
            layer : props.layerInfo,
            column : null,
            features : null,
            isLoading : true,
            selectedRow : null,
            isUrl : false,
            isError : false,
            selectedMode : 'height'    
        };
        this.loadJson = this.loadJson.bind(this);
        this.check_me = React.createRef();
        this.getJsonData = this.getJsonData.bind(this);
        this.handleChangeRow = this.handleChangeRow.bind(this);
        this.onChange = this.onChange.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    closeModal(){
      this.setState({isError : false});
    }

  componentDidMount(){
    const isMobile = window.innerWidth <= 500;
    if(isMobile && this.props.layerInfo){
      var geojson = '';
      const layer = this.props.layerInfo;
      if(typeof layer[0].geojson === 'object'){
        geojson = layer[0].geojson;
        this.setState({geojson : layer[0].geojson});              
        const props = geojson.features[0].properties;                        
        const col = Object.keys(props);
        console.log(col);
        const features = geojson.features;
        this.setState({ column : col,features : features,isLoading : false});
        this.setState({isUrl : false});
      } else{
        this.setState({isUrl : true});
        this.getJsonData(layer[0].geojson);  
      }

    }
  } 

  componentWillReceiveProps(nextProps) {
    // You don't have to do this check first, but it can help prevent an unneeded render
    if(nextProps.visibility === true){
      if (nextProps.layerInfo !== this.state.layer) {
        this.setState({selectedRow : null,isLoading : true}); 
        const layer = nextProps.layerInfo;
        console.log(layer);

        var geojson;

        if(typeof layer[0].geojson === 'object'){
          geojson = layer[0].geojson;
          this.setState({geojson : layer[0].geojson}); 
          const baseDate = layer[0].baseDate;

          const col = ['COUNTRY','INF_'+baseDate,'DTH_'+baseDate,'REC_'+baseDate];
          const features = geojson.features;
          this.setState({ column : col,features : features,isLoading : false});
          this.setState({isUrl : false});
        } else{
          this.setState({isUrl : true});
          this.getJsonData(layer[0].geojson);  
        }
        this.setState({ layer: nextProps.layerInfo}); 
        this.setState({selectedMode : nextProps.selectedMode});
      }
    }
    
  }

    loadJson(DATA){
      var dt;
      requestJson(DATA, (error, response) => {
        if (!error) {
        
          this.setState({ geojson: response });
          dt =  response;
        }
        else{
            JSON.stringify(error);
        }
      });
      return dt;
    }

   fitBounds(f){
      this.props.fitBounds(f);
   } 

    onChange(val,stat,i){
      var arrChecked = this.state.arrChecked;
      var isError = false;
      const {features,selectedMode} = this.state;
      arrChecked = arrChecked.filter(function(ele){
        return ele !== val;
      });

      if(stat){
         
        if(selectedMode === 'height'){
          isError = features.some(element => {
            return isNaN(element.properties[val]);
          });

        }
        if(isError){
          this.setState({isError:true});
          return;
        }
        this.props.add(val);
      }
      else{
          this.props.delete(val);
      }
      this.forceUpdate();
    }

    getJsonData = async(url) => {
      var x = await axios.get(url);
      
      var {data} = x;
      this.setState({geojson : data});
      const props = data.features[0].properties;                        
      const col = Object.keys(props);
      const features = data.features;
      this.setState({column : col,features : features, isLoading : false});
        
      return data;
    }

    handleChangeRow(value){
      const {geojson} = this.state;
      var ly = '';
      ly = geojson.features.filter(ft => ft.properties.mapid_id === value);        
      this.fitBounds(ly[0]);
      this.setState({ selectedRow : value });      
    }

    render() {
     const {features,isLoading,selectedRow,isError,column} = this.state;
    const columnHeader = ['Country','Infected','Death','Recovered'];

    //  if(column){
    //     column = column.filter((x) => {return x !== 'mapid_id' || x !== 'OBJECTID' });
    //  }
     
     
     var arrChecked = this.state.arrChecked;
    const isMobile = window.innerWidth <= 500;
    if(isMobile){
        return(
          <div style={mobileStyle} >      
                <Table collapsing unstackable celled size='small'  >
                    <Table.Header>
                      <Table.Row>
                          {/* <Table.HeaderCell >
                            Go To 
                          </Table.HeaderCell > */}
                      
                        { column && 
                              
                              column.map((l, index) => (
                                    <Table.HeaderCell key={index}  >
                                      <center><Checkbox
                                    label={l}
                                    name='column'
                                    value={l}
                                    checked={arrChecked.map(function(x) {return x; }).indexOf(l) < 0 ? false : true}
                                    // ref="check_me"
                                    onClick={(e, {label,checked}) => {this.onChange(label,checked,index)}}
                                    /></center>
                                     {/* <label style={{fontSize: '10px'}}>{l}</label> */}
                                     </Table.HeaderCell>
                                ))
                        }
                    </Table.Row>
                    </Table.Header>
                  { features && 
                    
                    <Table.Body >        
                    {
                      features.map((f,index) => (
                      <Table.Row key={index}>
                        {/* <Table.Cell>
                        <Radio
                                  name='radioGroup'
                                  value={f.properties['mapid_id']}
                                  checked={selectedRow === f.properties['mapid_id']}
                                  onChange={(e, {value}) => {this.handleChangeRow(value); }}
                                />
                        </Table.Cell> */}
                        
                          {
                            
                            column.map(function(l,index) {
                                return <Table.Cell key={index} style={{fontSize : 18}}>{f.properties[l]}</Table.Cell>
                                })
                          }
                            </Table.Row>
                      ))  
                      
                    }
                    </Table.Body> 
                  }
                </Table>
            </div>
        );
    }
    else{
      return (
        
        (isLoading && this.props.visibility) &&
          <LoadingSpinner/> || 
        
        <div className={this.props.visibility ? 'layer-info-corona' : 'layer-info-hidden'}>
            
{
  this.props.visibility && 
  <Table celled>
  <Table.Header>
    <Table.Row>

   
    
      { columnHeader && 
            
            columnHeader
            .map((l, index) => (
                  <Table.HeaderCell key={index}>
                    <center>{l}</center></Table.HeaderCell>
              ))
      }
  </Table.Row>
  </Table.Header>
{ features && 
  
  <Table.Body>        
   {
     features.map((f,index) => (
     <Table.Row key={index}>
       {/* <Table.Cell>
       <Radio
                 name='radioGroup'
                 value={f.properties['mapid_id']}
                 checked={selectedRow === f.properties['mapid_id']}
                 onChange={(e, {value}) => {this.handleChangeRow(value); }}
               />
       </Table.Cell> */}
      
         {
           
           column
           .filter((col) => !col.startsWith("id"))
           .filter((col) => !col.startsWith("color"))
           .filter((col) => !col.startsWith("mapid_id"))
           .filter((col) => !col.startsWith("icon-image"))
           .map(function(l,index) {
              return <Table.Cell key={index} style={{fontSize : 16}} >{String(f.properties[l]) === 'default' ? 0  : f.properties[l]}</Table.Cell>
              })
         }
          </Table.Row>
     ))  
     
   }
  </Table.Body> 
}
</Table>
}
        
              </div>
            );
        }
    }
    }
     

export default LayerInfo;
