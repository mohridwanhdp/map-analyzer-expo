import React, { PureComponent } from 'react';
import {Table} from 'semantic-ui-react';
import DATA from './data/yearexample.geojson';
import {json as requestJson} from 'd3-request';

class LayerInfo extends PureComponent {
    
    constructor(props){
        super(props);
        
        this.state = {
          features : null
        };
        console.log(props);
    }

    componentDidMount(){
      this.loadJson();
    }

    loadJson(){
      requestJson(DATA, (error, response) => {
        if (!error) {
          this.setState({features : response.features});
        }
      });
     
    }

   
    render() {
      const {features} = this.state;  
      return (
            <div className={this.props.visibility ? 'layer-info' : 'layer-info-hidden'}>
            <Table celled structured>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell rowSpan='2'> <center>Name</center></Table.HeaderCell>
                    <Table.HeaderCell colSpan='2'><center> SQM / Year </center></Table.HeaderCell>                    
                  </Table.Row>
                  <Table.Row>
                  <Table.HeaderCell><center>2016</center></Table.HeaderCell>
                  <Table.HeaderCell><center>2017</center></Table.HeaderCell>                     
                </Table.Row>
                </Table.Header>
              { features && 
                
                <Table.Body>
                 {
                   
                   features.map((f) =>(
                   <Table.Row>
                      
                    <Table.Cell>
                    {f.properties.name}
                    
                    </Table.Cell>
                    <Table.Cell>
                    {f.properties.value["2016"].valuePerSqm}
                    
                    </Table.Cell>
                    <Table.Cell>
                    {f.properties.value["2017"].valuePerSqm}
                    
                    </Table.Cell>
                    </Table.Row>
                   ))  
                
                 }
                </Table.Body>
              }
                
              </Table>
              </div>
            );
            
        }
    }

export default LayerInfo;