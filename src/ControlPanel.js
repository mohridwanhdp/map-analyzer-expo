import React, {PureComponent} from 'react';
import { Button,Icon,Modal, Header,Form, Checkbox, Divider, Radio,Dropdown, Input,Loader,Dimmer,Segment} from 'semantic-ui-react';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import 'jspdf-autotable';

const defaultContainer = ({children}) => <div className="control-panel" id="control-panel" style={{overflow : 'auto',maxWidth : 400}}>{children}</div>;
const Rainbow = require('rainbowvis.js');
const logoMAPID = './logo.svg';
var groupColor = new Rainbow();

const LoadingSpinner = () => (
 
  <div style={{height: "100%", width:"100%",backgroundColor : '#000',zIndex :9,color : 'white',position : 'static'}}>
   <Segment style={{height: "100%", width:"100%",backgroundColor : '#000',zIndex :9,color : 'white',position : 'static'}}>
     <Dimmer active>
       <Loader>Printing document . . .</Loader>
     </Dimmer>
 </Segment>  </div>
);
const  mobileStyle = {
    position: 'absolute',
    overflow: 'auto',
    background: '#fff',
    padding: 10,
    fontSize: 12,
    color : '#6b6b76',
    outline: 'none',
    textTransform: 'uppercase',
    zIndex: 4,
    maxHeight: 250,
    height : 300,
    classNumber : 0 ,
    classNumberValue : 0

}

function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

function HeightLegend(props){
  const settings = props.settings;
  const legendItem = props.legendItem;
  
  var i = 0;
  var ele = [];
  var min = settings.layerSettings.smallestName;
  var max = settings.layerSettings.largestName;
  var temp = min;
  var increment = round((max - min) / settings.scaleCount, 2);

  if(props.enableDecimal){
    for(i = 0 ; i < settings.scaleCount;i ++){  
      ele.push(<Divider style={{wrapContent : true,minHeight : 20,padding :0,borderStyle: 'none'}}>
        <div style={{backgroundColor : '#' + legendItem[i],width : 30, height : 20,marginRight : 5,float : 'left'}}/>
        <div style={{borderColor : 'white',padding : 2, textDecoration : 'none'}}> 
          {i === 0 ? min : round(temp + 0.01,2)} - { (temp+increment <= max - 0.01) ? round(temp+increment,2) : max}   
          
        </div>
        </Divider>);
    temp = temp + increment ;
  }
  }else{
  var increment = round((max - min) / settings.scaleCount, 0);

    for(i = 0 ; i < settings.scaleCount;i ++){  
      ele.push(<Divider style={{height : 'auto',backgroundColor : 'rgba(0,0,0,0)',borderStyle: 'none'}}>
        <div style={{backgroundColor : '#' + legendItem[i],width : 30, height : 20,float : 'left',marginRight : 5}}/>
        <div style={{borderColor : 'white',padding : 2, textDecoration : 'none'}}> 
          {i === 0 ? min : round(temp + 1,0)} - { (temp+increment <= max -1) ? round(temp+increment,0) : max}   
          
        </div>
        </Divider>);
    temp = temp + increment ;
  }
  }
    
  
  return min === 'Fewer' ? 'Data is loaded . . . ': min === max ? '' : <div id='legend-layout' style={{marginBottom : 5,height: 'auto',backgroundColor : 'rgba(0,0,0,0)'}}> {ele} </div>;
}

function ColorLegend(props){

  const items = props.items;
  
  return <div id='legend-layout'> 
   {items.map((i) => {
    // <Divider  inverted={true} style={{backgroundColor : i.color,wrapContent : true ,minHeight : 25 , margin : 0,padding : 3}}>
    //   <center>{i.name}</center>
    // </Divider>
    var fontSz = i.name.toString().length < 20 ? 14 : 11;
    return (
    <Divider style={{wrapContent : true,minHeight : 20,borderStyle: 'none'}}>
      <div style={{backgroundColor : i.color,width : 30, height : 20,float : 'left',marginRight : 5}}/>
      <div style={{borderColor : 'white',padding : 2, textDecoration : 'none', height : 'wrap-content',fontSize : fontSz}}>{i.name}</div>
    </Divider>
    );
   })}
  </div>
}
export default class ControlPanel extends PureComponent {
  constructor(props){
    super(props);
    this.state = {
      smallestName : 'Fewer',
      largestName : 'More',
      smallestColor : '#FF0000',
      largestColor : '#00FF00',
      colorPicked : false,
      openModal : false,
      selectedColor : "darkred,yellow,darkgreen",
      classNumber : 6,
      errorMessage : "The value of class number must between 2 and 15",
      errorStat : false,
      minimized : false,
      isLoading : false
    };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.saveLayerSetting = this.saveLayerSetting.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeComplete = this.handleChangeComplete.bind(this);
    this.validationCheck = this.validationCheck.bind(this);
    this.getDataUri = this.getDataUri.bind(this);
    groupColor.setSpectrum('darkred','red','orange','yellow','green','blue','darkblue','violet','purple');
    this.minimizePanel = this.minimizePanel.bind(this);
    

  }

  closeModal(){
    this.setState({openModal : false});
  }
  openModal(){
    this.setState({openModal : true});
  }

  saveLayerSetting = () => {
    const {selectedColor,classNumber} = this.state;
    var arrColor = selectedColor.split(',');
    var smallestColor = arrColor[0];
    var midColor = arrColor[1];
    var largestColor = arrColor[2];
    this.props.onChange('layerSettings',{smallestColor,midColor,largestColor,classNumber});

    this.closeModal();
  }
  _getDistanceTwoPoints(x, y,measurement) {
    const [lng1, lat1] = x;
    const [lng2, lat2] = y;
    const MILE_IN_KILOMETERS = 1.60934;
    // Radius of the earth in km or miles
    const R = measurement === 'km' ? 6371 : 6371 / MILE_IN_KILOMETERS;
    const dLat = this._deg2rad(lat2 - lat1);
    const dLng = this._deg2rad(lng2 - lng1);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this._deg2rad(lat1)) *
        Math.cos(this._deg2rad(lat2)) *
        Math.sin(dLng / 2) *
        Math.sin(dLng / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c;

    return d;
  }

  validationCheck(){
    const {classNumber} = this.state;
    if(classNumber < 2 || classNumber > 15){
      this.setState({errorStat : true})
    }
    else{
      this.setState({errorStat : false})

      this.saveLayerSetting();

    }
  }
  handleChangeComplete(value){
      this.setState({ selectedColor: value });

  }
  handleChangeMode(val){
    // this.setState({selectedMode : val})
    this.props.onChange('mode',val);
  }
  handleChange(val){
    this.setState({[val.target.name] : val.target.value});
    // const {name,value} = val;
  }

  handleChangeClassNumber(val){
    this.setState({classNumber : val});
  }

  UNSAFE_componentWillReceiveProps(nextProps){
    if(nextProps.scaleCount !== this.state.classNumber){
      this.setState({classNumber : nextProps.classNumber});
      this.setState({classNumberValue : nextProps.classNumber});

    }

  }

  convertMapScale(strScale){
    var scale = 0;
    var arrStrScale = strScale.split('k');
    
    if(arrStrScale[1]){
      scale = arrStrScale[0] * 1000;
    }else{
     arrStrScale = strScale.split('m');
      scale = arrStrScale[0];      
    } 
    
    return scale
  }

  async printPDF({projectName, layerName, features,column,scale}){
      // var sc = 1000;
      var sc = this.convertMapScale(features,scale);
      var imgLegend = '';
      var bgLegend = '';
      var logo = await this.getDataUri('https://geomapid-layer.s3-us-west-2.amazonaws.com/logo.svg');
      console.log(logo);
      html2canvas(document.getElementById('legend-layout'), {
        allowTaint : true,
        removeContainer : true,
        foreignObject : true,
        useCORS : true,
        backgroundColor : 'rgba(255,255,255,0.1)'
      }).then(function(canvas2){
        imgLegend = canvas2.toDataURL('image/png');
      });

      var bg = document.createElement("div");
      var compass = '';
      bg.id = "bg-legend"
      bg.style.width =  '54px';
      bg.style.height = '180px';
      bg.style.backgroundColor = 'rgba(255,255, 255, 0.0)';
      document.body.appendChild(bg);
      
      var divCompass = document.querySelectorAll('.mapboxgl-ctrl-icon,.mapboxgl-ctrl-compass');
      console.log(divCompass);
      html2canvas(divCompass[5],{
        allowTaint: true,
        removeContainer : true,
        foreignObject : true,
        useCORS : true,
        backgroundColor : 'rgba(255,255, 255, 0.0)'
      }).then(function(canvas) {
        compass = canvas.toDataURL('image/png');
      })

      html2canvas(document.getElementById('bg-legend'), {
        allowTaint: true,
        removeContainer : true,
        foreignObject : true,
        useCORS : true,
        backgroundColor : 'rgba(255,255, 255, 0.7)'
      }).then(function(canvas) {
        bgLegend = canvas.toDataURL('image/png');
      });

      html2canvas(document.getElementById('map-render'), {
        allowTaint: true,
        removeContainer : true,
        foreignObject : true,
        useCORS : true
      }).then(function(canvas) {
          const imgData = canvas.toDataURL('image/png');
          const pdf = new jsPDF({
            orientation : 'landscape'
          });
          const imgProps= pdf.getImageProperties(imgData);
          const imgLegendProps = pdf.getImageProperties(imgLegend);
          // const pdfWidth = pdf.internal.pageSize.getWidth();
          pdf.fromHTML(document.getElementById('legend-layout'),230,25);
          
          /*Versi full jangan didelete dulu */
          //MAP
          pdf.addImage(imgData, 'PNG', 5, 15,287.5,180);

          //Compass
          pdf.addImage(compass, 'PNG', 225, 20,10,10);

          pdf.addImage(
            bgLegend,238,15,54,180
          );

          // pdf.fromHTML(document.getElementById('bg-legend'),238,15);
          pdf.setDrawColor(0);
          pdf.setFillColor(255,255,255);
          pdf.rect(238, 15, 54, 180);
          pdf.rect(5, 15, 287.5, 180);

          pdf.addImage(imgLegend, 'PNG', 240, 90,48,imgLegendProps.height / 5 < 140 ? imgLegendProps.height / 5 : 140);
          
          pdf.setFontSize(12);
          var reportTitle = 'PETA ' + String(projectName).toUpperCase();
          var splitted = pdf.splitTextToSize(reportTitle, 50);
          pdf.text(splitted ,240,52.5);

          //Skala
          pdf.text('Skala = 1 : '+sc,10,190);
          
          pdf.setFontSize(9.5);
          pdf.text('PT Multi Areal Planing Indonesia',240,42);          
          pdf.setFontSize(10);

          var splittedLayer = pdf.splitTextToSize('Layer : ' + layerName,50);
          pdf.text(splittedLayer ,240,70);
          pdf.line(238, 48, 238+54, 48); // horizontal line
          pdf.line(238, 80, 238+54, 80); // horizontal line

          pdf.text('LEGENDA',240,85);
          pdf.addImage(logo,'PNG',255,20,15,15);
           
          //====PAGE KEDUA=====
          pdf.addPage();
          
          var head = [column];
          var body = features.map((f) => {
              var el = column.map((cl) =>
                  (f.properties[cl])
              );
              return el;
            }
          );

          
          pdf.autoTable({head: head, body: body});
          
          pdf.save(projectName+'_' +layerName+".pdf");
          
        });

        this.setState({isLoading : false});
  }

getDataUri(url)
{
  return new Promise(resolve => {
      var image = new Image();
      image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain

      image.onload = function () {
          var canvas = document.createElement('canvas');
          canvas.width = this.naturalWidth;
          canvas.height = this.naturalHeight; 

          //next three lines for white background in case png has a transparent background
          var ctx = canvas.getContext('2d');
          // ctx.fillStyle = 'rgba(255,255,255,0.6)';  /// set white fill style
          // ctx.fillRect(0, 0, canvas.width, canvas.height);
          canvas.getContext('2d').drawImage(this, 0, 0);

          resolve(canvas.toDataURL('image/png'));
      };
      image.src = url;
  })
}

getImageFromUrl = function(url) {
	var img = new Image, data, ret={data: null, pending: true};
  var dt ; 
  
	img.onError = function() {
		throw new Error('Cannot load image: "'+url+'"');
	}
	img.onload = function() {
  console.log('MASUK');

		var canvas = document.createElement('canvas');
		document.body.appendChild(canvas);
		canvas.width = img.width;
		canvas.height = img.height;

		var ctx = canvas.getContext('2d');
		ctx.drawImage(img, 0, 0);
		// Grab the image as a jpeg encoded in base64, but only the data
		data = canvas.toDataURL('image/jpeg');
		// Convert the data to binary form
		data = atob(data)
    dt = data;
	}
	return dt;
}
minimizePanel(){
  const {minimized} = this.state;
  this.setState({minimized : !minimized})
}
  render() {
    
    const Container = this.props.containerComponent || defaultContainer;
    const {settings} = this.props;
    const {errorStat,minimized} = this.state;
    const {openModal,selectedColor,classNumber} = this.state;
    const {isLoading} = this.state;
    var len = settings.visualParams.length;
    var i;
    var legendItem = [];
    
    for(i = 0 ; i < settings.scaleCount ; i++){
       legendItem.push(settings.legendColor.colourAt(i));    

    }

  var isPoint = settings.isPoint;
  var viewBuffer = settings.viewBuffer;

    var groupName = settings.dataColumn;
    var groupItem =  [];

    if(settings.selectedMode !== 'height'){
      if(settings.selectedMode === 'color' && groupName.length >0){
        groupColor = settings.legendColor;
        groupColor.setNumberRange(0,groupName.length > 1 ? groupName.length - 1 : 1) 
        groupItem = groupName.map((f,index) => {
          var obj = {
            name : f,
            color : '#' + groupColor.colourAt(index)
          };
          
          return obj;
         });
          
      }
    }
    var options = [
      { key : 1 , text : <div  key={i} style={{width : '100%', height : 10, background :'linear-gradient(to right, darkred,yellow,darkgreen)'}}></div> , value : "darkred,yellow,darkgreen"},  
      { key : 2 , text : <div  key={i} style={{width : '100%', height : 10, background :'linear-gradient(to right, darkgreen,yellow,darkred)'}}></div> , value : "darkgreen,yellow,darkred"},
      { key : 3 , text : <div  key={i} style={{width : '100%', height : 10, background :'linear-gradient(to right, lightblue,blue,darkblue)'}}></div> , value : "lightblue,blue,darkblue"},  
      { key : 4 , text : <div  key={i} style={{width : '100%', height : 10, background :'linear-gradient(to right, lightgreen,green,darkgreen)'}}></div> , value : "lightgreen,green,darkgreen"},
      { key : 5 , text : <div  key={i} style={{width : '100%', height : 10, background :'linear-gradient(to right, lightcoral,red,maroon)'}}></div> , value : "lightcoral,red,maroon"}  
    ];

    const styleDropdown = selectedColor.split(',');
    const isMobile = window.innerWidth <= 500;
    
    
    // Mobile page layout
    if(isMobile){
      return (
        <div id='legend-control' style={mobileStyle}>
          
          <div className={"setting-button"}>
            <Button onClick={this.openModal} icon >
              <Icon name='setting'/>
            </Button>                     
        </div>
      <Modal basic size='small'
        open={openModal}
        onClose={this.closeModal}
      >
      
      <Header icon='setting' content='Legend Setting' />
      <Modal.Content>
      <Form >
      <Form.Field >
      <label style={{'color' : 'white'}}>Color gradient</label>
        <Dropdown
            onChange={(e, {value}) => this.handleChangeComplete(value)}
            options={options}
            placeholder='Choose an option'
            selection
            value={selectedColor}
            style={{background : 'linear-gradient(to right,' + styleDropdown[0] + ',' +  styleDropdown[1] + ',' + styleDropdown[2]+ ')'}}
            />
      
      </Form.Field>
  
      <Form.Field>
          <label style={{'color' : 'white'}}>Class number</label>
          <Form.Input 
            
                error={ errorStat ? 'The value of class number must between 2 and 15' : false}
                type="number" min="2" max="15" defaultValue={this.state.classNumber} value={this.state.classNumberValue} onChange={(e, {value}) => this.handleChangeClassNumber(value)}/>
      </Form.Field>
    </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' inverted onClick={this.closeModal}>
          <Icon name='remove' /> Cancel
        </Button>
        <Button onClick={this.validationCheck} color='green' inverted>
          <Icon name='checkmark' /> Done
        </Button>
      </Modal.Actions>
      
    </Modal>
          
          <div>
          
          {/* Untuk inspect mode, mengeluarkan info saat layer di-hover */}
            <div width="100%">
                <Radio
                    name='viewMode'
                    value='height'
                    label='Value as height'
                    checked={settings.selectedMode === 'height'}
                    onChange={(e, {value}) => this.handleChangeMode(value)}
                  />
                <Radio
                name='viewMode'
                value='color'
                label='Value as group'
                checked={settings.selectedMode === 'color'}
                onChange={(e, {value}) => this.handleChangeMode(value)}
              />                  
              <Checkbox
                style={{width : "100%"}}
                label='Inspect Mode'
                name='inspectMode'
                value={settings.inspectMode}
                checked={settings.inspectMode}
                onClick={(e, {checked}) => this.props.onChange('inspect',checked)}
              />
            </div>
          {
            len === 0 ? 'Check the column table ' : ''
          }
  
         
          </div>
          Active layer    : {settings.layerName}
          <br/>
          Active column : { len === 0 ? '-' : settings.visualParams[settings.year] }
          <div key={'year'} className="input">
            
          {
            len > 0 &&
            <input
              type="range"
              value={settings.year}
              min={0}
              max={len - 1}
              step={1}
              onChange={evt => this.props.onChange('year', evt.target.value)}
            />
          }
          </div>
          <div className="input">
          <hr />
          {
            isPoint &&
            <div>
  
  
           {/* <h4>Radius</h4> */}
            
         Radius value : {settings.radius} {settings.isCorona ? ' Kilometer': ' Meter'} 
            </div>
         }
         <Checkbox
              label='View 2D'
              name='controlPanel'
              value={viewBuffer}
              onClick={(e, {checked}) => this.props.onChange('buffer', checked)}
              />
        {  isPoint &&     
            <input
            type="range"
            value={settings.radius}
            min={25}
            max={5000}
            step={50}
            onChange={evt => this.props.onChange('radius', evt.target.value)}
          />
          
          
          }
          </div>
                 <hr />
          {
           settings.selectedMode === 'height' ? <HeightLegend settings={settings} legendItem={legendItem}  onChange={this.props.onChange}/> : <ColorLegend settings={settings} items={groupItem} onChange={this.props.onChange}/>
          }
          
        </div>
      )
    }
    // Web page layout
    else{
      return (
        minimized &&  
        <Button style={{
          position: 'absolute',
          right: "3%",
          margin: 15,
          zIndex: 4
        }} 
        color = 'blue'
        onClick={this.minimizePanel} icon >
        <Icon name='angle double left'/>
      </Button>
      
      || 
      
      <div>      
      {
        isLoading && <LoadingSpinner/>  
      }
        <Container>
         
          <h3>Legend</h3>
          <div className={"setting-button"}>
          <Button onClick={this.openModal} icon >
            <Icon name='setting'/>
          </Button>        
  
          <Button icon onClick={() => {this.setState({isLoading : true});this.printPDF(settings.reportParam)}}>
            <Icon name='print'/>
          </Button>
          <Button onClick={this.minimizePanel} icon >
            <Icon name='angle double right'/>
          </Button>   
        </div>
      <Modal basic size='small'
        open={openModal}
        onClose={this.closeModal}
      >
      
      <Header icon='setting' content='Legend Setting' />
      <Modal.Content>
      <Form >
      <Form.Field >
      <label style={{'color' : 'white'}}>Color gradient</label>
        <Dropdown
            onChange={(e, {value}) => this.handleChangeComplete(value)}
            options={options}
            placeholder='Choose an option'
            selection
            value={selectedColor}
            style={{background : 'linear-gradient(to right,' + styleDropdown[0] + ',' +  styleDropdown[1] + ',' + styleDropdown[2]+ ')'}}
            />
      
      </Form.Field>
  
      <Form.Field>
          <label style={{'color' : 'white'}}>Class number</label>
          <Form.Input 
            
                error={ errorStat ? 'The value of class number must between 2 and 15' : false}
                type="number" min="2" max="15" defaultValue={this.state.classNumber} value={this.state.classNumberValue} onChange={(e, {value}) => this.handleChangeClassNumber(value)}/>

                  
      </Form.Field>
    </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' inverted onClick={this.closeModal}>
          <Icon name='remove' /> Cancel
        </Button>
        <Button onClick={this.validationCheck} color='green' inverted>
          <Icon name='checkmark' /> Done
        </Button>
      </Modal.Actions>
      
    </Modal>
  
          <hr />
          
          <div>
          
          {/* Untuk inspect mode, mengeluarkan info saat layer di-hover */}
            <div width="100%">
                <Radio
                    name='viewMode'
                    value='height'
                    label='Value as height'
                    checked={settings.selectedMode === 'height'}
                    onChange={(e, {value}) => this.handleChangeMode(value)}
                  />
                <Radio
                name='viewMode'
                value='color'
                label='Value as group'
                checked={settings.selectedMode === 'color'}
                onChange={(e, {value}) => this.handleChangeMode(value)}
              />                  
              <Checkbox
                style={{width : "100%"}}
                label='Inspect Mode'
                name='inspectMode'
                value={settings.inspectMode}
                checked={settings.inspectMode}
                onClick={(e, {checked}) => this.props.onChange('inspect',checked)}
              />
            </div>
          {
            len === 0 ? 'Check the column table ' : ''
          }
  
         
          </div>
          Active layer    : {settings.layerName}
          <br/>
          Active column : { len === 0 ? '-' : settings.visualParams[settings.year] }
          <div key={'year'} className="input">
            
          {
            len > 0 &&
            <input
              type="range"
              value={settings.year}
              min={0}
              max={len - 1}
              step={1}
              onChange={evt => this.props.onChange('year', evt.target.value)}
            />
          }
          </div>
          <div className="input">
          <hr />
          {
            isPoint &&
            <div>
  
  
           {/* <h4>Radius</h4> */}
            
         Radius value : {settings.radius}  {settings.isCorona ? ' Kilometer': ' Meter'} 
            </div>
         }
         <Checkbox
              label='View 2D'
              name='controlPanel'
              value={viewBuffer}
              onClick={(e, {checked}) => this.props.onChange('buffer', checked)}
              />
        {  isPoint &&     
            <input
            type="range"
            value={settings.radius}
            min={25}
            max={5000}
            step={50}
            onChange={evt => this.props.onChange('radius', evt.target.value)}
          />
          
          
          }
          </div>
                 <hr />
          {
           settings.selectedMode === 'height' ? <HeightLegend settings={settings} legendItem={legendItem}  onChange={this.props.onChange} enableDecimal = {true} /> : <ColorLegend settings={settings} items={groupItem} onChange={this.props.onChange}/>
          }
          
        </Container>

      </div>
      );
    }
    
  }
} 
