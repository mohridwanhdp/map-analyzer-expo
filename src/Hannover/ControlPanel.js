import React, {PureComponent} from 'react';
import { Icon, Button ,Checkbox} from 'semantic-ui-react';
import icon1 from './image/icon 1.png'
import icon2 from './image/icon 2.png'
import icon3 from './image/icon 3.png'
import icon4 from './image/icon 4.png'
import icon5 from './image/icon 5.png'
import icon6 from './image/icon 6.png'
import img1 from './image/content img1.png'
import img2 from './image/content img2.png'
import JAKARTA from './data/Jakarta.geojson'
import INDONESIA from './data/Indonesia.geojson'
import JAWA from './data/Hutan/Jawa.geojson'
import KALIMANTAN from './data/Hutan/Kalimantan.geojson'
import BALI_NT from './data/Hutan/Bali_dan_Nusa_Tenggara.geojson'
import MALUKU from './data/Hutan/Maluku.geojson'
import PAPUA from './data/Hutan/Papua.geojson'
import SUMATERA from './data/Hutan/Sumatera.geojson'
import SULAWESI from './data/Hutan/Sulawesi.geojson'


import {json as requestJson} from 'd3-request';

const buttonViewStyle = {
  position : 'absolute',right : 0,top : '2px',backgroundColor : 'rgba(0,0,0,0)'
}
const defaultContainer = ({children}) => <div className="hannover-control-panel" id="control-panel" >{children}</div>;
const elementCount = [1,1,2,1,1,1,1,1,1];

// function FactorMoving(props){
//   return (
   
//   )
// }
export default class ControlPanel extends PureComponent {
  constructor(props){
    super(props);
    this.state = {
      index : 1,
      year : 2015,
      arrVisibility : null
    }

    this.nextIndex = this.nextIndex.bind(this);
    this.prevIndex = this.prevIndex.bind(this);
    this.fitBounds = this.fitBounds.bind(this);
    this.FactorMoveView = this.FactorMoveView.bind(this);
    this.setElementVisibility = this.setElementVisibility.bind(this);
    this.BestPlaceToBuild = this.BestPlaceToBuild.bind(this);
  }

  componentDidUpdate(prevProps, prevState){
      if(prevState.index !== this.state.index){
        this.setElementVisibility();
      }
  }
  
  onChangeVisibility(index,value){
    var arr = this.state.arrVisibility;
    arr[index] = value;
  }

  FactorMoveView = () => {
    return (
        <div>
          <div>
            <div>Ruang terbuka hijau</div>
            <div style={{position : 'relative'}}>
              <input type='range' min={2015} max={2019} step={1} value={this.state.year} onChange={evt => {this.setState({year : evt.target.value });this.props.updateSetting('year', evt.target.value);}}/>
              <Button icon color='black'   style={buttonViewStyle}>
                  <Icon name={true ? 'eye' : 'eye slash'} />
              </Button>
        
            </div>

          </div>
          <div style={{position : 'relative'}}>
            <div>Kepadatan penduduk</div>
              <div>
                <input type='range' min={2015} max={2019} step={1} value={this.state.year} onChange={evt => {this.setState({year : evt.target.value });this.props.updateSetting('year', evt.target.value);}}/>
                <Button icon color='black'   style={buttonViewStyle}>
                  <Icon name={true ? 'eye' : 'eye slash'} />
              </Button>
        
              </div>
          </div>
        </div>
    )
  }

  BestPlaceToBuild = () => {
    return (
      <div>
        <div>Java Island <button onClick={(e) => {this.props.updateSetting('island',0);this.fitBounds(JAWA)}}> v </button> </div>
        <div>Sumatera Island<button onClick={(e) => {this.props.updateSetting('island',1);this.fitBounds(SUMATERA)}}> v</button></div>
        <div>Borneo Island<button onClick={(e) => {this.props.updateSetting('island',2);this.fitBounds(KALIMANTAN)}}> v</button></div>
        <div>Sulawesi Island<button onClick={(e) => {this.props.updateSetting('island',3);this.fitBounds(SULAWESI)}}> v</button></div>
        <div>Bali and Nusa Tenggara Island<button onClick={(e) => {this.props.updateSetting('island',4);this.fitBounds(BALI_NT)}}> v</button></div>
        <div>Maluku Island<button onClick={(e) => {this.props.updateSetting('island',5);this.fitBounds(MALUKU)}}> v</button></div>
        <div>Papua Island<button onClick={(e) => {this.props.updateSetting('island',6);this.fitBounds(PAPUA)}}> v</button></div>

      </div>
    )
  }

  setElementVisibility(){
    const {index} =  this.state;
    var i = 0;
    var arr = [];
    for(i = 0;i < elementCount[index - 1];i++){
      arr.push(false);
    }

    this.setState({arrVisibility : arr});
  }


  content = [
    {
      logo : '',
      title : 'JAKARTA\nPast Capital City',
      img : img1,
      desc :'' ,
      content : '',
      point : JAKARTA
    },
    {
      logo : '',
      title : 'JAKARTA\nTraction Site',
      img : img2,
      desc : '',
      content : '',
      point : JAKARTA
    },
    {
      logo : icon1,
      title : 'Factor for moving to new Capital City of Indonesia',
      img : '',
      desc : '',
      content : ''
      ,
      point : JAKARTA
    },
    {
      logo : icon2,
      title : "Let's see where is the best place to build a New Capital City!",
      img : '',
      desc : '',
      content : '',
      point : INDONESIA
    },
    {
      logo : icon3,
      title : 'Then where is the best location to be exact of Indonesia New Capital City ?',
      img : '',
      desc : '',
      content : '',
      point : INDONESIA
    },
    {
      logo : icon4,
      title : 'Yes, Now you already know where the best place to build a New Capital City!\nBorneo Island',
      img : '',
      desc : '',
      content :'' ,
      point : KALIMANTAN
    },
    {
      logo : icon5,
      title : 'See what you can get on Borneo Island!',
      img : '',
      desc : '',
      content : '',
      point : KALIMANTAN
    },
    {
      logo : icon6,
      title : "Let's fly abroad to Borneo Island!",
      img : '',
      desc : 'There are 2 ways to reach Borneo Island, there are by air transportation or water transportation. A longway river on Borneo Island make it very unique to down stream across the river by cruise.',
      content :
        <div>
          <div>
            <div  className={'content-title'}>
                Airport
              </div>
              <div>
              There are 4 airports around Island of Borneo
              </div>
              <div className={'content-standard'}>
                <div>International Airport Supadio <Checkbox style={{textAlign : 'right'}} /></div>
                <div>International Airport Sultan Aji <Checkbox style={{textAlign : 'right'}} /></div>
                <div>International Airport Syamsudin Noor <Checkbox style={{textAlign : 'right'}} /></div>
                <div>International Airport Juwata <Checkbox style={{textAlign : 'right'}} /></div>
              </div>

          </div>
          <div>
            <div  className={'content-title'}>
              Harbour
            </div>
            <div>
            There are 8 harbours around Island of Borneo
            </div>
            <div className={'content-standard'}>
              <div>Batu Licin Port <Checkbox style={{textAlign : 'right'}} /></div>
              <div>Dwikora Port <Checkbox style={{textAlign : 'right'}} /></div>
              <div>Palangkaraya Port <Checkbox style={{textAlign : 'right'}} /></div>
              <div>Semayang Port <Checkbox style={{textAlign : 'right'}} /></div>
              <div>Malundung Port <Checkbox style={{textAlign : 'right'}} /></div>
              <div>Trisakti Port <Checkbox style={{textAlign : 'right'}} /></div>
              <div>Samudera Port <Checkbox style={{textAlign : 'right'}} /></div>
              <div>TPK Palaran Port <Checkbox style={{textAlign : 'right'}} /></div>
            </div>
          </div>
        </div>
      ,
      point : KALIMANTAN
    },
    {
      logo : '',
      title : "Finally! \nBorneo island is place for the new Capital City, let's build some.",
      img : '',
      desc : '',
      content :'' ,
      point : KALIMANTAN
    }
  ]

  componentDidMount(){
    var index = 0;
    var content = this.content;
    if(this.props.index > 5){
      this.setState({index : 4});
      index = 4;    
    }

    this.fitBounds(content[index].point);
    this.setElementVisibility();
  }

  fitBounds(l){
    requestJson(l, (error, response) => {
      if (!error) {
        this.props.fitBounds(response);
      }
    });
    
  }
  IndexNavigator = () => {
    const {index} = this.state;
    return <div className={'index-navigator'}>
      {index >= 9 ?<button style={{marginBottom : 5}} className={'landing-page-button'} onClick={this.nextIndex}>Finish</button> : ''}
      <br/>
      <button className={'button-index-prev'}onClick={this.prevIndex}>  <Icon color='white' name='angle left' /></button>
      {/* <Button circular icon='angle left' className="button-index" onClick={this.prevIndex}/> */}
      <span style={{margin : 5}}>{index}</span> 
      <button className={'button-index-next'} style={{opacity : index < 9 ? '1' : '0'}} onClick={this.nextIndex}>  <Icon color='white' name='angle right' /> </button>

      {/* <Button circular icon='angle right' className="button-index" onClick={this.nextIndex} style={{opacity : index < 9 ? '1' : '0'}}/> */}
    </div>
  };
  nextIndex(){
    var val = this.props.index + 1;
    var content = this.content;

    this.props.updateSetting('index',val > 11 ? 0: val);
    this.fitBounds(content[val > 11 ? 0 : this.state.index].point);
    this.setState({index : this.state.index + 1});

  }

  prevIndex(){
    var val = this.props.index - 1;
    var content = this.content;
    this.props.updateSetting('index',val);
    this.fitBounds(content[this.state.index - 2].point);

    this.setState({index : this.state.index  -1});

  }

  render() {
    const Container = this.props.containerComponent || defaultContainer;
    const {index} = this.state;
    console.log(index);
    var content = this.content[index-1];
      return (
        <Container>
          <div>
            <div>
              <img src={content.logo} alt="" width={75}  height={70}/>
            </div>
            <div  className={'content-title'}>
              {
                content.title
              }
            </div>
            <div className={'content-image'}>
              <img src={content.img} alt="" width="103%"  height={225}/>
            </div>
            <div>
              {content.desc}
            </div>
            <div>
            {
              index === 3 ? this.FactorMoveView() :  index === 4 ? this.BestPlaceToBuild() : ''   
            }
            </div>        
          </div>
          <this.IndexNavigator 
          index={this.props.index}
          />
        </Container>
      ) 
   }
} 
