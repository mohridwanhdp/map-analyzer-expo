import React from 'react'
import image1 from './image/landingpage1.png'
import image2 from './image/landingpage2.png'
import image3 from './image/landingpage3.png'
import logo from './image/logo geo.png'
import worldMap from './image/world map.png'

import {Input,Modal,Dropdown} from 'semantic-ui-react'

const countryOptions = [
    { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
    { key: 'ax', value: 'ax', flag: 'ax', text: 'Aland Islands' },
    { key: 'al', value: 'al', flag: 'al', text: 'Albania' },
    { key: 'dz', value: 'dz', flag: 'dz', text: 'Algeria' },
    { key: 'as', value: 'as', flag: 'as', text: 'American Samoa' },
    { key: 'ad', value: 'ad', flag: 'ad', text: 'Andorra' },
    { key: 'ao', value: 'ao', flag: 'ao', text: 'Angola' },
    { key: 'ai', value: 'ai', flag: 'ai', text: 'Anguilla' },
    { key: 'ag', value: 'ag', flag: 'ag', text: 'Antigua' },
    { key: 'ar', value: 'ar', flag: 'ar', text: 'Argentina' },
    { key: 'am', value: 'am', flag: 'am', text: 'Armenia' },
    { key: 'aw', value: 'aw', flag: 'aw', text: 'Aruba' },
    { key: 'au', value: 'au', flag: 'au', text: 'Australia' },
    { key: 'at', value: 'at', flag: 'at', text: 'Austria' },
    { key: 'az', value: 'az', flag: 'az', text: 'Azerbaijan' },
    { key: 'bs', value: 'bs', flag: 'bs', text: 'Bahamas' },
    { key: 'bh', value: 'bh', flag: 'bh', text: 'Bahrain' },
    { key: 'bd', value: 'bd', flag: 'bd', text: 'Bangladesh' },
    { key: 'bb', value: 'bb', flag: 'bb', text: 'Barbados' },
    { key: 'by', value: 'by', flag: 'by', text: 'Belarus' },
    { key: 'be', value: 'be', flag: 'be', text: 'Belgium' },
    { key: 'bz', value: 'bz', flag: 'bz', text: 'Belize' },
    { key: 'bj', value: 'bj', flag: 'bj', text: 'Benin' },
  ]
export class LandingPage1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen : false,
            selectedCountry : null
        }
        this.nextPage = this.nextPage.bind(this);
        this.showModal = this.showModal.bind(this);
        this.changeCountry = this.changeCountry.bind(this);
    }
    changeCountry(val){
        this.setState({selectedCountry : val});
    }
    nextPage(){
        // POST negara 
        this.props.nextPage('index',1);
    }
    showModal(){
        this.setState({isOpen : true});
    }
    render(){
        const {isOpen,selectedCountry,email} = this.state;
        return(
            <div className={'full-screen'}>
               <Modal size='medium'
               basic
                open={isOpen}

            >
                    <Modal.Content 
                   
                >
                        <center>
                           <div className={'landing-page-modal'}>
                             <img src={worldMap} alt="" width={300}  height={150}/>                                
                               <br/>
                                First of all,<br/>
                                Please select your origin country
                                <br/>
                                <Input
                                    placeholder = 'Input your e-mail'
                                    onChange={(e,value) => this.setState({email : value})}
                                />
                                <br/>
                                <Dropdown
                                    placeholder='Select Country'
                                    search
                                    selection
                                    options={countryOptions}
                                    onChange={(e,{value})  => {this.changeCountry(value)}}
                                />
                                <br/>
                                <button className={'landing-page-button'}  disabled={(selectedCountry && email) ? false : true} onClick={this.nextPage}>Continue</button>
                           </div>
                        </center>
                    </Modal.Content>    
                </Modal>
                <div className={'full-screen landing-page'}>               
                    <img src={image1} alt="" width={window.outerWidth}  height='100%'/>
                </div>
                <div className='full-screen landing-page landing-page-bg'>
                    <div className={'landing-page-content'}>
                            <span>
                    <img src={logo} alt="" width={125}  height={95}/>                                
                            </span>
                            <br/>

                            <span>
                            <b>Hi Hannover Messe 2020  </b>
                            <br/>
                            Welcome to GEO MAPID. It's a Web-Based map which is using a Cloud-Computing, so you can work from anywhere, any-devices and anytime.
                            </span>
                            <br/>
                            <span>
                                <button white className={'landing-page-button'}  onClick={this.showModal}>
                                    <b>Let's start mapping</b>
                                </button>
                            </span>
                    </div>
                </div>
            </div>
        )
    }
}

export class LandingPage2 extends React.Component {
    constructor(props) {
        super(props);
        this.nextPage = this.nextPage.bind(this);

    }
    
    nextPage(){
        this.props.nextPage('index',2);
    }
    render(){
        return(
            <div className={'full-screen'}>
                <div className={'full-screen landing-page'}>               
                    <img src={image2} alt="" width={window.outerWidth}  height='100%'/>
                </div>
                <div className='full-screen landing-page landing-page-bg'>
                    <div className={'landing-page-content'}>
                            <span>
                                
                            </span>
                            <span>
                                <b>Feel Crowd Enough of Jakarta as a Capital City</b>
                                <br/>
                                Indonesian Government finally declare that will move the Capital City From Jakarta to somewhere in Indonesian Island. Let's start the journey!
                            </span>
                            <br/>
                            <span>
                                <button className={'landing-page-button'} onClick={this.nextPage}>
                                    Next
                                </button>
                            </span>
                    </div>
                </div>
            </div>
        )
    }
}

export class LandingPage3 extends React.Component {
    constructor(props) {
        super(props);
        this.nextPage = this.nextPage.bind(this);

    }
    
    nextPage(){
        this.props.nextPage('index',6);
    }
    render(){
        return(
            <div className={'full-screen'}>
                <div className={'full-screen landing-page'}>               
                    <img src={image3} alt="" width={window.outerWidth}  height='100%'/>
                </div>
                <div className='full-screen landing-page landing-page-bg'>
                    <div className={'landing-page-content'}>
                            <span>
                                
                            </span>
                            <span>
                                <b>It's has been a long-time process to repair Jakarta as Capital City </b>
                                <br/>
                                Indonesia has been spent a lot of time, money also effort to repair and build Jakarta as a Capital City properly but as it seen Jakarta has not good factors as a Capital City for the future, so government decide to move Indonesia Capital City to one of the island in Indonesia, let's have some research!
                            </span>
                            <br/>
                            <span>
                                <button className={'landing-page-button'} onClick={this.nextPage}>
                                    Next
                                </button>
                            </span>
                    </div>
                </div>
            </div>
        )
    }
}