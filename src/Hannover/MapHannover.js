import React, {createContext} from 'react';
import {DeckGL, ScatterplotLayer,GeoJsonLayer,ColumnLayer} from 'deck.gl';
import ReactMapGL, {FlyToInterpolator,ScaleControl,NavigationControl,StaticMap,_MapContext as MapContext	} from 'react-map-gl';
import {LandingPage1,LandingPage2,LandingPage3} from './LandingPage.js'
import mapboxgl from 'mapbox-gl'
import {defaultMapStyle, darkMapStyle, lightMapStyle, satelliteMapStyle} from '../style/map-style.js';
import {json as requestJson} from 'd3-request';
import ControlPanel from './ControlPanel.js'
import WebMercatorViewport from 'viewport-mercator-project';
import bbox from '@turf/bbox';
import axios from 'axios';
import { Button,Icon,Tab } from 'semantic-ui-react';
import ReactGA from 'react-ga'
import { Editor, EditorModes } from 'react-map-gl-draw';
import { EditableGeoJsonLayer, DrawPolygonMode } from 'nebula.gl';

//Data import
import DEMOGRAFI_JAKARTA from './data/DemografiJakarta.geojson'

//Ruang Terbuka
import RUANG_TERBUKA_JAKARTA_2015 from './data/Ruang Terbuka Hijau 2015.geojson'
import RUANG_TERBUKA_JAKARTA_2016 from './data/Ruang Terbuka Hijau 2016.geojson'
import RUANG_TERBUKA_JAKARTA_2017 from './data/Ruang Terbuka Hijau 2017.geojson'
import RUANG_TERBUKA_JAKARTA_2018 from './data/Ruang Terbuka Hijau 2018.geojson'
import RUANG_TERBUKA_JAKARTA_2019 from './data/Ruang Terbuka Hijau 2019.geojson'

//Kawasan Hutan
import HUTAN_JAWA from './data/Hutan/Jawa.geojson'
import HUTAN_KALIMATAN from './data/Hutan/Kalimantan.geojson'
import HUTAN_BALI_NT from './data/Hutan/Bali_dan_Nusa_Tenggara.geojson'
import HUTAN_MALUKU from './data/Hutan/Maluku.geojson'
import HUTAN_PAPUA from './data/Hutan/Papua.geojson'
import HUTAN_SUMATERA from './data/Hutan/Sumatera.geojson'
import HUTAN_SULAWESI from './data/Hutan/Sulawesi.geojson'


import "../analyzer/App.css";
const MAPBOX_TOKEN = 'pk.eyJ1IjoibW9ocmlkd2FuaGRwIiwiYSI6ImNrMDRvaGwzdDA0dDkzaG9tNnA3dmViODQifQ.qa1W5PaDaHzLnqIXbAM7zw';
const LayerURL = 'https://hnjp62bwxh.execute-api.us-west-2.amazonaws.com/GeoDev/getprojectbyid?_id='
const Rainbow = require('rainbowvis.js');
const convert = require('color-convert');
var groupColor = new Rainbow();
const refMap =  React.createRef();
const refDeck = React.createRef();

var geojsonArray = [
  [],
  [],
  [],
  [],
  [
    {geojson : [RUANG_TERBUKA_JAKARTA_2015,RUANG_TERBUKA_JAKARTA_2016,RUANG_TERBUKA_JAKARTA_2017,RUANG_TERBUKA_JAKARTA_2018,RUANG_TERBUKA_JAKARTA_2019],type : 'fill',column : 'LUAS_M2',visibilty : false,useHeight : false},
    {geojson : [DEMOGRAFI_JAKARTA],type : 'fill',column : 'KPDTN_',visibility : false,useHeight : true},
  ],
  [],
  [
    {geojson : [HUTAN_JAWA],type : 'fill',column : 'NAMA_ZONA',visibility : [false],useHeight : false},
    {geojson : [HUTAN_SUMATERA],type : 'fill',column : 'NAMA_ZONA',visibility : [false],useHeight : false},
    {geojson : [HUTAN_KALIMATAN],type : 'fill',column : 'NAMA_ZONA',visibility : [false],useHeight : false},
    {geojson : [HUTAN_SULAWESI],type : 'fill',column : 'NAMA_ZONA',visibility : [false],useHeight : false},
    {geojson : [HUTAN_BALI_NT],type : 'fill',column : 'NAMA_ZONA',visibility : [false],useHeight : false},
    {geojson : [HUTAN_MALUKU],type : 'fill',column : 'NAMA_ZONA',visibility : [false],useHeight : false},
    {geojson : [HUTAN_PAPUA],type : 'fill',column : 'NAMA_ZONA',visibility : [false],useHeight : false},
  ],
  [],
  [],
  [],
  [],
  []
]
const myFeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              106.73046112060547,
              -6.118707747190845
            ],
            [
              106.73286437988281,
              -6.185270134775632
            ],
            [
              106.7654800415039,
              -6.252847984958681
            ],
            [
              106.78298950195312,
              -6.298918791076363
            ],
            [
              106.88529968261719,
              -6.301648784883256
            ],
            [
              106.95945739746094,
              -6.300966287777954
            ],
            [
              106.94435119628906,
              -6.19721634559124
            ],
            [
              106.9244384765625,
              -6.128265935188198
            ],
            [
              106.90589904785156,
              -6.105735647265721
            ],
            [
              106.79397583007812,
              -6.092763233165217
            ],
            [
              106.73046112060547,
              -6.118707747190845
            ]
          ]
        ]
      }
    }
  ]
};
const selectedFeatureIndexes = [];

const MODES = [
  { id: EditorModes.EDITING, text: 'Select and Edit Feature'},
  { id: EditorModes.DRAW_POINT, text: 'Draw Point'},
  { id: EditorModes.DRAW_PATH, text: 'Draw Polyline'},
  { id: EditorModes.DRAW_POLYGON, text: 'Draw Polygon'},
  { id: EditorModes.DRAW_RECTANGLE, text: 'Draw Rectangle'}
];

ReactGA.initialize('UA-157598276-1', {
  debug: false,
  titleCase: false
});
ReactGA.pageview('/analyzer');

export default class MapHannover extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    mapStyle: defaultMapStyle,
    viewState : {
          width : window.innerWidth,
          height : window.innerHeight,
          latitude: -6.175110,
          longitude:  106.865036,
          zoom: 12,
          bearing : 0,
          pitch : 0,
          transitionDuration : 1000
        },
      popupInfo : null,
      layers3D : [],
      hoveredObject : null,
      pointerX : 0,
      pointerY : 0,
      dataLayers : null,
      view3D : true,
      columnPosition : null,
      scaleCount : 6,
      legendColor : null,
      isPoint : false,
      radius : 200,
      layerSettings : {
        smallestName : 'Fewer',
        largestName : 'More',
        smallestColor : '#8B0000',
        largestColor : '#006400'
      },
      showPopup : true,
      inspectMode : false,
      max : 1,
      min : 0,
      width : window.innerWidth,
      height : window.innerHeight,
      navigatorIndex : 0,
      selectedMode: EditorModes.DRAW_POINT,
      data : myFeatureCollection,
      year : 2015,
      islandValue : null
  }
    this._onViewStateChange = this._onViewStateChange.bind(this);
    this._loadData = this._loadData.bind(this); 
    this.changeLayerVisibility = this.changeLayerVisibility.bind(this);
    this.changeLayerInfoVisibility = this.changeLayerInfoVisibility.bind(this);
    this._onViewStateChange = this._onViewStateChange.bind(this);
    this._refreshMap = this._refreshMap.bind(this);
    this.layerBoxRef = React.createRef();
    this._fitBounds = this._fitBounds.bind(this);
    this._renderTooltip = this._renderTooltip.bind(this);
    this.filter = this.filter.bind(this);
    this.setReportColumn = this.setReportColumn.bind(this);
    this.setReportFeatures = this.setReportFeatures.bind(this);
    this.setMapScale = this.setMapScale.bind(this);
    this._onMapLoaded = this._onMapLoaded.bind(this);
    this._updateSettings = this._updateSettings.bind(this);
    groupColor.setSpectrum('darkred','red','orange','yellow','green','blue','darkblue','violet','purple');

  }

  componentDidMount(){
    
    //Resize window
    window.addEventListener('resize', this._resize.bind(this));
    this._resize();
  }
  
  _resize() {
    this._onViewStateChange({
      width: window.outerWidth,
      height: window.outerHeight
      // width : "100%",
      // height : "100%"
    });
    this.setState({
      width : window.innerWidth,
      height : window.innerHeight
    }); 
    if(this.reactMap){
      const map = this.reactMap.getMap();
    
      map.resize();
    }
  }
  
  
  _loadData = dataRes => {   
    const dataLayers = dataRes.layers;
    
    var myRainbow = new Rainbow();
    var MAX = 6;

    myRainbow.setSpectrum('#8B0000','yellow','#006400');      
    

    myRainbow.setNumberRange(0,MAX - 1);
    var legendItem = []
    var j;
    for(j = 0 ; j < MAX ; j++){
      legendItem.push(convert.hex.rgb(myRainbow.colourAt(j)));       
     }    
    
     const arrLayerInfo = dataLayers.map((l,index) => {
       
       return {
         "id" : l._id,    
         "name" : l.name,     
         "arrChecked" : ['mapid_id'],
         "visibility" : false,
         "selectedColumn" : 'mapid_id',
         "selectedMode" : 'height',
         "radius" : 200,
         "view2D" : false,
         "classNumber" : isNaN(l.legend) || l.legend === null ? 6 : l.legend.class_number,
         "filterParam" : null
       };
     });
    this.setState({legendColor : myRainbow});
    this.setState({dataLayers : dataLayers});
    this.setState({arrLayerInfo : arrLayerInfo});
    this.setState({projectName : dataRes.name});
  }

  _switchMode = evt => {
    const selectedMode = evt.target.id;
    this.setState({
     selectedMode: selectedMode === this.state.selectedMode ? null : selectedMode
    });
  };
 
  _renderToolbar = () => {
    return (
      <div style={{position: 'absolute', top: 0, right: 200, maxWidth: '320px',zIndex : 22}}>
        <select onChange={this._switchMode}>
          <option value="">--Please choose a mode--</option>
          {MODES.map(mode => <option value={mode.id}>{mode.text}</option>)}
        </select>
      </div>
    );
  };

  _onMapLoaded(){

    var map = this.reactMap.getMap();
    document
      .getElementById("deckgl-wrapper")
      .addEventListener("contextmenu", evt => evt.preventDefault());  
  }

  setMapScale(){
    var sc = document.getElementsByClassName("mapboxgl-ctrl-scale");
  if(sc[0]){
    sc[0].style.position = "absolute";
    sc[0].style.bottom = 0;
    sc[0].style.zIndex = 15;

    this.setState({mapScale : sc[0].innerHTML});
  }

  }

  getFilteredFeatures(ft,op,col,pr){
    if(op==='>='){
      ft = ft.filter((f) => {if(f.properties[col] >= pr){return f}});      
    }
    else if(op==='<='){
      ft = ft.filter((f) => {if(f.properties[col] <= pr){return f}});      
    }
    else if(op==='!='){
      ft = ft.filter((f) => {if(String(f.properties[col]) != pr){return f}});      
    }
    else if(op==='>'){
      ft = ft.filter((f) => {if(f.properties[col] > pr){return f}});      

    }
    else if(op==='<'){
      ft = ft.filter((f) => {if(f.properties[col] < pr){return f}});      

    }
    else if(op==='='){
      ft = ft.filter((f) => {if(String(f.properties[col]) === pr){return f}});           
    }
    else if(op==='has'){
      ft = ft.filter((f) => {
        var str = String(f.properties[col]); 
        var bool =  str.startsWith(pr);
        if(bool){return f}});            
    }

    return ft;
  }
  
  async _render3DLayers(navIndex){
    var geoArr = geojsonArray[navIndex];
    const {year,islandValue} = this.state;
    var arr = [];
    var col;
    if(navIndex === 6){
      geoArr = [geoArr[islandValue]]
    }
    if(geoArr.length > 0){
         await geoArr.map(async(g,index) =>{
          
          if(islandValue === null && navIndex === 6){
            return [];
          }
          col = g.column;
          var gjson = g.geojson;
          var gjsonParsed;
          
          if(navIndex === 4){
            if(index === 1){
              col += year;
            }
            else if(index === 0) {
              gjson = [gjson[year-2015]];
            }
            
          }        
          
          // await requestJson(gjson, (error, response) => {
          //   if (!error) {
          //    gjsonParsed = response;
          //   }
          // });
          
          arr = await gjson.map(async(gj) => {
            await axios.get(gj).then((response) => 
              {
                gjsonParsed = response.data;
              }
            );

            gjsonParsed.features = gjsonParsed.features.filter((f) => (f.geometry !== null));
            console.log(gjsonParsed);
            if(g.type === 'fill'){
              return new GeoJsonLayer({
                id: col + index,
                data : gjsonParsed,
                opacity: 1,
                stroked : false,
                filled: true,
                fp64 : true,
                wireframe: true,
                extruded : true,
                elevationScale : g.useHeight ? 1.25 : 1,
                getElevation: d => g.useHeight? d.properties[col] * 1: 50,
                getFillColor: [255,0,0],
                getLineColor: [255,0,0],
                getLineWidth : 1,
                pickable: true,
                updateTriggers: {
                  getPosition: [this.state.year],              
                  getElevation: [this.state.year],
                  getFillColor: [this.state.year],
                  getLineColor: [this.state.year]
                  
                },
                transitions : {
                  getElevation : 1000
                }
                // onHover: (info) => { this.setState({
                //   hoveredObject: info.object,
                //   pointerX: info.x,
                //   pointerY: info.y
                // }); }
              });
            }
          })
        
        }        
      );
    }

    const arr3D = await Promise.all(arr);
    console.log(arr3D);
    
    this.setState({layers3D : arr3D});
  }

  _renderTooltip() {
    const {pointerX, pointerY, hoveredObject} = this.state;
    // const props = geojson.features[0].properties;                        
    // const col = Object.keys(props);  
    return (
      hoveredObject && (
        <div  style={{top: pointerY, left: pointerX, zIndex : 13,position: 'absolute',
          margin: 8,
          padding: 4,
          background: 'rgba(0, 0, 0, 0.8)',
          color: '#fff',
          maxWidth: 300,
          fontSize: 10,
          pointerEvents: 'none'}}>
            <div>
            {Object.keys(hoveredObject['properties'])
            .filter((col) => col.startsWith("image"))
            .map(function(key) {       
              return ( 
                <div key={key} >
                    {hoveredObject['properties'][key] === '-' ? '' : <div> <img src={hoveredObject['properties'][key]} width="100" height="125"/></div>}            
                </div>
                )
            })}
            {Object.keys(hoveredObject['properties'])
            .filter((col) => !col.startsWith("image"))
            .map(function(key) {       
              return ( 
                <div key={key}>
                    <div> {key} : {hoveredObject['properties'][key]} <br /></div>            
                </div>
                )
            })}
            </div>
        </div>
      )
    );
  }

  _onViewStateChange({viewState}) {

    this.setState({
      viewState: {...this.state.viewState, ...viewState}
    });
    this.setMapScale();
    if(this.refMap){
      const map = this.refMap.getMap();
      console.log();
    }
  }

  changeLayerVisibility(layer){
    var arrLayerInfo = this.state.arrLayerInfo;

    var index = arrLayerInfo.map(function(x) {return x.id; }).indexOf(layer);    
    arrLayerInfo[index]['visibility'] = !arrLayerInfo[index]['visibility']; 
    this.setState({arrLayerInfo : arrLayerInfo});
    
    this._render3DLayers(this.state.activeLayer);
  }

  //Method for layer info
  changeLayerInfoVisibility(layer){
    const vis = this.state.layerInfoVisibility;
    this.setState({activeLayer : layer}); 
    this.forceUpdate();
    const {arrLayerInfo} = this.state;  


    this.setState({layerInfoVisibility : !vis});
    
    var lyr = this.state.dataLayers.filter(f => f._id === layer)
    
    this.setState({layerInfo : lyr});  
    var myRainbow = new Rainbow();
    if(lyr[0]['legend']){
      var sml = lyr[0]['legend']['smallest_color'];
      var lrg = lyr[0]['legend']['largest_color'];
      if(sml ==='#00'){
        lyr[0]['legend']['smallest_color']='#00FF00';
      }

      if( lrg ==='#00'){
        lyr[0]['legend']['largest_color']='#00FF00';
      }
      if(sml === '#8b0000' || sml === '#006400'){
        myRainbow.setSpectrum(lyr[0]['legend']['smallest_color'],'yellow',lyr[0]['legend']['largest_color']);          
      }
      else if(sml === '#FF0000' && lrg === '#00FF00'){
        myRainbow.setSpectrum('#8b0000','yellow','#006400');                          
      }
      else if(sml === '#00FF00' && lrg === '#FF0000'){
        myRainbow.setSpectrum('#006400','yellow','#8b0000');                                            

      }
      else {
        myRainbow.setSpectrum(lyr[0]['legend']['smallest_color'],lyr[0]['legend']['largest_color']);          
      }
    }else{  
      myRainbow.setSpectrum('#8B0000','yellow','#006400');    
    }

    var index = arrLayerInfo.map(function(x) {return x.id; }).indexOf(layer);
    
    if(lyr[0].layer_type === 'symbol'){
        this.setState({isPoint : true})
    }else{
      this.setState({isPoint : false})
    }

    myRainbow.setNumberRange(0,arrLayerInfo[index].classNumber - 1);
    this.setState({year : 0});  
    this.setState({visualParams : arrLayerInfo[index].arrChecked});  
    this.setState({legendColor : myRainbow});


    this._render3DLayers(layer);
  }
  

  changeMapStyle = (e) => {

    if(e === 'dark'){
      this.setState({mapStyle : darkMapStyle});
    }
    else if(e === 'light'){
      this.setState({mapStyle : lightMapStyle});
    }else if(e === 'basic'){
      this.setState({mapStyle : defaultMapStyle});
    }
    else{
      this.setState({mapStyle : satelliteMapStyle});      
    }
  }

   async _updateSettings(name, value){
    if(name === 'index'){
      this.setState({navigatorIndex : value});
      this._render3DLayers(value);
    }
    else if(name === 'year'){
    const {navigatorIndex} = this.state;

      await this.setState({year : value});
      this._render3DLayers(navigatorIndex);
    }
    else if(name === 'island'){
      const {navigatorIndex} = this.state;
  
        await this.setState({islandValue : value});
        this._render3DLayers(navigatorIndex);
      }
  };

  _refreshMap(){
    this.setState({layers3D: []});
    const {navigatorIndex} = this.state;
    setTimeout(() => {
      this._render3DLayers(navigatorIndex);
    },5
     );
  }

  _fitBounds = (l) => {
    
    this.setState({layers3D : []});
    // this.setState({view3D : false});
    
    const {viewState,navigatorIndex} = this.state;
    const [minLng, minLat, maxLng, maxLat] = bbox(l);
    
    // construct a viewport instance from the current state
    const viewport = new WebMercatorViewport(viewState);
    const {longitude, latitude, zoom} = viewport.fitBounds([[minLng, minLat], [maxLng, maxLat]], {
      padding : { top: 50 , right: 250 , bottom: 50, left: 0 }
    });
    
 
    this._onViewStateChange({
      viewState: {
        ...this.state.viewState,
        longitude,
        latitude,
        zoom,
        transitionInterpolator: new FlyToInterpolator(),
        transitionDuration: 2000
      }
    });
    this._render3DLayers(navigatorIndex);
  }

  _onViewStateChangeDeck({viewState}) {
    this.setState({
      viewStateDeck: {...this.state.viewStateDeck, ...viewState}
    });
  }
  
  filter(column,param){
    const {activeLayer} = this.state;
    var arrLayerInfo = this.state.arrLayerInfo;
    /**Delete from arrLayerInfo */
    var arrInfo = arrLayerInfo.filter(f => f.id === activeLayer);
    var op;
    var prm;
    var index = arrLayerInfo.map(function(x) {return x.id; }).indexOf(activeLayer);
    
    if(param === null || param.length == 0){
      op = null;
    }
    else if(param.startsWith('>=')){
      op = '>=';
    }
    else if(param.startsWith('<=')){
      op = '<=';
    }
    else if(param.startsWith('!=')){
      op = '!=';
    }
    else if(param.startsWith('>')){
      op = '>';
    }
    else if(param.startsWith('<')){
      op = '<';
    }
    else if(param.startsWith('=') || param.startsWith('==')){
      op = '=';      
    }
    else{
      op = 'has'
    }

    if(op && column){
      var prm = param.split(op);
      var paramSearch = {
        op : op,
        param : op === 'has' ? prm[0]: prm[1],
        col : column
      };
    }
    else{
      var paramSearch = null;
    }

    arrInfo[0].filterParam = paramSearch;
    arrLayerInfo[index] = arrInfo[0];
      
      this.setState({arrLayerInfo : arrLayerInfo});
    
    this._render3DLayers(activeLayer);
  }

  setReportFeatures(features){
    this.setState({reportFeatures : features});
  }
  setReportColumn(column){
    this.setState({reportColumn : column});

  }
  render() {
    
    const {viewState,layers3D,legendColor,dataLayers,layerInfo,layerInfoVisibility,view3D,activeLayer,inspectMode,mapStyle} = this.state;    
    const {scaleCount,isPoint,viewBuffer,visualParams,dataColumn,selectedMode,year,layerSettings,radius,height,width} = this.state;
    const {navigatorIndex,data} = this.state;
    // const layer = new EditableGeoJsonLayer({
    //   id: 'geojson-layer',
    //   data: data,
    //   mode: DrawPolygonMode,
    //   selectedFeatureIndexes,
 
    //   onEdit: ({ updatedData }) => {
    //     this.setState({
    //       data: updatedData,
    //     });
    //   }
    // });
    switch(navigatorIndex){
      case 0 : 
        return (
          <LandingPage1 nextPage={this._updateSettings}/>
        )
        break;
      case 1 : 
        return (
          <LandingPage2 nextPage={this._updateSettings}/>
        )
      break;
      case 5 : 
      return (
        <LandingPage3 nextPage={this._updateSettings}/>
      )
      break;
      default :   
        return (
          <div style={{height:height, width:width}}>
            <div style={{position: 'absolute', top: 10, left: 10,zIndex : 14}}>
              <a href="https://geo.mapid.io">
                <img src="https://geomapid2019.s3-us-west-2.amazonaws.com/logo.svg" width="35" height="35" alt="Go to dashboard"/>  
              </a>
            </div>

            <div className="btn-map-style">
            <Button.Group>
            <Button icon onClick={() => this._refreshMap()}>
              <Icon name='refresh' />
            </Button>
                <Button value='basic' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Street</Button>
                    <Button value='light' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Light</Button>
                    <Button value='dark' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Dark</Button>      
                <Button value='satellite' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Satellite</Button>   
          </Button.Group> 
            </div>
          
              <ControlPanel
                updateSetting = {this._updateSettings}
                index={navigatorIndex}
                fitBounds = {this._fitBounds}
              /> 
    
          <div style={{height: height, width :width}} id="map-render">
    
          
        <DeckGL 
        initialViewState={viewState}
        controller={true}
        layers={layers3D}
        viewState = {viewState}
        onViewStateChange={(viewState) => this._onViewStateChange(viewState)}
        ContextProvider={MapContext.Provider}

        glOptions={{preserveDrawingBuffer: true}}
        >
          
    <ReactMapGL
      mapStyle={mapStyle}
      mapboxApiAccessToken={MAPBOX_TOKEN}
      preserveDrawingBuffer={true}
      ref={(reactMap) => this.reactMap = reactMap}
      refMap={refMap}
      onLoad={() => {this._onMapLoaded()}}
    >

  </ReactMapGL>
          
      { inspectMode && this._renderTooltip() }
    
    </DeckGL>
      
        
        
        </div>
    </div>
        );
        break;
    }
      
    }
}  

