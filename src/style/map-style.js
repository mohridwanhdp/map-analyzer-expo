import {fromJS} from 'immutable';
import MAP_STYLE from './map-style-basic-v8.json';
import DARK_MAP_STYLE from './map-style-dark.json';
import LIGHT_MAP_STYLE from './map-style-light.json';
import SATELLITE_MAP_STYLE from './map-style-satellite.json';

// For more information on data-driven styles, see https://www.mapbox.com/help/gl-dds-ref/
export const dataLayer = fromJS({
  id: 'data',
  source: 'dataLayers',
  type: 'fill',
  interactive: true,
  paint: {
    'fill-color': '#008',
    'fill-opacity': 0.8
  }
});

export const defaultMapStyle = fromJS(MAP_STYLE);
export const darkMapStyle = fromJS(DARK_MAP_STYLE);
export const lightMapStyle = fromJS(LIGHT_MAP_STYLE);
export const satelliteMapStyle = fromJS(SATELLITE_MAP_STYLE);