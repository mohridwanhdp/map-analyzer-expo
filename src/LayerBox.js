import React, { PureComponent } from 'react';
import LayerBoxItem from './LayerBoxItem.js'
import {Container,CardGroup,Button, Header,Icon} from 'semantic-ui-react';

const mobileStyle = {
    
        position: 'absolute', 
        height: '100%',
        background: 'rgba(255, 255, 255, 0.5)',
        overflow: 'auto',
        padding: 5,
        display : 'inline-block'
        
}
function RenderLayerList(props){
    const {listLayer} = props.data;
    return (
        <CardGroup style={{maxWidth : '300px'}}>
         {   listLayer &&
            
             listLayer.dataLayers.map((layer,index)=>(
             <LayerBoxItem key={index} layer={layer} changeVisibility={props.changeVisibility} zoom={props.zoom} changeInfo={props.changeInfo} fitBounds={props.fitBounds}/>    
        )
        )
    }
        
    </CardGroup>
    )
}

function RenderToolbox(props){
    return (
        <Button.Group vertical labeled icon className={"toolbox-button-bar"}>
        <Button icon='area chart' content='Elevation'/>
        <Button icon='line graph' content='Distance' />
        <Button icon='shuffle' content='Unit Converter' />
        <Button icon='expand' content='Buffer Point' />
        <Button icon='content' content='Buffer Line' />
        <Button icon='list layout' content='Layer Interaction' />
        <Button icon='eraser' content='Clear' />
      </Button.Group>
    )
}

class LayerBox extends PureComponent {
    constructor(props){
        super(props);
        this.state = {
            mode: true,
            listLayer : this.props.data,
            activeItem : 'layer management',
            minimized : false,
            width : window.innerWidth,
            height : window.innerHeight
        };
        
        this.handleItemClick = this.handleItemClick.bind(this);
        this.minimizePanel = this.minimizePanel.bind(this);

    }
    handleItemClick = (e, { name }) => this.setState({ activeItem: name })
    minimizePanel(){
        const {minimized} = this.state;
        this.setState({minimized : !minimized})
      }
    
    
    render() {
        const {listLayer,activeItem,minimized,width} = this.state;
        const isMobile = width <= 500;
    if(isMobile){
        return(
            <div>
           <Container style={mobileStyle}>            
            {   
                listLayer &&         
                    activeItem === 'layer management' ? <RenderLayerList zoom={this.props.zoom} view3D={this.props.view3D} data={{listLayer}} changeVisibility={this.props.changeVisibility} changeInfo={this.props.changeInfo} changeDimension={this.props.changeDimension} fitBounds={this.props.fitBounds}/> : <RenderToolbox/>
            }    
            
            
            </Container>
            </div>
        )
    }
    else{
        return (
            minimized &&  
      <Button style={{
        position: 'absolute',
        left: 5,
        // margin: 20,
        zIndex: 4,
        top : "10%"
      }} 
      color = 'blue'
      onClick={this.minimizePanel} icon >
      <Icon name='angle double right'/>
    </Button>
    
    || 
           <div>
           <Container className={"layers-box"} >
           <Header as='h3' dividing>
                Layer List
                <div className={"setting-button"}>
            <Button onClick={this.minimizePanel} icon  style={{position : 'absolute',top : 5,backgroundColor : 'rgba(255, 255, 255, 0.2)'}}>
            <Icon name='angle double left'/>
            </Button>        
            </div>
            </Header>
            
            {   
                listLayer &&         
                    activeItem === 'layer management' ? <RenderLayerList zoom={this.props.zoom} view3D={this.props.view3D} data={{listLayer}} changeVisibility={this.props.changeVisibility} changeInfo={this.props.changeInfo} changeDimension={this.props.changeDimension} fitBounds={this.props.fitBounds}/> : <RenderToolbox/>
            }    
            
            
            </Container>
            </div>
        );
    }
       
    }
}


LayerBox.propTypes = {

};
export default LayerBox;
