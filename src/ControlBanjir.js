import React, {PureComponent} from 'react';
import { Button,Icon,Checkbox, Divider} from 'semantic-ui-react';

const mobileStyle = {
  left : 0,
  bottom :0,
  height: "27.5vh",
  width : "100vw",
  position: 'absolute',
  overflow: 'auto',
  background: '#fff',
  padding: 10,
  fontSize: 13,
  color: '#6b6b76',
  outline: 'none',
  textTransform: 'uppercase',
  zIndex: 4
};

const settingsButtonStyle = {
  position: 'absolute',
  top : 0,
  right : 0,
  margin : [10,0,0,0],
  background : 'white'
}
const defaultContainer = ({children}) => <div className="control-panel" id="control-panel" style={{overflow : 'auto',maxWidth : 600,h4 : {fontSize : '1rem'}, h3 : {fontSize : '1.25rem'}}}>{children}</div>;
const mobileContainer = ({children}) => <div id="control-panel" style={mobileStyle}>{children}</div>;

const Rainbow = require('rainbowvis.js');
const groupColor = new Rainbow();
const arrColorDAS = [
  'cyan', //Anak sungai
  'darkblue',//sungai utama
  'yellow', // Danau
  'purple' // DAS cilicis

];
function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

function PengungsiLegend(props){
  const settings = props.settings;
  const legendItem = props.legendItem;
  
  var i = 0;
  var ele = [];
  var min = settings.minPengungsi;
  var max = settings.maxPengungsi;
  var temp = min;
  var increment = round((max - min) / settings.scaleCount, 0);

  if(min === max){
    i = max < 10 ? 0 : max <= 70 ? 1 : max <= 150 ? 2 : 3;
    ele.push(
    <Divider style={{wrapContent : true,minHeight : 10}}>
      <div style={{backgroundColor : '#' + legendItem[i],width : 30, height : 20,float : 'left',marginRight : 5}}/>
      <div style={{borderColor : 'white',padding : 2, textDecoration : 'none'}}> 
        {min}
      </div>
    </Divider>
    );
  }else{
    for(i = 0 ; i < settings.scaleCount;i ++){  
      ele.push(<Divider key={'pengungsi'+i} style={{wrapContent : true,minHeight : 10}}>
        <div style={{backgroundColor : '#' + legendItem[i],width : 30, height : 20,float : 'left',marginRight : 5}}/>
        <div style={{borderColor : 'white',padding : 2, textDecoration : 'none'}}> 
          {i === 0 ? min : round(temp + 1,0)} - { (temp+increment <= max - 1) ? round(temp+increment,0) : max}   
          
        </div>
        </Divider>);
    temp = temp + increment ;
    }
  }
  return <div id='legend-layout'> {ele} </div>;
}

function HeightLegend(props){
  const settings = props.settings;
  const legendItem = props.legendItem;
  
  var i = 0;
  var ele = [];
  var min = settings.layerSettings.smallestName;
  var max = settings.layerSettings.largestName;
  var temp = min;
  var increment = round((max - min) / settings.scaleCount, 2);

  if(min === max){
    var isDiffferent = min === 0 && max === 0;
    i = isDiffferent ? 3 : max < 10 ? 0 : max <= 70 ? 1 : max <= 150 ? 2 : 3;
    ele.push(
    <Divider style={{wrapContent : true,minHeight : 10}}>
      <div style={{backgroundColor : '#' + legendItem[i],width : 30, height : 20,float : 'left',marginRight : 5}}/>
      <div style={{borderColor : 'white',padding : 2, textDecoration : 'none',fontSize : isDiffferent ? 9.5: 12}}> 
        { isDiffferent  ? 'Titik Banjir berdasarkan laporan warga' : min}
      </div>
    </Divider>
    );
  }else{
    for(i = 0 ; i < settings.scaleCount;i ++){  
      ele.push(
      <Divider key={'height'+i} style={{wrapContent : true,minHeight : 10}}>
        <div style={{backgroundColor : '#' + legendItem[i],width : 30, height : 20,float : 'left',marginRight : 5}}/>
        <div style={{borderColor : 'white',padding : 2, textDecoration : 'none'}}> 
          {i === 0 ? min : round(temp + 0.01,2)} - { (temp+increment <= max - 0.01) ? round(temp+increment,2) : max}   
          
        </div>
        </Divider>);
    temp = temp + increment ;

    }
  }
  return <div id='legend-layout'> {ele} </div>;
}

export default class ControlBanjir extends PureComponent {
  constructor(props){
    super(props);
    this.state = {
      smallestName : 'Fewer',
      largestName : 'More',
      smallestColor : '#FF0000',
      largestColor : '#00FF00',
      colorPicked : false,
      openModal : false,
      selectedColor : "darkred,yellow,darkgreen",
      classNumber : 4,
      errorMessage : "The value of class number must between 2 and 15",
      errorStat : false,
      minimized : false
    };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.saveLayerSetting = this.saveLayerSetting.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeComplete = this.handleChangeComplete.bind(this);
    this.validationCheck = this.validationCheck.bind(this);
    this.minimizePanel = this.minimizePanel.bind(this);
    groupColor.setSpectrum('darkred','red','orange','yellow','green','blue','darkblue','violet','purple');
  }

  closeModal(){
    this.setState({openModal : false});
  }
  openModal(){
    this.setState({openModal : true});
  }

  saveLayerSetting = () => {
    const {selectedColor,classNumber} = this.state;
    var arrColor = selectedColor.split(',');
    var smallestColor = arrColor[0];
    var midColor = arrColor[1];
    var largestColor = arrColor[2];
    this.props.onChange('layerSettings',{smallestColor,midColor,largestColor,classNumber});

    this.closeModal();
  }

  validationCheck(){
    const {classNumber} = this.state;
    if(classNumber < 2 || classNumber > 15){
      this.setState({errorStat : true})
    }
    else{
      this.setState({errorStat : false})

      this.saveLayerSetting();

    }
  }
  handleChangeComplete(value){
      this.setState({ selectedColor: value });

  }
  handleChangeMode(val){
    // this.setState({selectedMode : val})
    this.props.onChange('mode',val);
  }
  handleChange(val){
    this.setState({[val.target.name] : val.target.value});
    // const {name,value} = val;
  }

  handleChangeClassNumber(val){
    this.setState({classNumber : val});
  }

  UNSAFE_componentWillReceiveProps(nextProps){
    if(nextProps.scaleCount !== this.state.classNumber){
      this.setState({classNumber : nextProps.scaleCount});
    }

  }
  
  minimizePanel(){
    const {minimized} = this.state;
    this.setState({minimized : !minimized})
  }
  changeViewState(name,val){
    if(name === 'polygon'){
      this.props.onChange('layerPolygon',!val);
    }
    else if(name === 'pengungsi'){
      this.props.onChange('viewPengungsi',!val);
    
    }
    else if(name === 'pintu'){
      this.props.onChange('viewPintu',!val);
    }
    else if(name === 'laporan'){
      this.props.onChange('viewLaporan',!val);    
    }
    else{
      this.props.onChange('viewDAS',!val);
    }
  }
  render() {
    const {settings} = this.props;
    const Container = settings.isMobile ? (this.props.containerComponent || mobileContainer) : (this.props.containerComponent || defaultContainer);
    
    const {minimized} = this.state;
    var viewBuffer = settings.viewBuffer;

    var i;
    var legendItem = [];
    
    for(i = 0 ; i < settings.scaleCount ; i++){
       legendItem.push(settings.legendColor.colourAt(i));        
      }


    var ele = [];
    for(i = 0;i<4;i++){
      ele.push(
        <Divider key={'ele'+i} style={{wrapContent : true,minHeight : 10}}>
          <div style={{backgroundColor : '#' + legendItem[i],width : 30, height : 20,float : 'left',marginRight : 5}}/>
          <div style={{borderColor : 'white',padding : 2, textDecoration : 'none', height : 'wrap-content'}}> 
            {i === 0 ? '< 10' : i === 1 ? '10 - 70' : i === 2 ? '71 - 150' : '> 150' }
          
          </div>
        </Divider>
      );      
    }  

    var elePintu = [];
    for(i = 0;i<4;i++){
      elePintu.push(
        <Divider key={'ele'+i} style={{wrapContent : true,minHeight : 10}}>
          <div style={{backgroundColor : '#' + legendItem[i],width : 30, height : 20,float : 'left',marginRight : 5}}/>
          <div style={{borderColor : 'white',padding : 2, textDecoration : 'none', height : 'wrap-content'}}> 
            {i === 0 ? 'Siaga 4 : < 10' : i === 1 ? 'Siaga 3 : 10 - 70' : i === 2 ? 'Siaga 2 : 71 - 150' : 'Siaga 1 : > 150' }
          
          </div>
        </Divider>
      );      
    }  

    var eleDAS = [];
    var strDAS = [
      'Anak Sungai Ciliwung - Cisadane',
      'Sungai Utama Ciliwung - Cisadane',
      'Danau',
      'Daerah Aliran Sungai (DAS) Ciliwung dan Cisadane'


    ];
    for(i = 0 ; i < 4 ; i++){
      eleDAS.push(
        <Divider key={'eleDAS'+i} style={{wrapContent : true,minHeight : 10}}>
          <div style={{backgroundColor : arrColorDAS[i],width : 30, height : 20,float : 'left',marginRight : 5}}/>
          <div style={{borderColor : 'white',padding : 2, textDecoration : 'none', height : 'wrap-content',fontSize : 11}}> 
            {strDAS[i]}
          </div>
        </Divider>
      )
    }

    return (
      minimized &&  
      <Button style={{
        position: 'absolute',
        right: "3%",
        margin: 15
        
        ,
        zIndex: 4
      }} 
      color = 'blue'
      onClick={this.minimizePanel} icon >
      <Icon name='angle double left'/>
    </Button>
    
    || 
      <Container>
        {
          settings.isMobile ?<center>        <h2>Legenda</h2></center> :         <h2>Legenda</h2>
        }

        <div 
          className={settings.isMobile ? "" : "setting-button"}>
        
        <Button onClick={this.minimizePanel} icon style={settings.isMobile ? settingsButtonStyle : null}>
          <Icon name='angle double right'/>
        </Button>        
      </div>

        <hr />
        
        <div>
        
        {/* Untuk inspect mode, mengeluarkan info saat layer di-hover */}
          <div width="100%">
            <Checkbox
              style={{width : "100%"}}
              label='Inspect Mode'
              name='inspectMode'
              value={settings.inspectMode}
              checked={settings.inspectMode}
              onClick={(e, {checked}) => this.props.onChange('inspect',checked)}
            />
            <Checkbox
            label='View 3D'
            name='controlPanel'
            value={viewBuffer}
            onClick={(e, {checked}) => this.props.onChange('buffer', checked)}
            />
          </div>
       
        </div>

        {/* Genangan Banjir  */}
        
        <center><h3>Genangan Banjir</h3></center>
        Tanggal : {settings.arrPolygonDate[settings.indexPolygon]}  
        <input
          type="range"
          value={settings.indexPolygon}
          min={0}
          max={settings.arrPolygonDate.length - 1}
          step={1}
          onChange={evt => this.props.onChange('indexPolygon', evt.target.value)}
      />
        <h4>Laporan warga</h4>
        <Button icon color='white' onClick={() => this.changeViewState('laporan',settings.viewLaporan)}  style={{position : 'absolute',right : 10,top : 220,backgroundColor : 'white'}}>
            <Icon name={settings.viewLaporan ? 'eye' : 'eye slash'} />
        </Button>
        <div className="input">
        
          <div>
          
       Radius value : {settings.radius} Meter 
          </div>
        
      <input
          type="range"
          value={settings.radius}
          min={25}
          max={5000}
          step={50}
          onChange={evt => this.props.onChange('radius', evt.target.value)}
      />
        
        
        
        </div>
        
        {
          (settings.layerSettings.smallestName === 0 && settings.layerSettings.largestName === 0 ) ? '' : (settings.layerSettings.smallestName === Infinity && settings.layerSettings.largestName === -Infinity ?
          <b>0 reports in result</b> : <center><h4>Ketinggian Banjir(cm)</h4></center> )

        }
        
         { settings.layerSettings.smallestName !== Infinity && settings.layerSettings.largestName !== -Infinity &&
             <HeightLegend settings={settings} legendItem={legendItem}  onChange={this.props.onChange}/> 
        }
        <hr />
        
        
        {/* DIV Area Banjir */}
        <div>
        <h4>Area Banjir - Data BNPB</h4>
        
        <Button icon color='white' onClick={() => this.changeViewState('polygon',settings.viewPolygon)}  style={{position : 'absolute',right : 10,top : 475,backgroundColor : 'white'}}>
            <Icon name={settings.viewPolygon ? 'eye' : 'eye slash'} />
        </Button>
        </div>
        <div id="polygon-control">
        

      <center><h5>Ketinggian Banjir(cm)</h5></center>
      <div id='legend-layout'>
        {
          ele
        }
      </div>
       <hr/> 
       <div id="div-lokasi-pengungsi">
          <h3>Titik Lokasi Pengungsian</h3>
          Tanggal : {settings.arrPengungsiDate[settings.indexPengungsi]}  
          <Button icon color='white' onClick={() => this.changeViewState('pengungsi',settings.viewPengungsi)}  style={{position : 'absolute',right : 10,top : 700,backgroundColor : 'white'}}>
            <Icon name={settings.viewPengungsi ? 'eye' : 'eye slash'} />
        </Button>
        <input
          type="range"
          value={settings.indexPengungsi}
          min={0}
          max={6}
          step={1}
          onChange={evt => this.props.onChange('indexPengungsi', evt.target.value)}
      />
       <h4>Jumlah jiwa</h4>
          <PengungsiLegend settings={settings} legendItem={legendItem}/>
        </div>

        <hr/>
      
       <h4>DAS - Aliran Sungai</h4>
       <Button icon color='white' onClick={() => this.changeViewState('das',settings.viewDAS)}  style={{position : 'absolute',right : 10,top : 950,backgroundColor : 'white'}}>
            <Icon name={settings.viewDAS ? 'eye' : 'eye slash'} />
        </Button>

      <div id='legend-layout'>

        {
          eleDAS
        }
        
      </div>
        </div>
        <hr/>
        <div id="pintu-air">
        <h4>Pintu Air</h4>
        <Button icon color='white' onClick={() => this.changeViewState('pintu',settings.viewPintu)}  style={{position : 'absolute',right : 10,top : 1100,backgroundColor : 'white'}}>
              <Icon name={settings.viewPintu ? 'eye' : 'eye slash'} />
          </Button>
         <center><h5>Ketinggian Muka Air (CM)</h5></center> 
              {
                elePintu
              }
        </div>
      </Container>
    );
  }
} 
