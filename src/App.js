import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import MapAnalyzer from './MapAnalyzer.js';
import MapBanjir from './MapBanjir.js';

import MapAnalyzerDeck from './MapDeckGL.js';
import MapCorona from './Corona/MapCorona.js';
import MapHannover from './Hannover/MapHannover.js';

import MapPolice from './MapPolicePolygon.js';
import './App.css';

function App() {
  return (
    <div className="App">
      {/* <Router>
        <Route path="/:id" component={MapAnalyzerDeck}/>          
      </Router> */}
      {/* <MapBanjir /> */}
      {/* <MapCorona /> */}
      <MapHannover/>
    </div>
  );
}

export default App;
