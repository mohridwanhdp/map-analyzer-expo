import React, {createContext} from 'react';
import {DeckGL, ScatterplotLayer,GeoJsonLayer,ColumnLayer} from 'deck.gl';
import ReactMapGL, {FlyToInterpolator,ScaleControl,NavigationControl,StaticMap,_MapContext as MapContext	} from 'react-map-gl';

import mapboxgl from 'mapbox-gl'
import {defaultMapStyle, darkMapStyle, lightMapStyle, satelliteMapStyle} from './style/map-style.js';
import {json as requestJson} from 'd3-request';
import ControlPanel from './ControlPanel.js'
import LayerInfo from './LayerInfoAnalyzer.js';
import LayerBox from './LayerBox.js'
import WebMercatorViewport from 'viewport-mercator-project';
import bbox from '@turf/bbox';
import axios from 'axios';
import { Button,Icon,Tab } from 'semantic-ui-react';
// import { ScaleControl } from "react-mapbox-gl";
import ReactGA from 'react-ga'
import "../analyzer/App.css";
const MAPBOX_TOKEN = 'pk.eyJ1IjoibW9ocmlkd2FuaGRwIiwiYSI6ImNrMDRvaGwzdDA0dDkzaG9tNnA3dmViODQifQ.qa1W5PaDaHzLnqIXbAM7zw';
const LayerURL = 'https://hnjp62bwxh.execute-api.us-west-2.amazonaws.com/GeoDev/getprojectbyid?_id='
const Rainbow = require('rainbowvis.js');
const convert = require('color-convert');
var groupColor = new Rainbow();
const refMap =  React.createRef();
const refDeck = React.createRef();

ReactGA.initialize('UA-157598276-1', {
  debug: false,
  titleCase: false
});
ReactGA.pageview('/analyzer');

export default class MapAnalyzer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    mapStyle: defaultMapStyle,
    viewState : {
          width : window.innerWidth,
          height : window.innerHeight,
          latitude: this.props.match.params.id === '5e2c33298d400500086a4f3e' ? 13.736717 : -6.175110,
          longitude:  this.props.match.params.id === '5e2c33298d400500086a4f3e' ? 100.523186 : 106.865036,
          zoom: this.props.match.params.id === '5e2c33298d400500086a4f3e' ? 3 : 12,
          bearing : 0,
          pitch : 0,
          transitionDuration : 1000
        },
      popupInfo : null,
      layers3D : [],
      hoveredObject : null,
      pointerX : 0,
      pointerY : 0,
      visualParams : [],
      dataLayers : null,
      view3D : true,
      columnPosition : null,
      year : 0,
      layerInfo : null,
      layerInfoVisibility : false,
      activeLayer : null,
      scaleCount : 6,
      legendColor : null,
      isPoint : false,
      radius : 200,
      viewBuffer : false,
      layerSettings : {
        smallestName : 'Fewer',
        largestName : 'More',
        smallestColor : '#8B0000',
        largestColor : '#006400'
      },
      showPopup : true,
      dataColumn : null,
      selectedMode : 'height',
      inspectMode : false,
      arrLayerInfo : [],
      max : 1,
      min : 0,
      width : window.innerWidth,
      height : window.innerHeight,
      tabIsMinimized : false,
      isCorona : this.props.match.params.id === '5e2c33298d400500086a4f3e' ? true : false
  }
    this._onViewStateChange = this._onViewStateChange.bind(this);
    this._loadData = this._loadData.bind(this); 
    this.changeLayerVisibility = this.changeLayerVisibility.bind(this);
    this.changeLayerInfoVisibility = this.changeLayerInfoVisibility.bind(this);
    this._onViewStateChange = this._onViewStateChange.bind(this);
    this._refreshMap = this._refreshMap.bind(this);
    this.addVisualParam = this.addVisualParam.bind(this);
    this.deleteVisualParam = this.deleteVisualParam.bind(this);
    this.layerBoxRef = React.createRef();
    this._fitBounds = this._fitBounds.bind(this);
    this._renderTooltip = this._renderTooltip.bind(this);
    this.filter = this.filter.bind(this);
    this.setReportColumn = this.setReportColumn.bind(this);
    this.setReportFeatures = this.setReportFeatures.bind(this);
    this.setMapScale = this.setMapScale.bind(this);
    this._onMapLoaded = this._onMapLoaded.bind(this);
    groupColor.setSpectrum('darkred','red','orange','yellow','green','blue','darkblue','violet','purple');

  }

  componentDidMount(){
    
    if(window.innerWidth > 500){
      document
      .getElementById("deckgl-wrapper")
      .addEventListener("contextmenu", evt => evt.preventDefault());  
    }

    requestJson(LayerURL+this.props.match.params.id, (error, response) => {
      if (!error) {
        this._loadData(response);    
      }
    });
    
    //Resize window
    window.addEventListener('resize', this._resize.bind(this));
    this._resize();
  }
  
  _resize() {
    this._onViewStateChange({
      width: window.outerWidth,
      height: window.outerHeight
    });
    this.setState({
      width : window.innerWidth,
      height : window.innerHeight
    }); 
    if(this.reactMap){
      const map = this.reactMap.getMap();
    
      map.resize();
    }
  }
  
  
  _loadData = dataRes => {   
    const dataLayers = dataRes.layers;
    
    var myRainbow = new Rainbow();
    var MAX = 6;

    myRainbow.setSpectrum('#8B0000','yellow','#006400');      
    

    myRainbow.setNumberRange(0,MAX - 1);
    var legendItem = []
    var j;
    for(j = 0 ; j < MAX ; j++){
      legendItem.push(convert.hex.rgb(myRainbow.colourAt(j)));       
     }    
    
     const arrLayerInfo = dataLayers.map((l,index) => {
       
       return {
         "id" : l._id,    
         "name" : l.name,     
         "arrChecked" : ['mapid_id'],
         "visibility" : false,
         "selectedColumn" : 'mapid_id',
         "selectedMode" : 'height',
         "radius" : 200,
         "view2D" : false,
         "classNumber" : isNaN(l.legend) || l.legend === null ? 6 : l.legend.class_number,
         "filterParam" : null
       };
     });
    this.setState({legendColor : myRainbow});
    this.setState({dataLayers : dataLayers});
    this.setState({arrLayerInfo : arrLayerInfo});
    this.setState({projectName : dataRes.name});
  }

  _onMapLoaded(){
    if(this.reactMap){
      const map = this.reactMap.getMap();
      map.addControl(new mapboxgl.ScaleControl());
    // map.on('moveend', e => {
    //     // do whatever you do after the fitbounds event.
    //     console.log(e.target);
    //     var viewst  = {
    //       viewState : {
    //         width : e.target.transform.width,
    //         height : e.target.transform.height,
    //         latitude: e.target.transform._center.lat,
    //         longitude: e.target.transform._center.lng,
    //         zoom: e.target.transform._zoom,
    //        pitch : e.target.transform._pitch
    //       }          
    //     }
    //     if(e.originalEvent){
    //       map.on(['movestart', 'zoomstart', 'boxzoomstart', 'rotatestart', 'pitchstart'], 
    //       // this._onViewStateChange(viewst),
    //       this.setState({viewState : viewst.viewState}),
    //       // viewst = e.target.transform
    //       );
    //     }
    //  })

    // var x = document.getElementsByClassName("mapboxgl-ctrl-top-right");
    // x.setAttribute("style", "z-index : 15");
    // x[0].style.zIndex = "16";
    // x.style.cssText = "z-index : 15";
    }
    this.setMapScale();
  }

  setMapScale(){
    var sc = document.getElementsByClassName("mapboxgl-ctrl-scale");
  if(sc[0]){
    sc[0].style.position = "absolute";
    sc[0].style.bottom = 0;
    sc[0].style.zIndex = 15;

    this.setState({mapScale : sc[0].innerHTML});
  }

  }

  getFilteredFeatures(ft,op,col,pr){
    if(op==='>='){
      ft = ft.filter((f) => {if(f.properties[col] >= pr){return f}});      
    }
    else if(op==='<='){
      ft = ft.filter((f) => {if(f.properties[col] <= pr){return f}});      
    }
    else if(op==='!='){
      ft = ft.filter((f) => {if(String(f.properties[col]) != pr){return f}});      
    }
    else if(op==='>'){
      ft = ft.filter((f) => {if(f.properties[col] > pr){return f}});      

    }
    else if(op==='<'){
      ft = ft.filter((f) => {if(f.properties[col] < pr){return f}});      

    }
    else if(op==='='){
      ft = ft.filter((f) => {if(String(f.properties[col]) === pr){return f}});           
    }
    else if(op==='has'){
      ft = ft.filter((f) => {
        var str = String(f.properties[col]); 
        var bool =  str.startsWith(pr);
        if(bool){return f}});            
    }

    return ft;
  }
  
  async _render3DLayers(activeLayer){
    const {dataLayers,arrLayerInfo,isCorona} = this.state;
    const {scaleCount} = this.state;
    var arr = [];
      
      arr =  arrLayerInfo.map(async(l,index) => {
        var data = dataLayers[index];
        var col = l.selectedColumn;
        var geojson = data.geojson;
        var max = 1,min = 0;  
        
        
        if(typeof geojson === 'string'){
          await axios.get(geojson).then(
            function (response) {
              geojson = response.data;

              if(l.selectedMode === 'height'){
                max = Math.max.apply(Math, geojson.features.map(function(o) { return o.properties[col];}));
                min = Math.min.apply(Math, geojson.features.map(function(o) { return o.properties[col];}));
              }


            }
          ).catch(function (error) {
            console.log(error);
          });
     }
     else{
       max = Math.min.apply(Math, geojson.features.map(function(o) { return o.properties[col];}));      
       min = Math.min.apply(Math, geojson.features.map(function(o) { return o.properties[col];}));
     }

     var dt = geojson.features.map((d) => (
      d.properties[col]
    ));

        if(activeLayer === data._id){
          /**
             * Untuk menentukan label di legend 
             */
            if(l.selectedMode === 'height'){
              if(isNaN(min) || isNaN(max)){
                min = 1;
                max = 1;
              }
              var lyrS = this.state.layerSettings;
              lyrS.smallestName = min;
              lyrS.largestName = max;
              this.setState({layerSettings : lyrS});
  
            }

            /**
             * Menentukan grouping
             */
            
            const dataColumn = dt.filter((val, id, array) => {
              return array.indexOf(val) === id;  
            });

            this.setState({dataColumn : dataColumn});
            this.setState({scaleCount : l.classNumber});
            
      }

        if(l.visibility && l.arrChecked.length > 0){
          var legendItem = []
          var i;

          if(l.filterParam){

            geojson.features = this.getFilteredFeatures(geojson.features,l.filterParam['op'],l.filterParam['col'],l.filterParam['param']);
     
          }
          const type = data.layer_type;

          var legendColor = new Rainbow();
     
              if(data['legend']){
                var sml = data['legend']['smallest_color'];
                var lrg = data['legend']['largest_color'];
                if(sml ==='#00'){
                  data['legend']['smallest_color']='#00FF00';
                }
          
                if( lrg ==='#00'){
                  data['legend']['largest_color']='#00FF00';
                }
                if(sml === '#8b0000' || sml === '#006400'){
                  legendColor.setSpectrum(data['legend']['smallest_color'],'yellow',data['legend']['largest_color']);          
                }
                else if(sml === '#FF0000' && lrg === '#00FF00'){
                  legendColor.setSpectrum('#8b0000','yellow','#006400');                          
                }
                else if(sml === '#00FF00' && lrg === '#FF0000'){
                  legendColor.setSpectrum('#006400','yellow','#8b0000');                                            

                }
                else {
                  legendColor.setSpectrum(data['legend']['smallest_color'],data['legend']['largest_color']);          
                }
              }else{  
                legendColor.setSpectrum('#8B0000','yellow','#006400');    
              }

              legendColor.setNumberRange(0,l.classNumber - 1);
              
              for(i = 0 ; i < l.classNumber ; i++){
                legendItem.push(convert.hex.rgb(legendColor.colourAt(i)));       
              }
              this.setState({legendColor : legendColor});

          if(type === 'fill' || type === 'line'){
            if(l.selectedMode === 'height'){
              return new GeoJsonLayer({
                  id: data._id,
                  data : geojson,
                  opacity: 0.7,
                  // stroked:  type === 'fill' ? true : false,
                  stroked : false,
                  filled: type === 'fill' ? true : false,
                  fp64 : true,
                  wireframe: true,
                  extruded : true,
                  elevationScale : type === 'line' ? 100 : (max <= 500 || l.view2D) ? 1 : 500/max,
                  getElevation: d => l.view2D ? 0.1  :  d.properties[col] * 10,
                  getFillColor: d => legendItem[(Math.round(((d.properties[col] - min)/(max-min))* (l.classNumber - 1)))],
                  getLineColor: d => legendItem[(Math.round(((d.properties[col] - min)/(max-min))*(l.classNumber - 1)))],
                  getLineWidth : d => type === 'fill' ? 1 :  d.properties['line-width'] * 15,
                  pickable: true,
                  updateTriggers: {
                    getPosition: [this.state.year],              
                    getElevation: [this.state.year],
                    getFillColor: [this.state.year],
                    getLineColor: [this.state.year]
                    
                  },
                  onHover: (info) => { this.setState({
                    hoveredObject: info.object,
                    pointerX: info.x,
                    pointerY: info.y
                  }); }
                });
            }
            else{
              /**
               * Layer jika selectedMode adalah group
               */
              

              const dataColumn = dt.filter((val, id, array) => {
                return array.indexOf(val) === id;  
              });


              groupColor.setNumberRange(0,dataColumn.length - 1);
           
                return new GeoJsonLayer({
                  id: data._id,
                  data : geojson,
                  opacity: 0.7,
                  stroked: false,
                  filled: true,
                  extruded: true,
                  fp64 : true,
                  wireframe: true,
                  elevationScale : 1,
                  getElevation: 0.1,
                  getFillColor: d => convert.hex.rgb(legendColor.colourAt(dataColumn.map(function(x) {return x; }).indexOf(d.properties[col]))),
                  getLineColor: d => convert.hex.rgb(legendColor.colourAt(dataColumn.map(function(x) {return x; }).indexOf(d.properties[col]))),
                  getLineWidth : d => type === 'fill' ? 1 :  d.properties['line-width'] * 15,                  
                  pickable: true,
                  updateTriggers: {
                    getPosition: [this.state.year],              
                    getElevation: [this.state.year],
                    getFillColor: [this.state.year],
                    getLineColor: [this.state.year]
                    
                  },
                  onHover: (info) => { this.setState({
                    hoveredObject: info.object,
                    pointerX: info.x,
                    pointerY: info.y
                  }); }
                });

            }
          }
          else if(type === 'symbol'){
              
              

              const dataColumn = dt.filter((val, id, array) => {
                return array.indexOf(val) === id;  
              });


              groupColor.setNumberRange(0,dataColumn.length - 1);

            if(l.view2D || l.selectedMode !== 'height'){
              return new ScatterplotLayer({
                  id: data._id,
                  data : geojson.features,
                  pickable: true,
                  opacity: 0.4,
                  stroked: true,
                  filled: true,
                  radiusMinPixels: 1,
                  radiusMaxPixels: 5000,
                  lineWidthMinPixels: 1,
                  getPosition: d => d.geometry.coordinates,
                  getElevation : 1,
                  getRadius: isCorona ? l.radius * 1000 : l.radius * 1,
                  getFillColor: d => l.selectedMode === 'height' ? legendItem[(Math.round((d.properties[col]/max)*5))] : convert.hex.rgb(groupColor.colourAt(dataColumn.map(function(x) {return x; }).indexOf(d.properties[col]))),
                  getLineColor: d => l.selectedMode === 'height' ? legendItem[(Math.round((d.properties[col]/max)*5))] : convert.hex.rgb(groupColor.colourAt(dataColumn.map(function(x) {return x; }).indexOf(d.properties[col]))),                  
                  updateTriggers: {
                    getPosition: [this.state.year],              
                    getFillColor: [this.state.year],
                    getLineColor: [this.state.year],
                    getRadius : [this.state.radius]                    
                  },
                  onHover: (info) => { this.setState({
                    hoveredObject: info.object,
                    pointerX: info.x,
                    pointerY: info.y
                  }); }
                })
            }
            else{
              return new ColumnLayer({
                      id: data._id,
                      data : geojson.features,
                      diskResolution : 2000,
                      opacity: 0.3,
                      stroked: false,
                      filled: true,
                  // getRadius: isCorona ? l.radius * 1000 : l.radius * 1,
                      radius : isCorona ? l.radius * 1000 : l.radius * 1,
                      extruded: true,
                      wireframe: true,
                      getElevationScale : 5000,    
                      elevationScale : max <= 500 ? 1 : 500/max,
                      getPosition : d => d.geometry.coordinates,
                      getElevation: d => d.properties[col] * 10,
                      getFillColor: d => l.selectedMode === 'height' ? legendItem[(Math.round((d.properties[col]/max)*(l.classNumber - 1)))] : convert.hex.rgb(groupColor.colourAt(dataColumn.map(function(x) {return x; }).indexOf(d.properties[col]))),
                      getLineColor: d => l.selectedMode === 'height' ? legendItem[(Math.round((d.properties[col]/max)*(l.classNumber - 1)))] : convert.hex.rgb(groupColor.colourAt(dataColumn.map(function(x) {return x; }).indexOf(d.properties[col]))),
                      pickable: true,
                      updateTriggers: {
                        getPosition: [this.state.year],              
                        getElevation: [this.state.year],
                        getFillColor: [this.state.year],
                        getLineColor: [this.state.year]
                        
                      },
                      onHover: (info) => { this.setState({
                        hoveredObject: info.object,
                        pointerX: info.x,
                        pointerY: info.y
                      });},
                      onClick : (info) => {this.setState({popupInfo : info});}
                 
                    });
            }
          }
        }else{
          return;
        }
      });
    const arr3D = await Promise.all(arr);
    this.setState({layers3D : arr3D});
  }

  async replace3DLayers(){

  }

  _renderTooltip() {
    const {pointerX, pointerY, hoveredObject} = this.state;
    // const props = geojson.features[0].properties;                        
    // const col = Object.keys(props);  
    return (
      hoveredObject && (
        <div  style={{top: pointerY, left: pointerX, zIndex : 13,position: 'absolute',
          margin: 8,
          padding: 4,
          background: 'rgba(0, 0, 0, 0.8)',
          color: '#fff',
          maxWidth: 300,
          fontSize: 10,
          pointerEvents: 'none'}}>
            <div>
            {Object.keys(hoveredObject['properties'])
            .filter((col) => col.startsWith("image"))
            .map(function(key) {       
              return ( 
                <div key={key} >
                    {hoveredObject['properties'][key] === '-' ? '' : <div> <img src={hoveredObject['properties'][key]} width="100" height="125"/></div>}            
                </div>
                )
            })}
            {Object.keys(hoveredObject['properties'])
            .filter((col) => !col.startsWith("image"))
            .map(function(key) {       
              return ( 
                <div key={key}>
                    <div> {key} : {hoveredObject['properties'][key]} <br /></div>            
                </div>
                )
            })}
            </div>
        </div>
      )
    );
  }

  _onViewStateChange({viewState}) {

    this.setState({
      viewState: {...this.state.viewState, ...viewState}
    });
    this.setMapScale();
    if(this.refMap){
      const map = this.refMap.getMap();
      console.log();
    }
  }

  changeLayerVisibility(layer){
    var arrLayerInfo = this.state.arrLayerInfo;

    var index = arrLayerInfo.map(function(x) {return x.id; }).indexOf(layer);    
    arrLayerInfo[index]['visibility'] = !arrLayerInfo[index]['visibility']; 
    this.setState({arrLayerInfo : arrLayerInfo});
    
    this._render3DLayers(this.state.activeLayer);
  }

  //Method for layer info
  changeLayerInfoVisibility(layer){
    const vis = this.state.layerInfoVisibility;
    this.setState({activeLayer : layer}); 
    this.forceUpdate();
    const {arrLayerInfo} = this.state;  


    this.setState({layerInfoVisibility : !vis});
    
    var lyr = this.state.dataLayers.filter(f => f._id === layer)
    
    this.setState({layerInfo : lyr});  
    var myRainbow = new Rainbow();
    if(lyr[0]['legend']){
      var sml = lyr[0]['legend']['smallest_color'];
      var lrg = lyr[0]['legend']['largest_color'];
      if(sml ==='#00'){
        lyr[0]['legend']['smallest_color']='#00FF00';
      }

      if( lrg ==='#00'){
        lyr[0]['legend']['largest_color']='#00FF00';
      }
      if(sml === '#8b0000' || sml === '#006400'){
        myRainbow.setSpectrum(lyr[0]['legend']['smallest_color'],'yellow',lyr[0]['legend']['largest_color']);          
      }
      else if(sml === '#FF0000' && lrg === '#00FF00'){
        myRainbow.setSpectrum('#8b0000','yellow','#006400');                          
      }
      else if(sml === '#00FF00' && lrg === '#FF0000'){
        myRainbow.setSpectrum('#006400','yellow','#8b0000');                                            

      }
      else {
        myRainbow.setSpectrum(lyr[0]['legend']['smallest_color'],lyr[0]['legend']['largest_color']);          
      }
    }else{  
      myRainbow.setSpectrum('#8B0000','yellow','#006400');    
    }

    var index = arrLayerInfo.map(function(x) {return x.id; }).indexOf(layer);
    
    if(lyr[0].layer_type === 'symbol'){
        this.setState({isPoint : true})
    }else{
      this.setState({isPoint : false})
    }

    myRainbow.setNumberRange(0,arrLayerInfo[index].classNumber - 1);
    this.setState({year : 0});  
    this.setState({visualParams : arrLayerInfo[index].arrChecked});  
    this.setState({legendColor : myRainbow});


    this._render3DLayers(layer);
  }
  

  changeMapStyle = (e) => {

    if(e === 'dark'){
      this.setState({mapStyle : darkMapStyle});
    }
    else if(e === 'light'){
      this.setState({mapStyle : lightMapStyle});
    }else if(e === 'basic'){
      this.setState({mapStyle : defaultMapStyle});
    }
    else{
      this.setState({mapStyle : satelliteMapStyle});      
    }
  }

  _updateSettings = (name, value) => {
    const {activeLayer,dataLayers} = this.state;
    var arrLayerInfo = this.state.arrLayerInfo;

    if (name === 'year') {
      var index = 0;
      if(arrLayerInfo.length > 0 ){
        index = arrLayerInfo.map(function(x) {return x['id']; }).indexOf(activeLayer);
        arrLayerInfo[index].selectedColumn = arrLayerInfo[index].arrChecked[value];
      }
      this.setState({year: value});
      this.setState({arrLayerInfo: arrLayerInfo});
      this._render3DLayers(activeLayer);
    }else if(name === 'radius'){
        this.setState({radius: value});
        index = arrLayerInfo.map(function(x) {return x['id']; }).indexOf(activeLayer);
        arrLayerInfo[index].radius = value;
        this.setState({arrLayerInfo: arrLayerInfo});
        this._render3DLayers(activeLayer); 
    } else if(name === 'buffer'){
        this.setState({viewBuffer: value});
        index = arrLayerInfo.map(function(x) {return x['id']; }).indexOf(activeLayer);
        arrLayerInfo[index].view2D = value;
        this.setState({arrLayerInfo: arrLayerInfo});
        this._refreshMap(activeLayer);

    } else if(name === 'layerSettings'){

        var myRainbow = new Rainbow();

        index = arrLayerInfo.map(function(x) {return x['id']; }).indexOf(activeLayer);

        var MAX = value.classNumber;

        myRainbow.setSpectrum(value.smallestColor,value.midColor,value.largestColor);
        
        myRainbow.setNumberRange(0,MAX - 1);
        console.log(myRainbow.colourAt(MAX - 1));
        axios.post(
          'https://hnjp62bwxh.execute-api.us-west-2.amazonaws.com/GeoDev/updatelayerlegendbyproject',
          {
            "project_id": this.props.match.params.id,
            "layer_id": activeLayer,
            "smallest_color": '#' + myRainbow.colourAt(0),
            "mid_color" : '#' + myRainbow.colourAt(MAX / 2),
            "largest_color": '#' + myRainbow.colourAt(MAX - 1),
            "class_number" : MAX
          }
        )
        index = dataLayers.map(function(x) {return x['_id']; }).indexOf(activeLayer);
        
        dataLayers[index]['legend'] = {
          smallest_color : '#' + myRainbow.colourAt(0),
          mid_color : '#' + myRainbow.colourAt(MAX / 2),
          largest_color : '#' + myRainbow.colourAt(MAX - 1),
          class_number : MAX
        }
        
        if(arrLayerInfo.length > 0 ){
          index = arrLayerInfo.map(function(x) {return x['id']; }).indexOf(activeLayer);
          arrLayerInfo[index].classNumber = MAX;
        }

        this.setState({legendColor : myRainbow});
        this.setState({dataLayers: dataLayers});
        this.setState({scaleCount: MAX});
        this.setState({arrLayerInfo : arrLayerInfo})
        this._render3DLayers(activeLayer);

    }else if(name === 'mode'){
        const {legendColor,dataColumn,scaleCount} = this.state;
        if(arrLayerInfo.length > 0 ){
          index = arrLayerInfo.map(function(x) {return x['id']; }).indexOf(activeLayer);
          arrLayerInfo[index].selectedMode = value;
        }
        var lColor = legendColor;
        if(value === 'height'){
          lColor.setNumberRange(0, scaleCount - 1);
        }
        else{
          lColor.setNumberRange(0,dataColumn.length - 1);
        }

        this.setState({selectedMode : value});
        this.setState({arrLayerInfo : arrLayerInfo});
        this.setState({legendColor : lColor});

        this._refreshMap();
    }else if(name === 'inspect'){
        this.setState({inspectMode : value});
    }

  };

  _refreshMap(){
    console.log(this.reactMap.getMap()._controls[2]);

    this.setState({layers3D: []});
    const {activeLayer} = this.state;
    setTimeout(() => {
      this._render3DLayers(activeLayer);
    },5
     );
  }

  _fitBounds = (l) => {
    
    const {viewState} = this.state;
    const [minLng, minLat, maxLng, maxLat] = bbox(l);
    
    // construct a viewport instance from the current state
    const viewport = new WebMercatorViewport(viewState);
    const {longitude, latitude, zoom} = viewport.fitBounds([[minLng, minLat], [maxLng, maxLat]], {
      padding: 50
    });
    
    const {activeLayer} = this.state;

    this._onViewStateChange({
      viewState: {
        ...this.state.viewState,
        longitude,
        latitude,
        zoom,
        transitionInterpolator: new FlyToInterpolator(),
        transitionDuration: 300
      }
    });
  }

  addVisualParam(val){
    var arr = this.state.visualParams;
    const {year,activeLayer} = this.state;
    var arrLayerInfo = this.state.arrLayerInfo;
    arr.push(val);
    // arr.sort();

    arr = [...new Set(arr)];
    
    var arrInfo = arrLayerInfo.filter(f => f.id === activeLayer);
    
    arrInfo[0]['arrChecked'].push(val);
    arrInfo[0]['arrChecked'] = [...new Set(arrInfo[0]['arrChecked'])];
    var index = arrLayerInfo.map(function(x) {return x.id; }).indexOf(activeLayer);
    
    arrLayerInfo[index] = arrInfo[0];


    this.setState({visualParams : arr});
    this.setState({arrLayerInfo : arrLayerInfo});

    if(arr.length === 1){
      this._updateSettings('year',0);
    }else{
      this._updateSettings('year',year);
    }
  }

  deleteVisualParam(val){
    var arr = this.state.visualParams;
    const {year,activeLayer} = this.state;
    var arrLayerInfo = this.state.arrLayerInfo;
    
    /**Delete from arrLayerInfo */
    var arrInfo = arrLayerInfo.filter(f => f.id === activeLayer);
    
    arrInfo[0].arrChecked = arrInfo[0].arrChecked.filter(ele => {
      return ele !== val;
    });

    var index = arrLayerInfo.map(function(x) {return x.id; }).indexOf(activeLayer);
    
    arrLayerInfo[index] = arrInfo[0];
    this.setState({arrLayerInfo : arrLayerInfo});

    
    /**Delete from  visualParam*/
    index = arr.map(function(x) {return x; }).indexOf(val);
    arr = arr.filter(function(ele){
      return ele !== val;
    });    
    
    this.setState({visualParams : arr});

    if(arr.length  < 1){
      var lyrS = this.state.layerSettings;
      lyrS.smallestName = 'Fewer';
      lyrS.largestName = 'More';
  
      this.setState({layerSettings : lyrS});
      this._render3DLayers(activeLayer);
      

    }else{
      if(year === index){
        if(arr.length === 1){
          this._updateSettings('year',0);
        }else {
          if(year === 0){
            this._updateSettings('year',0);
          }else{
            this._updateSettings('year',year - 1);
          }
        }
      }else if(year >= arr.length){  
        if(index < year){
          this._updateSettings('year',arr.length - 1);

        }else{
          this._updateSettings('year',year - 1);
        }  
      }else{
        this._updateSettings('year',year);
      }
     
      this.forceUpdate();
    }
  }

  _onViewStateChangeDeck({viewState}) {
    this.setState({
      viewStateDeck: {...this.state.viewStateDeck, ...viewState}
    });
  }
  
  filter(column,param){
    const {activeLayer} = this.state;
    var arrLayerInfo = this.state.arrLayerInfo;
    /**Delete from arrLayerInfo */
    var arrInfo = arrLayerInfo.filter(f => f.id === activeLayer);
    var op;
    var prm;
    var index = arrLayerInfo.map(function(x) {return x.id; }).indexOf(activeLayer);
    
    if(param === null || param.length == 0){
      op = null;
    }
    else if(param.startsWith('>=')){
      op = '>=';
    }
    else if(param.startsWith('<=')){
      op = '<=';
    }
    else if(param.startsWith('!=')){
      op = '!=';
    }
    else if(param.startsWith('>')){
      op = '>';
    }
    else if(param.startsWith('<')){
      op = '<';
    }
    else if(param.startsWith('=') || param.startsWith('==')){
      op = '=';      
    }
    else{
      op = 'has'
    }

    if(op && column){
      var prm = param.split(op);
      var paramSearch = {
        op : op,
        param : op === 'has' ? prm[0]: prm[1],
        col : column
      };
    }
    else{
      var paramSearch = null;
    }

    arrInfo[0].filterParam = paramSearch;
    arrLayerInfo[index] = arrInfo[0];
      
      this.setState({arrLayerInfo : arrLayerInfo});
    
    this._render3DLayers(activeLayer);
  }

  setReportFeatures(features){
    this.setState({reportFeatures : features});
  }
  setReportColumn(column){
    this.setState({reportColumn : column});

  }
  render() {
    
    const {viewState,layers3D,legendColor,dataLayers,layerInfo,layerInfoVisibility,view3D,activeLayer,inspectMode,mapStyle,arrLayerInfo} = this.state;    
    const {scaleCount,isPoint,viewBuffer,visualParams,dataColumn,selectedMode,year,layerSettings,radius,height,width,tabIsMinimized,isCorona} = this.state;
    const {reportFeatures,reportColumn,projectName,mapScale} = this.state;
    var arrChecked = arrLayerInfo.filter((f) => f.id === activeLayer); 
    var filterParam = '';
    var layerName = '';
    const isMobile = width <= 500;
    
    
    if(arrChecked.length > 0){
      layerName = arrChecked[0]['name'];
      filterParam = arrChecked[0]['filterParam'];
      arrChecked = arrChecked[0]['arrChecked'];
    }
    
    if(isMobile){
      const panes = [
        {
          menuItem: 'Layer list',
          render: () =>
  <Tab.Pane attached={false}>
          {     dataLayers &&
                <div style={{height : '40vh'}}>
                      <LayerBox data={{dataLayers}}
                      changeVisibility={this.changeLayerVisibility}
                      changeInfo={this.changeLayerInfoVisibility}
                      changeDimension = {this.changeDimensionalView}
                      view3D= {view3D}
                      zoom = {this.zoomBound}
                      ref={this.layerBoxRef}
                      fitBounds = {this._fitBounds}
                    />
                  </div>
              || <div style={{height : '40vh'}}>
              Data is loaded . . .
            </div> 
          }
        </Tab.Pane>
          }
        ,
        {
          menuItem: 'Legend Control',
          render: () => 
          <Tab.Pane attached={false}>
          {
            
            (legendColor && activeLayer ) &&
        <div style={{height : '40vh'}}>
            <ControlPanel
            // containerComponent={this.props.containerComponent}
            settings={{
              scaleCount,
              isPoint,
              viewBuffer,
              visualParams,
              dataColumn,
              selectedMode,
              inspectMode,
              year,
              legendColor,
              layerSettings,
              radius,
              layerName,
              isCorona
            }}
            onChange={this._updateSettings}
            style={{overflow: 'auto'}}
            ref={refMap}
            /> 
            </div> || <div style={{height : '40vh'}}>
                    Select a layer first
                  </div> 
           
          }
           </Tab.Pane>
        },
        {
          menuItem: 'Layer Info',
          render: () => <Tab.Pane attached={false}>
             {
                (dataLayers && activeLayer) && 
                  <div style={{height : '40vh'}}>
  
                          <LayerInfo
                          visibility={layerInfoVisibility}
                          layerInfo={layerInfo}
                          add = {this.addVisualParam}
                          delete = {this.deleteVisualParam}
                          fitBounds = {this._fitBounds}
                          arrChecked = {arrChecked}
                          /> 
                    </div> || <div style={{height : '40vh'}}>
                    Select a layer first
                  </div> 
  
            }
  
          </Tab.Pane>,
        },
      ]
      return (
        <div style={{height : "100%", width : "100%"}}>
          
          {/*MAPID logo*/}
          <div style={{position: 'absolute', top: 15, left: 10,zIndex : 14}}>
            <a href="https://geo.mapid.io">
            <img src="https://geomapid2019.s3-us-west-2.amazonaws.com/logo.svg" width="35" height="35" alt="Go to dashboard"/>  
            </a>
          </div>
          <div className="btn-map-style">
          <Button.Group>
          <Button icon onClick={() => this._refreshMap()}>
            <Icon name='refresh' />
          </Button>
              <Button value='basic' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Street</Button>
              <Button value='satellite' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Satellite</Button>   
        </Button.Group> 
          </div>
          
          {/* Tab for controller */}
          <div style={{position : 'absolute', bottom : 0, left : 0, width : "100%",zIndex : 5}}>
          <Button style={{position : 'absolute', right : 5, top : 0,background : 'white'}} icon onClick={ () => this.setState({tabIsMinimized : !tabIsMinimized})}>
              <Icon name={tabIsMinimized ? 'angle double up' : 'angle double down'}/>
          </Button>
            <Tab menu={{ secondary: true, pointing: true }} panes={panes} style={{backgroundColor : 'white'}} renderActiveOnly={!tabIsMinimized}>
              
            </Tab>
          </div>
          
          <div style={{height: "100vh", width : "100vw"}} id="map-render">
  
          <DeckGL 
      initialViewState={viewState}
      controller={true}
      layers={layers3D}
      viewState = {viewState}
      onViewStateChange={(viewState) => this._onViewStateChange(viewState)}
      ContextProvider={MapContext.Provider}

      glOptions={{preserveDrawingBuffer: true}}
      >
          
 
  <ReactMapGL
    mapStyle={mapStyle}
    mapboxApiAccessToken={MAPBOX_TOKEN}
    preserveDrawingBuffer={true}
    ref={(reactMap) => this.reactMap = reactMap}
    refMap={refMap}
    onLoad={() => {this._onMapLoaded()}}
  >


 <ScaleControl maxWidth={100} unit={"metric"} position={"bottom-left"}/>
 <div style={{position : 'absolute',right : 10,top : 20, zIndex : 15}}>
        <NavigationControl 
      onViewStateChange={(viewState) => this._onViewStateChange(viewState)}
      showCompass={true}
      captureDrag={true}
      captureScroll={true}
  />
        </div>

</ReactMapGL>
        
    { inspectMode && this._renderTooltip() }
   
  </DeckGL>
          </div>
        </div>
      );
    }
    else{
      return (
        <div style={{height:height, width:width}}>
  
          <div style={{position: 'absolute', top: 10, left: 10,zIndex : 14}}>
            <a href="https://geo.mapid.io">
            <img src="https://geomapid2019.s3-us-west-2.amazonaws.com/logo.svg" width="35" height="35" alt="Go to dashboard"/>
  
            </a>
          </div>
                    <div className="btn-map-style">
          <Button.Group>
          <Button icon onClick={() => this._refreshMap()}>
            <Icon name='refresh' />
          </Button>
              <Button value='basic' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Street</Button>
                  <Button value='light' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Light</Button>
                  <Button value='dark' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Dark</Button>      
              <Button value='satellite' type="button" onClick={ (e) => this.changeMapStyle(e.target.value)}>Satellite</Button>   
        </Button.Group> 
          </div>
          {
              dataLayers && 
              <LayerInfo
              visibility={layerInfoVisibility}
              layerInfo={layerInfo}
              add = {this.addVisualParam}
              delete = {this.deleteVisualParam}
              fitBounds = {this._fitBounds}
              arrChecked = {arrChecked}
              filter={this.filter}
              filterParam = {filterParam}
              setReportFeatures = {this.setReportFeatures}
              setReportColumn = {this.setReportColumn}
              />
          }
  
          {
            dataLayers &&
            
              <div>
            <LayerBox data={{dataLayers}}
            changeVisibility={this.changeLayerVisibility}
            changeInfo={this.changeLayerInfoVisibility}
            changeDimension = {this.changeDimensionalView}
            view3D= {view3D}
            zoom = {this.zoomBound}
            ref={this.layerBoxRef}
            fitBounds = {this._fitBounds}
            
            />
            
            </div>
          }
          {
            (legendColor && activeLayer && layerInfoVisibility) &&
  
            <ControlPanel
            // containerComponent={this.props.containerComponent}
            settings={{
              scaleCount,
              isPoint,
              viewBuffer,
              visualParams,
              dataColumn,
              selectedMode,
              inspectMode,
              year,
              legendColor,
              layerSettings,
              radius,
              layerName,
              isCorona,
              reportParam : {
                projectName : projectName,
                layerName : layerName,
                features : reportFeatures,
                column : reportColumn,
                scale : mapScale
              },
            }}
            onChange={this._updateSettings}
            style={{overflow: 'auto'}}
            ref={refMap}
            /> 
          }
  
        {/* <div style={{height: window.innerHeight, width:window.innerWidth}} id="map-render"> */}
  
        <div style={{height: height, width :width}} id="map-render">
  

      <DeckGL 
      initialViewState={viewState}
      controller={true}
      layers={layers3D}
      viewState = {viewState}
      onViewStateChange={(viewState) => this._onViewStateChange(viewState)}
      ContextProvider={MapContext.Provider}

      glOptions={{preserveDrawingBuffer: true}}
      >
          
 
  <ReactMapGL
    mapStyle={mapStyle}
    mapboxApiAccessToken={MAPBOX_TOKEN}
    preserveDrawingBuffer={true}
    ref={(reactMap) => this.reactMap = reactMap}
    refMap={refMap}
    onLoad={() => {this._onMapLoaded()}}
  >


 {/* <ScaleControl maxWidth={100} unit={"metric"} position={"bottom-left"}/> */}
 <div style={{position : 'absolute',right : 10,top : 20, zIndex : 15}}>
        <NavigationControl 
      onViewStateChange={(viewState) => this._onViewStateChange(viewState)}
      showCompass={true}
      captureDrag={true}
      captureScroll={true}
  />
        </div>

</ReactMapGL>
        
    { inspectMode && this._renderTooltip() }
   
  </DeckGL>
    
      
      
      </div>
  </div>
      );
    }
  }  
}

