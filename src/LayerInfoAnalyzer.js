import React, { PureComponent } from 'react';
import {Radio,Table, Button} from 'semantic-ui-react';
import {json as requestJson} from 'd3-request';
import {Input,Checkbox,Loader,Dimmer,Segment,Modal,Message,Dropdown} from 'semantic-ui-react';
import axios from 'axios';

const marginedStyle = {
  marginRight : 10  
}
const LoadingSpinner = () => (
 
  <div style={{height: 200, width:"100%",backgroundColor : '#000',zIndex :9,color : 'white',position : 'absolute',bottom : 0}}>
   <Segment style={{height: "100%", width:"100%",backgroundColor : '#000',zIndex :9,color : 'white'}}>
     <Dimmer active>
       <Loader>Load data</Loader>
     </Dimmer>
 </Segment>  </div>
);
const mobileStyle = {
  maxWidth : 400,
  height : 300,
  maxHeight : 250,
  overflow : 'auto',
  size : 10
}

const arrOperator = ['==','>','<','!='];

class LayerInfo extends PureComponent {
    
    constructor(props){
        super(props);
        
        this.state = {
            arrChecked : [],
            layer : props.layerInfo,
            column : null,
            features : null,
            isLoading : true,
            selectedRow : null,
            isUrl : false,
            isError : false,
            selectedMode : 'height',
            op : null,
            filterColumn : null,
            paramSearch : null,
            filterParam : null    
        };
        this.loadJson = this.loadJson.bind(this);
        this.check_me = React.createRef();
        this.getJsonData = this.getJsonData.bind(this);
        this.handleChangeRow = this.handleChangeRow.bind(this);
        this.onChange = this.onChange.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.filter = this.filter.bind(this);
    }

  closeModal(){
    this.setState({isError : false});
  }

  getFilteredFeatures(ft,{op,col,param}){
    if(op==='>='){
      ft = ft.filter((f) => {if(f.properties[col] >= param){return f}});      
    }
    else if(op==='<='){
      ft = ft.filter((f) => {if(f.properties[col] <= param){return f}});      
    }
    else if(op==='!='){
      ft = ft.filter((f) => {if(String(f.properties[col]) != param){return f}});      
    }
    else if(op==='>'){
      ft = ft.filter((f) => {if(f.properties[col] > param){return f}});      

    }
    else if(op==='<'){
      ft = ft.filter((f) => {if(f.properties[col] < param){return f}});      

    }
    else if(op==='='){
      ft = ft.filter((f) => {if(String(f.properties[col]) === param){return f}});           
    }
    else if(op==='has'){
      ft = ft.filter((f) => {
        var str = String(f.properties[col]); 
        var bool =  str.startsWith(param);
        if(bool){return f}});            
    }
    console.log(ft);
    return ft;
  }

  componentDidMount(){
    const isMobile = window.innerWidth <= 500;
    if(isMobile && this.props.layerInfo){
      var geojson = '';
      const layer = this.props.layerInfo;
      if(typeof layer[0].geojson === 'object'){
        geojson = layer[0].geojson;
        this.setState({geojson : layer[0].geojson});              
        const props = geojson.features[0].properties;                        
        const col = Object.keys(props);
        const features = geojson.features;
        this.setState({ column : col,features : features,isLoading : false});
        this.setState({isUrl : false});
      } else{
        this.setState({isUrl : true});
        this.getJsonData(layer[0].geojson);  
      }
    this.setState({arrChecked : this.props.arrChecked});
    
    }
    this.setState({isMobile : isMobile});
  } 

  componentWillReceiveProps(nextProps) {
    // You don't have to do this check first, but it can help prevent an unneeded render
    if(nextProps.visibility === true){
      if (nextProps.layerInfo !== this.state.layer) {
        this.setState({selectedRow : null,isLoading : true}); 
        const layer = nextProps.layerInfo;
        
        var geojson;
        if(typeof layer[0].geojson === 'object'){
          geojson = layer[0].geojson;
          this.setState({geojson : layer[0].geojson});              
          const props = geojson.features[0].properties;                        
          const col = Object.keys(props);
          const features = geojson.features;
          this.setState({ column : col,features : features,filteredFeatures : features,isLoading : false});
          this.setState({isUrl : false});
          if(!this.state.isMobile){
          this.props.setReportColumn(col);
            this.props.setReportFeatures(features);
          }
        } else{
          this.setState({isUrl : true});
          this.getJsonData(layer[0].geojson);  
        }
        this.setState({ layer: nextProps.layerInfo}); 
        this.setState({selectedMode : nextProps.selectedMode});
        this.setState({ filterParam: nextProps.filterParam}); 
      }
      else{
        if(this.state.filterParam !== nextProps.filterParam){
          if(nextProps.filterParam){
            const {op,col,param} = nextProps.filterParam;
            if(op && col && param){
              console.log(nextProps.filterParam);
              var features = this.state.features;
              features = this.getFilteredFeatures(features,nextProps.filterParam);
              this.setState({filteredFeatures : features});
          if(!this.state.isMobile){
            this.props.setReportFeatures(features);
          }
            }
            else{
              const {features} = this.state;
              this.setState({filteredFeatures : features});
            if(!this.state.isMobile){
              this.props.setReportFeatures(features);
            }
            }
           
          }
          else{
            const {features} = this.state;
            this.setState({filteredFeatures : features});
            if(!this.state.isMobile){
            
            nextProps.setReportFeatures(features);
            }
          }
          this.setState({ filterParam: nextProps.filterParam}); 

        }
      }

      
    }
    
    this.setState({arrChecked : nextProps.arrChecked});
  }

    loadJson(DATA){
      var dt;
      requestJson(DATA, (error, response) => {
        if (!error) {
        
          this.setState({ geojson: response });
          dt =  response;
        }
        else{
            JSON.stringify(error);
        }
      });
      return dt;
    }

   fitBounds(f){
      this.props.fitBounds(f);
   } 

   filter(paramSearch,filterColumn){
      this.props.filter(filterColumn,paramSearch);      
   }
   onSearchType(param){
      const {filterColumn} = this.state;
      this.filter(param,filterColumn);
      this.setState({paramSearch : param});
   }

   onColumnChange(val){
    const {paramSearch} = this.state;
    this.filter(paramSearch,val);
    this.setState({filterColumn : val});

   }

    onChange(val,stat,i){
      var arrChecked = this.state.arrChecked;
      var isError = false;
      const {features,selectedMode} = this.state;
      arrChecked = arrChecked.filter(function(ele){
        return ele !== val;
      });

      if(stat){
         
        if(selectedMode === 'height'){
          isError = features.some(element => {
            return isNaN(element.properties[val]);
          });

        }
        if(isError){
          this.setState({isError:true});
          return;
        }
        this.props.add(val);
      }
      else{
          this.props.delete(val);
      }
      this.forceUpdate();
    }

    getJsonData = async(url) => {
      var x = await axios.get(url);
      
      var {data} = x;
      this.setState({geojson : data});
      const props = data.features[0].properties;                        
      const col = Object.keys(props);
      const features = data.features;
      this.setState({column : col,features : features,filteredFeatures : features, isLoading : false});
      if(!this.state.isMobile){
        this.props.setReportFeatures(features);
        this.props.setReportColumn(col);

      }
      return data;
    }

    handleChangeRow(value){
      const {geojson} = this.state;
      var ly = '';
      ly = geojson.features.filter(ft => ft.properties.mapid_id === value);        
      this.fitBounds(ly[0]);
      this.setState({ selectedRow : value });      
    }

    render() {
     const {column,filteredFeatures,isLoading,features,selectedRow,isError,paramSearch} = this.state;
    //  var column = this.state.column;

    //  if(column){
    //     column = column.filter((x) => {return x !== 'mapid_id' || x !== 'OBJECTID' });
    //  }
     
     
     var arrChecked = this.state.arrChecked;
    const isMobile = window.innerWidth <= 500;
    if(isMobile){
        return(
          <div style={mobileStyle} >      
                <Table collapsing unstackable celled size='small'  >
                    <Table.Header>
                      <Table.Row>
                          {/* <Table.HeaderCell >
                            Go To 
                          </Table.HeaderCell > */}
                      
                        { column && 
                              
                              column.map((l, index) => (
                                    <Table.HeaderCell key={index}  >
                                      <center><Checkbox
                                    label={l}
                                    name='column'
                                    value={l}
                                    checked={arrChecked.map(function(x) {return x; }).indexOf(l) < 0 ? false : true}
                                    // ref="check_me"
                                    onClick={(e, {label,checked}) => {this.onChange(label,checked,index)}}
                                    /></center>
                                     {/* <label style={{fontSize: '10px'}}>{l}</label> */}
                                     </Table.HeaderCell>
                                ))
                        }
                    </Table.Row>
                    </Table.Header>
                  { features && 
                    
                    <Table.Body >        
                    {
                      features.map((f,index) => (
                      <Table.Row key={index}>
                        {/* <Table.Cell>
                        <Radio
                                  name='radioGroup'
                                  value={f.properties['mapid_id']}
                                  checked={selectedRow === f.properties['mapid_id']}
                                  onChange={(e, {value}) => {this.handleChangeRow(value); }}
                                />
                        </Table.Cell> */}
                        
                          {
                            
                            column.map(function(l,index) {
                                return <Table.Cell key={index} style={{fontSize : 18}}>{f.properties[l]}</Table.Cell>
                                })
                          }
                            </Table.Row>
                      ))  
                      
                    }
                    </Table.Body> 
                  }
                </Table>
            </div>
        );
    }
    else{
      return (
       
        (isLoading && this.props.visibility) &&
          <LoadingSpinner/> || 
        
        <div className={this.props.visibility ? 'layer-info' : 'layer-info-hidden'}>
          <Modal basic size='small'
            open={isError}
            onClose={this.closeModal}>                   
              <Modal.Content>
                <Message negative={true}>
                  <p>
                    Please only check the column which have only numbers value.
                  </p>
                </Message>
                </Modal.Content>          
          </Modal> 
          {
            this.props.visibility && 
          <div>          
          <div id='search-box' className={this.props.visibility ? 'search-box' : ''}>
                {
                  column &&               
                  <Dropdown       
                  selection        
                  placeholder='Select column for filter'
                  value={this.state.filterColumn}
                style={marginedStyle}  
                  options={
                    column.map( (cl,index)=>({
                      key:index,
                      text:cl,
                      value : cl
                    }))
                  }
                  onChange={(e,{value}) => {this.onColumnChange(value)}}
                /> 
                }
                
                <Input placeholder='Type to filter . . .' 
                style={marginedStyle}  
                onChange={(e,{value}) => {this.onSearchType(value)}}
                defaultValue={paramSearch}/>
          </div>   
          
          <div id='table-layer-info' className={'table-layer-info'}>
            <Table celled>
                  <Table.Header>
                    <Table.Row>

                      { !isLoading && 
                          <Table.HeaderCell>
                          Go To 
                </Table.HeaderCell >
                      }
                    
                      { column && 
                            
                            column
                            .map((l, index) => (
                                  <Table.HeaderCell key={index}>
                                    <center><Checkbox
                                  label={l}
                                  name='column'
                                  value={l}
                                  checked={arrChecked.map(function(x) {return x; }).indexOf(l) < 0 ? false : true}
                                  // ref="check_me"
                                  onClick={(e, {label,checked}) => {this.onChange(label,checked,index)}}
                                  /></center></Table.HeaderCell>
                              ))
                      }
                  </Table.Row>
                  </Table.Header>
                { filteredFeatures && 
                  
                  <Table.Body>        
                  {
                    filteredFeatures.map((f,index) => (
                    <Table.Row key={index}>
                      <Table.Cell>
                      <Radio
                                name='radioGroup'
                                value={f.properties['mapid_id']}
                                checked={selectedRow === f.properties['mapid_id']}
                                onChange={(e, {value}) => {this.handleChangeRow(value); }}
                              />
                      </Table.Cell>
                      
                        {                          
                          column                          
                          .map(function(l,index) {
                              return <Table.Cell key={index} style={{fontSize : 14}} >{f.properties[l]}</Table.Cell>
                              })
                        }
                          </Table.Row>
                    ))  
                    
                  }
                  </Table.Body> 
                }
              </Table>
          </div>
          </div>
          } 
        </div>
            );
        }
    }
    }
     

export default LayerInfo;
